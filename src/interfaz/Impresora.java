package interfaz;

import java.io.*;

public class Impresora {
	//Variables de  acceso   al dispositivo
	private FileWriter fw;
	private BufferedWriter bw;
	private PrintWriter pw;
	private String dispositivo="";
	
	/* Esta funcion inicia el  dispositivo donde se va a imprimir */
	public Impresora(){
		
	}
	public  void setDispositivo( String texto ) {
		dispositivo=texto;
		
		try{
			fw = new FileWriter(dispositivo);
			bw = new BufferedWriter (fw);
			pw = new PrintWriter (bw);
		}catch(Exception e){
			System.out.print(e);
		}

	}

	public  void escribirln( String texto ) {
		try{

			pw.println(texto);

		}catch(Exception e){
			System.out.print(e);
		}

	}
	public  void escribir( String texto ) {
		try{

			pw.print(texto);

		}catch(Exception e){
			System.out.print(e);
		}

	}
	
	public  void escribir( char[] texto ) {
		try{
			pw.write(texto);
		}catch(Exception e){
			System.out.print(e);
		}

	}
	
	public  void cortar( ) {
		try{
			
			if(!this.dispositivo.trim().equals("pantalla.txt")){
				char[] CORTAR_PAPEL=new char[]{0x1B,'m'};
				escribir(CORTAR_PAPEL);
			}

		}catch(Exception e){
			System.out.print(e);
		}

	}
	public  void avanza_pagina( ) {
		try{
			if(!this.dispositivo.trim().equals("pantalla.txt")){
				pw.write(0x0C);
			}

		}catch(Exception e){
			System.out.print(e);
		}

	}
	public  void setRojo( ) {
		try{
			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'r',1};
			if(!this.dispositivo.trim().equals("pantalla.txt")){
				pw.write(ESC_CUT_PAPER);
			}

		}catch(Exception e){
			System.out.print(e);
		}

	}
	public  void setNegro( ) {
		try{
			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'r',0};
			if(!this.dispositivo.trim().equals("pantalla.txt")){
				pw.write(ESC_CUT_PAPER);
			}

		}catch(Exception e){
			System.out.print(e);
		}
	}
	public  void setTipoCaracterLatino( ) {
		try{
			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'R',18};
			if(!this.dispositivo.trim().equals("pantalla.txt")){
				pw.write(ESC_CUT_PAPER);
			}

		}catch(Exception e){
			System.out.print(e);
		}
	}
	public void centrar(){
		try{
			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'a',1};
			if(!this.dispositivo.trim().equals("pantalla.txt")){
				pw.write(ESC_CUT_PAPER);
			}

		}catch(Exception e){
			System.out.print(e);
		}
	}
	public void izquierda(){
		try{
			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'a',0};
			if(!this.dispositivo.trim().equals("pantalla.txt")){
				pw.write(ESC_CUT_PAPER);
			}

		}catch(Exception e){
			System.out.print(e);
		}
	}
	public void derecha(){
		try{
			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'a',2};
			if(!this.dispositivo.trim().equals("pantalla.txt")){
				pw.write(ESC_CUT_PAPER);
			}

		}catch(Exception e){
			System.out.print(e);
		}
	}
	public void reestablecer(){
		try{
			char[] ESC_CUT_PAPER = new char[] { 0x1B, '@'};
			if(!this.dispositivo.trim().equals("pantalla.txt")){
				pw.write(ESC_CUT_PAPER);
			}

		}catch(Exception e){
			System.out.print(e);
		}
	}
	
	public void triple(){
		try{
			char[] ESC_CUT_PAPER = new char[] { 0x1D, '!',1};
			if(!this.dispositivo.trim().equals("pantalla.txt")){
				pw.write(ESC_CUT_PAPER);
			}

		}catch(Exception e){
			System.out.print(e);
		}
	}
	
	
	public void fuentea(){
		try{
			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'M',0};
			if(!this.dispositivo.trim().equals("pantalla.txt")){
				pw.write(ESC_CUT_PAPER);
			}

		}catch(Exception e){
			System.out.print(e);
		}
	}
	public void fuenteb(){
		try{
			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'M',1};
			if(!this.dispositivo.trim().equals("pantalla.txt")){
				pw.write(ESC_CUT_PAPER);
			}

		}catch(Exception e){
			System.out.print(e);
		}
	}
	public void fuentec(){
		try{
			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'M',2};
			if(!this.dispositivo.trim().equals("pantalla.txt")){
				pw.write(ESC_CUT_PAPER);
			}

		}catch(Exception e){
			System.out.print(e);
		}
	}
	public void negrita(){
		try{
			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'E',1};
			if(!this.dispositivo.trim().equals("pantalla.txt")){
				pw.write(ESC_CUT_PAPER);
			}

		}catch(Exception e){
			System.out.print(e);
		}
	}
	public void nonegrita(){
		try{
			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'E',0};
			if(!this.dispositivo.trim().equals("pantalla.txt")){
				pw.write(ESC_CUT_PAPER);
			}

		}catch(Exception e){
			System.out.print(e);
		}
	}
	public void doble_strike_on(){
		try{
			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'G',1};
			if(!this.dispositivo.trim().equals("pantalla.txt")){
				pw.write(ESC_CUT_PAPER);
			}

		}catch(Exception e){
			System.out.print(e);
		}
	}
	public void doble_strike_off(){
		try{
			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'G',0};
			if(!this.dispositivo.trim().equals("pantalla.txt")){
				pw.write(ESC_CUT_PAPER);
			}

		}catch(Exception e){
			System.out.print(e);
		}
	}
	public  void setFormato(int formato ) {
		try{
			char[] ESC_CUT_PAPER = new char[] { 0x1B, '!',(char)formato};
			if(!this.dispositivo.trim().equals("pantalla.txt")){
				pw.write(ESC_CUT_PAPER);
			}

		}catch(Exception e){
			System.out.print(e);
		}
	}
	public  void correr(int fin){
		try{
			int i=0;
			for(i=1;i<=fin;i++){
				this.salto();
			}

		}catch(Exception e){
			System.out.print(e);
		}
	}
	public  void salto() {
		try{

			pw.println("");

		}catch(Exception e){
			System.out.print(e);

		}

	}
	public void dividir(){
		escribirln("------------------------------------------");

	}
	public void dividir_asterisco(){
		escribirln("******************************************");
	}
	public  void cerrarDispositivo(  ){
		try{

			pw.close();
			if(this.dispositivo.trim().equals("pantalla.txt")){

				java.io.File archivo=new java.io.File("pantalla.txt");
				java.awt.Desktop.getDesktop().open(archivo);
			}

		}catch(Exception e){
			System.out.print(e);
		}

	}
	public void abrir_cajon(){
		/*(!this.dispositivo.trim().equals("pantalla.txt")){
			escribirln("\\x1B@\\x0A\\x0D\\027\\112\\000\\100\\250\\x0A\\x0D");
		}*/
		try{
			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'p',0,50,50};
			if(!this.dispositivo.trim().equals("pantalla.txt")){
				pw.write(ESC_CUT_PAPER);
			}

		}catch(Exception e){
			System.out.print(e);
		}
	}
	
	public void emitir_sonido(){
		try{
			char[] ESC_CUT_PAPER = new char[] { 0x1B, '(','A', 3,0,97,1,1};
			if(!this.dispositivo.trim().equals("pantalla.txt")){
				pw.write(ESC_CUT_PAPER);
			}
		}catch(Exception e){
			System.out.print(e);
		}
	}
	

}
