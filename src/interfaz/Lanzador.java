package interfaz;

import javax.swing.UIManager;

public class Lanzador {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
        javax.swing.SwingUtilities.invokeLater (() -> new PanelInicializacion());
	}
}
