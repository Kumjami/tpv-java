package interfaz;

import java.util.Vector;

public class ConfigObject {
	public boolean bol_replicante,bol_modificadores;
	public String st_imp_tiq,st_imp_com;
	public int numerocaja;
	public Vector <String> st_cabecera,st_pie;
	
	public ConfigObject(){
		bol_replicante=bol_modificadores=false;
		st_imp_tiq="";
		st_imp_com="";
		st_cabecera=new Vector<String>(1,1);
		st_pie=new Vector<String>(1,1);
		numerocaja=0;
	}
}
