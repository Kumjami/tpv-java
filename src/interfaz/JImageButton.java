package interfaz;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;

import javax.swing.Icon;
import javax.swing.JButton;


@SuppressWarnings("serial")
public class JImageButton extends JButton {
	
	Image backgroundImage;
	int posicion=0;
	
	public JImageButton(){
		super();	
	}
	public JImageButton(String str){
		super(str);
	}
	public JImageButton(String str,Icon icono){
		super(str,icono);
	}
	public JImageButton(String str,int pos){
		super(str);
		posicion=pos;
	}
	
	
	public void setBackgroundImage( Image image )  
	    {  
	        MediaTracker mt = new MediaTracker( this );  
	        mt.addImage( image, 0 );  
	        try  
	        {  
	            mt.waitForAll();  
	            backgroundImage = image;  
	        }  
	        catch( InterruptedException x )  
	        {  
	            System.err.println(  
	                "Specified background image could not be loaded." );  
	        }  
	    }  
	  
	  public void paintComponent( Graphics g )  
	    {  
		  
	        super.paintComponent( g );  
	   
	       /* Color saved = g.getColor();  
	        g.setColor( getBackground() );  
	        g.fillRect( 0, 0, getWidth(), getHeight() );  
	        g.setColor( saved );  
	        */
	        Font fuente=new Font("TimesRoman",Font.BOLD,16);
	        g.setFont(fuente);
	        if( backgroundImage != null )  
	        {  
	        	//g.drawString( super.getText(), getWidth() / 4, getHeight() / 2  ); 
	            int imageX = ( getWidth() - backgroundImage.getWidth( this ) ) / 2  ;  
	            int imageY = ( getHeight() - backgroundImage.getHeight( this ) ) / 2  ;  
	            g.drawImage( backgroundImage, imageX+1, imageY+1, this );  
	            String cadena=super.getText();
	           
	            if(cadena.equals("----->") || cadena.equals("<-----")){
	            	
	            }else if (posicion==0){
	            	 if(cadena.length()>18){
	 	            	cadena=cadena.substring(0, 18);
	 	            	fuente=new Font("TimesRoman",Font.BOLD,13);
	 	     	        g.setFont(fuente);
	 	            }else if(cadena.length()>=14){
	 	            	cadena=cadena.substring(0, cadena.length());
	 	            	fuente=new Font("TimesRoman",Font.BOLD,14);
	 	     	        g.setFont(fuente);
	 	            }
	            	g.drawString( cadena, 7, getHeight() -10 ); 
	            }else if(posicion==1){
	            	
	            	//cadena=cadena.replace("#", newChar);
	            	
	            	//g.drawString(cadena,getWidth()-fuente.getLineMetrics(cadena, null).getBaselineIndex()/3, getHeight()/2-fuente.getLineMetrics(cadena, null).getHeight()/2 ); 
	            	g.setFont(new Font("TimesRoman",Font.BOLD,40));
	            	FontMetrics fm=g.getFontMetrics();
	            	int ancho=getWidth()/2-fm.stringWidth(cadena)/2;
	            	int alto=((getHeight()/2)+(fm.getHeight())/2)-20;
	            	
	            	
	            	g.drawString(cadena.toString(),ancho, alto); 
	            	g.setFont(fuente);
	            }
	           
	          
	        }  
	   
	        /*if( !getText().equals( "" ) )  
	        {  
	            g.drawString( super.getText(), getWidth() / 2, getHeight() / 2  );  
	        }  
	   
	       /* if( getIcon() != null )  
	        {  
	            Icon icon = getIcon();  
	            icon.paintIcon( this, g, 10, 10 );  
	        }  */
	    }  
	  
	

}
