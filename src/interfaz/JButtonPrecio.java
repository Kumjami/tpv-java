package interfaz;

import java.awt.Image;

public class JButtonPrecio extends JImageButton {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double precioT,precioB;
	private String tabla;
	private int id_p;
	Image backgroundImage;
	
	public JButtonPrecio(String str,double prcT,double prcB,int id,String table){
		super(str);
		precioT=prcT;
		precioB=prcB;
		id_p=id;
		tabla=table;
	}
	

	public void set_precioT(double prcT){
		precioT=prcT;
	}


	
	public double get_precioT(){
		return precioT;
	}
	public void set_precioB(double prcB){
		precioT=prcB;
	}
	public double get_precioB(){
		return precioB;
	}
	public void set_id(int id){
		id_p=id;
	}
	public int get_id(){
		return id_p;
	}
	public void set_tabla(String table){
		tabla=table;
	}
	public String get_tabla(){
		return tabla;
	}
	
}
