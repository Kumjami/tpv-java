package interfaz;

public class JButtonEnlazado extends JImageButton{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButtonEnlazado siguiente;
	private JButtonEnlazado anterior;
	private int index;
	private boolean terraza;
	
	public JButtonEnlazado(String str,JButtonEnlazado jbe,int idx){
		super(str);
		anterior=jbe;
		index=idx;
		siguiente=null;
	}
	public JButtonEnlazado(String str,JButtonEnlazado jbe,int idx,boolean sit){
		super(str);
		anterior=jbe;
		index=idx;
		siguiente=null;
		terraza=sit;
	}
	public void set_siguiente(JButtonEnlazado sgt){
		siguiente=sgt;
	}
	public void set_anterior(JButtonEnlazado ant){
		anterior=ant;
	}
	public void set_index(int idx){
		index=idx;
	}
	public JButtonEnlazado get_siguiente(){
		return siguiente;
	}
	public JButtonEnlazado get_anterior(){
		return anterior;
	}
	public int get_index(){
		return index;
	}
	public boolean get_sit(){
		return terraza;
	}
}
