package interfaz;

import java.io.*;
import java.net.*;
import java.util.*;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;


import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class VeneciaTPV extends JFrame implements ActionListener{

	
	
	private boolean interfazcreada=false;
	
	private String permiso="";
	private String usuario="";
	private ListenThread hilo_escucha;
	private JPanel Pa_terraza,Pa_barra,Pa_inferior,Pa_principal,Pa_secundario;
	private JScrollPane Sc_inferior;
	private JImageButton [] Bt_barras, Bt_mesas,Bt_numero,Bt_categoria;
	private JImageButton  Bt_terraza,Bt_barra,Bt_cerrar,Bt_num;
	private JButtonEnlazado lista,current;

	private int x,y;//prueba grid mesas
	
	private JPanel Pa_menu_categorias;
	
	private JPanel Pa_categorias[][];
	private JCustomButton Bt_prod_cat[][];
	
	private Dimension Dim_panel;
	
	private Connection conexion_db;
	private Statement statement,statement2,statement3,statementsesion;
	private ResultSet rs,rs2,rssesion;
	private String query,fecha;
	
	
	private final String salto_linea=System.getProperty("line.separator");
	/*
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	private JImageButton [] Bt_number;
	private JImageButton Bt_cifra,Bt_coma,Bt_terraza2,Bt_barra2,Bt_imprimir,Bt_cobrar,Bt_juntar,Bt_invita;
	private JImageButton Bt_cerrar2,Bt_camarero,Bt_camarera,Bt_tiquet,Bt_borrar,Bt_separa,Bt_unidades,Bt_precio;
	private int currentcategoria=0,currentpanel=0,decimales=0,current_mesa,n_mesa,current_pedido,sesion_actual=0;
	private boolean tiquet=true;
	
	private Vector<String> lista_prod=new Vector<String>(1,1);
	private String row,situacion;
	private JList Lst_prod;
	private JScrollPane Sc_prod;
	
	private JLabel La_total,La_importe;
	private DecimalFormat df;
	
	private boolean b_coma=false;
	private double importe;
	private int unidades,tiempo_mesa,pedido_juntar;
	
	
	private JLabel La_entrega,La_cambio;
	private JPanel Pa_cobrar,Pa_opc_derecha;
	private Font fuente_cobros,fuente_importe,ft_bot_general,ft_bot_pequeno,ft_numeros;
	
	
	private JDialog menu_op,seleccion;
	private JPanel Pa_opciones,Pa_camareros[],Pa_dependientas[]; 
	private JImageButton Bt_opciones,Bt_add_camarero,Bt_add_dependienta,Bt_config,Bt_jefe;
	private JImageButton Bt_out_camarero,Bt_out_dependienta,Bt_salir;
	private JDialog menu_camareros;
	private JButtonEnlazado lista_camareros,lista_dependientas,current_camareros,current_dependientas;
	private int n_cam=0,n_pans_camareros,n_dep=0,n_pans_dependientas;
	private JScrollPane Sc_camareros[],Sc_dependientas[];
	private int current_panel_camareros=0,current_panel_dependientas=0;
	private JImageButton Bt_sig_pan_cam,Bt_ant_pan_cam,Bt_sig_pan_dep,Bt_ant_pan_dep;
	
	private boolean sacar=false,juntando=false;
	private JPanel Pa_cit,Pa_cadep,Pa_num_sup;
	
	private int id_pedido_aux;
	
	private GridBagConstraints gbc;
	
	private JImageButton Bt_can_cobro;
	
	private final Color azulclaro=new Color(175,224,254),azuloscuro=new Color(39,196,239),marron=new Color(82,53,21),mesaabierta=new Color(96,18,141);
	private ConfigObject obj_conf=new ConfigObject();
	
	private boolean separando=false;
	
	
	private Vector<String> ids_lista_prod=new Vector<String>(1,1);
	
	
	/*
	 * 
	 *  Constructor Unasolaventanalayout. 
	 * 
	 * 
	 */
	
	private boolean pintar_bordes=false;
	
	
	
	private JPanel Pa_terbar;
	private Process p;
	/****************************************************************************************************************/
	/*******************************HASTA AKI LAS VARIABLES**********************************************************/
	/****************************************************************************************************************/
	
	public VeneciaTPV (){
		super("Programa Venecia v.Dev");
		this.setIconImage( Toolkit.getDefaultToolkit().getImage("resources/images/interfaz/iconogondola.jpg"));
		
		ArchivoConfig.cargar_config(obj_conf);
		
		if(obj_conf.bol_replicante){
			try 
			{ 
				System.out.println("Iniciando replicaci�n");
				String[] comando=new String[]{"cmd","/c","C:/rubyrep-1.2.0/rubyrep","replicate","-c","C:/rubyrep-1.2.0/prueba1.conf"};
				p = Runtime.getRuntime().exec(comando);
			} 
			catch (Exception e) 
			{ 
				JOptionPane.showMessageDialog(this,"Error al intentar iniciar la replicaci�n");
			}
		}
		
		
		
		
		this.setLayout(new CardLayout());
		
		
		
		double altod=Toolkit.getDefaultToolkit().getScreenSize().getHeight();
		double anchod=Toolkit.getDefaultToolkit().getScreenSize().getWidth();
		int anchot=(int)anchod;
		int altot=(int)altod;
		
		gbc=new GridBagConstraints();
		
		this.setSize(anchot,altot);
		this.setLocationRelativeTo(null);
		this.setUndecorated(true);
		this.setVisible(true);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.getContentPane().setBackground(azuloscuro);
		
		
		ft_bot_general=new Font("TimesRoman",Font.BOLD,20);
		ft_bot_pequeno=new Font ("TimesRoman",Font.BOLD,16);
		ft_numeros=new Font("TimesRoman",Font.BOLD,24);
		
	//Preparaci�n de las tablas y paneles a usar
		
		db_connect();	//Conexion con la base de datos
		leer_tablas();
		
		try{
			new MulticastSocket();
		}catch(Exception e){
			e.printStackTrace();
		}
	//-----------
	/*
	 * 
	 * PANEL PRINCIPAL
	 * 
	 * 
	 * 
	 * 
	 */
		
		
		
		Pa_principal=new JPanel();
		Pa_principal.setBackground(azulclaro);
		Pa_principal.setLayout(new GridBagLayout());
		Pa_principal.setVisible(true);
		this.add(Pa_principal,"grid");
		
		
		
		
	//Barra de botones superior 
		
		
		
		Bt_barra=new JImageButton("<html><p align='center'>Barra</p></html>");
		Bt_barra.setFont(ft_bot_general);
		Bt_barra.setBackground(marron);
		Bt_barra.setForeground(Color.white);
		Bt_barra.setBorderPainted(pintar_bordes);
		Bt_barra.addActionListener(this);
		Bt_barra.setPreferredSize(new Dimension(100,40));
		
		gbc.gridx=gbc.gridy=0;
		gbc.gridheight=1;
		gbc.gridwidth=1;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1.0;
		gbc.weighty=0.05;
		
		Pa_principal.add(Bt_barra,gbc);
		
		
		
		Bt_terraza=new JImageButton("<html><p align='center'>Terraza</p></html>");
		Bt_terraza.setFont(ft_bot_general);
		Bt_terraza.setBackground(marron);
		Bt_terraza.setForeground(Color.white);
		Bt_terraza.setBorderPainted(pintar_bordes);
		Bt_terraza.addActionListener(this);
		Bt_terraza.setPreferredSize(new Dimension(100,40));
		
		gbc.gridx=1;
		gbc.gridy=0;
		gbc.gridheight=1;
		gbc.gridwidth=1;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1.0;
		gbc.weighty=0.0;
		
		Pa_principal.add(Bt_terraza,gbc);
		
		
		Bt_numero=new JImageButton[10];
		gbc.gridx=GridBagConstraints.RELATIVE;
		gbc.gridy=0;
		gbc.gridheight=1;
		gbc.gridwidth=1;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1.0;
		gbc.weighty=0.0;
		for(int i=0;i<10;i++){
			Bt_numero[i]=new JImageButton(""+i);
			Bt_numero[i].setFont(ft_numeros);
			Bt_numero[i].setBackground(Color.white);
			Bt_numero[i].setForeground(mesaabierta);
			Bt_numero[i].setBorderPainted(pintar_bordes);
			Bt_numero[i].addActionListener(this);
			gbc.gridx=i+2;
			Pa_principal.add(Bt_numero[i],gbc);
			
		}
				
		Bt_num=new JImageButton("");
		Bt_num.setFont(ft_numeros);
		Bt_num.setBackground(Color.white);
		Bt_num.setForeground(mesaabierta);
		Bt_num.setBorderPainted(pintar_bordes);
		Bt_num.addActionListener(this);
		gbc.gridx=12;
		gbc.gridy=0;
		gbc.gridheight=1;
		gbc.gridwidth=GridBagConstraints.REMAINDER;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1.0;
		gbc.weighty=0.0;
		Pa_principal.add(Bt_num,gbc);
		
		
		
		
	//--------
		
	//Panel para los botones correspondientes a las mesas
		Pa_terbar=new JPanel(new CardLayout());
		gbc.gridx=0;
		gbc.gridy=1;
		gbc.gridheight=1;
		gbc.gridwidth=GridBagConstraints.REMAINDER;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1.0;
		gbc.weighty=1.0;
		Pa_principal.add(Pa_terbar,gbc);
	
		
		Pa_terraza=new JPanel(new GridLayout(7,7));
		Pa_terraza.setBackground(azulclaro);
		Bt_mesas=new JImageButton[49];
		for(int i=0;i<49;i++){
			Bt_mesas[i]=new JImageButton("Mesa "+(i+1));
			Bt_mesas[i].setFont(ft_bot_general);
			Bt_mesas[i].setBackground(Color.white);
			Bt_mesas[i].setForeground(azuloscuro);
			//Bt_mesas[i].setBorder(BorderFactory.createLineBorder(marron));
			Bt_mesas[i].setBorderPainted(pintar_bordes);
			Bt_mesas[i].addActionListener(this);
			Pa_terraza.add(Bt_mesas[i]);
		}
		Pa_terbar.add(Pa_terraza,"terraza");
		
		
		new Dimension(Bt_mesas[0].getSize());
		
	//Panel para los botones correspondientes a los cientes de la barra.
		

		
		
		Pa_barra=new JPanel(new GridLayout(7,7));
		Pa_barra.setBackground(azulclaro);
		Bt_barras=new JImageButton[49];
		for(int i=0;i<49;i++){
			Bt_barras[i]=new JImageButton("Barra "+(i+1));
			Bt_barras[i].setFont(ft_bot_general);
			Bt_barras[i].setBackground(Color.white);
			Bt_barras[i].setForeground(azuloscuro);
			Bt_barras[i].setBorder(BorderFactory.createLineBorder(marron));
			Bt_barras[i].setBorderPainted(pintar_bordes);
			Bt_barras[i].addActionListener(this);
			Pa_barra.add(Bt_barras[i],gbc);
		}
		Pa_terbar.add(Pa_barra,"barra");
		
	//---------
		
	//Panel inferior 
		
		
		Pa_inferior=new JPanel(new FlowLayout(FlowLayout.LEADING));	
		Pa_inferior.setBackground(azulclaro);
		Sc_inferior=new JScrollPane(Pa_inferior,JScrollPane.VERTICAL_SCROLLBAR_NEVER,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);			

		Sc_inferior.getVerticalScrollBar().setValue(((Sc_inferior.getVerticalScrollBar().getMaximum())));
		Sc_inferior.setPreferredSize(new Dimension (0,60));
		gbc.gridx=0;
		gbc.gridy=2;
		gbc.gridheight=GridBagConstraints.REMAINDER;
		gbc.gridwidth=12;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1.0;
		gbc.weighty=0.0;
		Pa_principal.add(Sc_inferior,gbc);

		Bt_opciones=new JImageButton("Opciones");
		Bt_opciones.setFont(ft_bot_general);
		Bt_opciones.setForeground(Color.white);
		Bt_opciones.setBackground(marron);
		Bt_opciones.addActionListener(this);
		//Bt_opciones.setPreferredSize(new Dimension(180,40));
		gbc.gridx=12;
		gbc.gridy=2;
		gbc.gridheight=GridBagConstraints.REMAINDER;
		gbc.gridwidth=GridBagConstraints.REMAINDER;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1.0;
		gbc.weighty=0.05;
		Pa_principal.add(Bt_opciones,gbc);
		
		
		
	//-----
		
		hilo_escucha=new ListenThread(Bt_mesas,Bt_barras,obj_conf.st_imp_com);
		hilo_escucha.start();
		Pa_principal.repaint();
		
		
		
		
	/*
	 * PANEL SECUNDARIO
	 * 
	 * 
	 * 
	 * 	
	 */
		
		Pa_secundario=new JPanel();
		Pa_secundario.setLayout(new GridBagLayout());
		Pa_secundario.setBackground(azuloscuro);
		Pa_secundario.setVisible(false);
		this.add(Pa_secundario,"mesas");
		
		

		Pa_num_sup=new JPanel(new GridBagLayout());
		Pa_num_sup.setBackground(azuloscuro);
		
		df=new DecimalFormat("0.00");
		gbc.gridy=0;
		gbc.gridheight=1;
		gbc.gridwidth=1;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1;
		gbc.weighty=1;
		Bt_number=new JImageButton[10];
		
		
		for(int i=0;i<10;i++){
			Bt_number[i]=new JImageButton(""+i);
			Bt_number[i].setFont(ft_numeros);
			Bt_number[i].setBackground(Color.white);
			Bt_number[i].setForeground(mesaabierta);
			Bt_number[i].setBorder(BorderFactory.createLineBorder(marron));
			Bt_number[i].setBorderPainted(pintar_bordes);
			
			gbc.gridx=i;
			Bt_number[i].setVerticalTextPosition(SwingConstants.BOTTOM);
			Bt_number[i].addActionListener(this);
			Pa_num_sup.add(Bt_number[i],gbc);
		}
		
		Bt_coma=new JImageButton(",");
		Bt_coma.setFont(ft_numeros);
		Bt_coma.setBackground(Color.white);
		Bt_coma.setForeground(mesaabierta);
		Bt_coma.setBorder(BorderFactory.createLineBorder(marron));
		Bt_coma.setBorderPainted(pintar_bordes);
		Bt_coma.addActionListener(this);
		gbc.gridx=10;
		gbc.gridy=0;
		gbc.gridheight=1;
		gbc.gridwidth=1;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1;
		gbc.weighty=1;
		Pa_num_sup.add(Bt_coma,gbc);
		
		Bt_cifra=new JImageButton("");
		Bt_cifra.setFont(ft_numeros);
		Bt_cifra.setBackground(azulclaro);
		Bt_cifra.setForeground(mesaabierta);
		Bt_cifra.setBorder(BorderFactory.createLineBorder(azulclaro));
		Bt_cifra.setBorderPainted(pintar_bordes);
		Bt_cifra.setPreferredSize(new Dimension(100,40));
		gbc.gridx=11;
		gbc.gridy=0;
		gbc.gridheight=1;
		gbc.gridwidth=2;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1.0;
		gbc.weighty=1;
		Bt_cifra.addActionListener(this);
		Pa_num_sup.add(Bt_cifra,gbc);
		
		
		gbc.gridx=0;
		gbc.gridy=0;
		gbc.gridheight=1;
		gbc.gridwidth=GridBagConstraints.REMAINDER;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1.0;
		gbc.weighty=0.2;
		Pa_secundario.add(Pa_num_sup,gbc);
		
	//------
		
	//Panel de menu y productos
		
		
		gbc.gridx=0;
		gbc.gridy=1;
		gbc.gridwidth=1;
		gbc.gridheight=2;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1.0;
		gbc.weighty=0.1;
		Pa_secundario.add(Pa_menu_categorias,gbc);
		
		
		gbc.gridx=0;
		gbc.gridy=3;
		gbc.gridwidth=3;
		gbc.gridheight=GridBagConstraints.REMAINDER;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1.0;
		gbc.weighty=1;
		for(int i=0;i<Pa_categorias.length;i++){
			for(int j=0;j<Pa_categorias[i].length;j++){
				Pa_secundario.add(Pa_categorias[i][j],gbc);
				Pa_categorias[i][j].setVisible(false);
				
			}
		}
		currentcategoria=0;
		currentpanel=0;
		Pa_categorias[currentcategoria][currentpanel].setVisible(true);
		
	//Opciones derecha y lista 
		
		Bt_barra2=new JImageButton("Barra");
		Bt_barra2.setFont(ft_bot_general);
		Bt_barra2.setBackground(azulclaro);
		Bt_barra2.setForeground(marron);
		Bt_barra2.setBorder(BorderFactory.createLineBorder(azuloscuro));
		Bt_barra2.setBorderPainted(pintar_bordes);
		Bt_barra2.setPreferredSize(new Dimension(60,60));
		Bt_barra2.addActionListener(this);
		gbc.gridx=2;
		gbc.gridy=1;
		gbc.gridwidth=1;
		gbc.gridheight=1;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=0.2;
		gbc.weighty=0.2;
		Pa_secundario.add(Bt_barra2,gbc);
		
	
		Bt_terraza2=new JImageButton("<html>Mesa<br>"+n_mesa+"</html>");
		Bt_terraza2.setFont(ft_bot_general);
		Bt_terraza2.setBackground(azulclaro);
		Bt_terraza2.setForeground(marron);
		Bt_terraza2.setBorder(BorderFactory.createLineBorder(azuloscuro));
		Bt_terraza2.setBorderPainted(pintar_bordes);
		Bt_terraza2.setPreferredSize(new Dimension(60,60));
		Bt_terraza2.addActionListener(this);
		gbc.gridx=2;
		gbc.gridy=2;
		gbc.gridwidth=1;
		gbc.gridheight=1;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=0.2;
		gbc.weighty=0.2;
		Pa_secundario.add(Bt_terraza2,gbc);
		
		
		Pa_cit=new JPanel(new GridBagLayout());
		Pa_cit.setBackground(azuloscuro);
		
		
		Bt_cerrar2=new JImageButton("");
		Bt_cerrar2.setBackground(Color.white);
		Bt_cerrar2.setForeground(marron);
		Bt_cerrar2.setFont(ft_bot_general);
		Bt_cerrar2.addActionListener(this);
		gbc.gridx=0;
		gbc.gridy=0;
		gbc.gridwidth=GridBagConstraints.RELATIVE;
		gbc.gridheight=GridBagConstraints.RELATIVE;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1;
		gbc.weighty=1;
		Pa_cit.add(Bt_cerrar2,gbc);
		
		Bt_imprimir=new JImageButton("");
		Bt_imprimir.setFont(ft_bot_general);
		Bt_imprimir.setBackground(Color.white);
		Bt_imprimir.addActionListener(this);
		
		gbc.gridx=1;
		gbc.gridy=0;
		gbc.gridwidth=GridBagConstraints.REMAINDER;
		gbc.gridheight=GridBagConstraints.RELATIVE;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1;
		gbc.weighty=1;
		Pa_cit.add(Bt_imprimir,gbc);
		
		Bt_tiquet=new JImageButton("Tiquet: SI");
		Bt_tiquet.setFont(ft_bot_general);
		Bt_tiquet.setBackground(Color.white);
		Bt_tiquet.setForeground(azuloscuro);
		Bt_tiquet.addActionListener(this);
		Bt_tiquet.setPreferredSize(new Dimension(160,25));
		gbc.gridx=0;
		gbc.gridy=1;
		gbc.gridwidth=GridBagConstraints.REMAINDER;
		gbc.gridheight=GridBagConstraints.REMAINDER;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1;
		gbc.weighty=0.1;
		Pa_cit.add(Bt_tiquet,gbc);
		
		gbc.gridx=6;
		gbc.gridy=1;
		gbc.gridwidth=GridBagConstraints.REMAINDER;
		gbc.gridheight=2;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=0.1;
		gbc.weighty=0.2;
		Pa_secundario.add(Pa_cit,gbc);
		
		
		Pa_cadep=new JPanel(new GridLayout());
		Pa_cadep.setBackground(azuloscuro);
		
		Bt_camarero=new JImageButton("Camarero",1);
		Bt_camarero.setBorderPainted(pintar_bordes);
		Bt_camarero.setBackground(Color.white);
		Bt_camarero.setForeground(Color.blue);
		Bt_camarero.addActionListener(this);
		
		Pa_cadep.add(Bt_camarero);
		
		
		Bt_camarera=new JImageButton("Dependienta",1);
		Bt_camarera.setBorderPainted(pintar_bordes);
		Bt_camarera.setBackground(Color.white);
		Bt_camarera.setForeground(Color.blue);
		Bt_camarera.addActionListener(this);
	
		Pa_cadep.add(Bt_camarera);
		
		gbc.gridx=3;
		gbc.gridy=1;
		gbc.gridwidth=3;
		gbc.gridheight=2;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=0.1;
		gbc.weighty=0.2;
		Pa_secundario.add(Pa_cadep,gbc);
		
		
		//Parte intermedia, superior al grid de productos y a la lista -----//
		
		fuente_importe=new Font("TimesRoman",Font.BOLD,60);
		
		La_importe=new JLabel("",JLabel.CENTER);
		La_importe.setBackground(azulclaro);
		La_importe.setForeground(Color.blue);
		La_importe.setFont(fuente_importe);
		La_importe.repaint();
		gbc.gridx=3;
		gbc.gridy=7;
		gbc.gridwidth=3;
		gbc.gridheight=2;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=0.1;
		gbc.weighty=0.2;
		Pa_secundario.add(La_importe,gbc);
		
		
		Lst_prod=new JList();
		Lst_prod.setFont(ft_bot_pequeno);
		Lst_prod.setBackground(Color.white);
		Lst_prod.setForeground(marron);
		Lst_prod.setBorder(BorderFactory.createLineBorder(azulclaro, 5));
		Sc_prod=new JScrollPane(Lst_prod,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		Sc_prod.setPreferredSize(new Dimension(330,350));
		gbc.gridx=3;
		gbc.gridy=3;
		gbc.gridwidth=4;
		gbc.gridheight=4;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=0;
		gbc.weighty=0.7;
		Pa_secundario.add(Sc_prod,gbc);
		
		Pa_cobrar=new JPanel(new GridLayout(2,1));
		Pa_cobrar.setBackground(azulclaro);
		Pa_cobrar.setPreferredSize(new Dimension(330,350));
		gbc.gridx=3;
		gbc.gridy=3;
		gbc.gridwidth=4;
		gbc.gridheight=4;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=0.0;
		gbc.weighty=0.7;
		Pa_secundario.add(Pa_cobrar,gbc);
		Pa_cobrar.setVisible(false);
		
		fuente_cobros=new Font("TimesRoman", Font.BOLD,120);
		
		La_entrega=new JLabel("",JLabel.CENTER);
		La_entrega.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.blue), "Entrega:",0,0,Font.getFont("Entrega"),Color.blue));
		La_entrega.setFont(fuente_cobros);
		La_entrega.setForeground(Color.blue);
		Pa_cobrar.add(La_entrega);
		
		La_cambio=new JLabel("",JLabel.CENTER);
		La_cambio.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.blue), "Cambio:",0,0,Font.getFont("Cambio"),Color.blue));
		La_cambio.setFont(fuente_cobros);
		La_cambio.setForeground(Color.blue);
		Pa_cobrar.add(La_cambio);
		
	
		
		Pa_opc_derecha=new JPanel();
		Pa_opc_derecha.setBackground(azuloscuro);
		Pa_opc_derecha.setLayout(new GridBagLayout());
		File arch;
		if((arch=new File("resources/images/interfaz/papelera.png")).exists()){
			Icon icono=new ImageIcon("resources/images/interfaz/papelera.png");
			Bt_borrar=new JImageButton("Borrar",icono);
		}else{
			Bt_borrar=new JImageButton("Borrar");
		}
		Bt_borrar.setBackground(azulclaro);
		Bt_borrar.setForeground(marron);
		Bt_borrar.setFont(ft_bot_pequeno);
		Bt_borrar.addActionListener(this);
		gbc.gridx=0;
		gbc.gridy=0;
		gbc.gridwidth=GridBagConstraints.REMAINDER;
		gbc.gridheight=1;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1;
		gbc.weighty=0.3;
		Pa_opc_derecha.add(Bt_borrar,gbc);
		
		La_total=new JLabel("TOTAL:",JLabel.CENTER);
		gbc.gridx=4;
		gbc.gridy=7;
		gbc.gridwidth=1;
		gbc.gridheight=1;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=0.2;
		gbc.weighty=0.2;
		La_total.setForeground(new Color(0,0,255));
		La_total.setBackground(new Color(0,0,255));
		
		Bt_unidades=new JImageButton("Unidades");
		Bt_unidades.setFont(ft_bot_pequeno);
		Bt_unidades.setBackground(azulclaro);
		Bt_unidades.setForeground(marron);
		Bt_unidades.addActionListener(this);
		
		
		gbc.gridx=0;
		gbc.gridy=1;
		gbc.gridwidth=GridBagConstraints.REMAINDER;
		gbc.gridheight=1;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1;
		gbc.weighty=0.3;
		Pa_opc_derecha.add(Bt_unidades,gbc);
		
		
		Bt_precio=new JImageButton("Precio");
		Bt_precio.setFont(ft_bot_pequeno);
		Bt_precio.setBackground(azulclaro);
		Bt_precio.setForeground(marron);
		Bt_precio.addActionListener(this);
		gbc.gridx=0;
		gbc.gridy=2;
		gbc.gridwidth=GridBagConstraints.REMAINDER;
		gbc.gridheight=1;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1;
		gbc.weighty=0.3;
		Pa_opc_derecha.add(Bt_precio,gbc);
		
		
		Bt_invita=new JImageButton("Invita");
		Bt_invita.setFont(ft_bot_pequeno);
		Bt_invita.setBackground(azulclaro);
		Bt_invita.setForeground(marron);
		Bt_invita.addActionListener(this);
		gbc.gridx=0;
		gbc.gridy=3;
		gbc.gridwidth=GridBagConstraints.REMAINDER;
		gbc.gridheight=1;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1;
		gbc.weighty=0.3;
		Pa_opc_derecha.add(Bt_invita,gbc);
		
		Bt_cobrar=new JImageButton("Cobrando");
		Bt_cobrar.setFont(ft_bot_pequeno);
		Bt_cobrar.setBackground(Color.white);
		Bt_cobrar.addActionListener(this);	
		gbc.gridx=0;
		gbc.gridy=5;
		gbc.gridwidth=1;
		gbc.gridheight=GridBagConstraints.REMAINDER;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1;
		gbc.weighty=1;
		Pa_opc_derecha.add(Bt_cobrar,gbc);
		
		
	
		
		Bt_separa=new JImageButton("Separa");
		Bt_separa.setFont(ft_bot_pequeno);
		Bt_separa.setBackground(azulclaro);
		Bt_separa.addActionListener(this);
		gbc.gridx=0;
		gbc.gridy=4;
		gbc.gridwidth=1;
		gbc.gridheight=GridBagConstraints.RELATIVE;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1;
		gbc.weighty=1;
		Pa_opc_derecha.add(Bt_separa,gbc);
		
		Bt_juntar=new JImageButton("Juntar");
		Bt_juntar.setFont(ft_bot_pequeno);
		Bt_juntar.setBackground(azulclaro);
		Bt_juntar.setForeground(marron);
		Bt_juntar.addActionListener(this);
		gbc.gridx=1;
		gbc.gridy=4;
		gbc.gridwidth=GridBagConstraints.REMAINDER;
		gbc.gridheight=GridBagConstraints.RELATIVE;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1;
		gbc.weighty=1;
		Pa_opc_derecha.add(Bt_juntar,gbc);
		
		Bt_can_cobro = new JImageButton("<html><p align='center'>Cancelar</p><br><p align='center'>Cobro</p></html>");
		Bt_can_cobro.addActionListener(this);
		Bt_can_cobro.setBackground(azulclaro);
		Bt_can_cobro.setFont(ft_bot_pequeno);
		gbc.gridx=1;
		gbc.gridy=5;
		gbc.gridwidth=GridBagConstraints.REMAINDER;
		gbc.gridheight=GridBagConstraints.REMAINDER;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=0.15;
		gbc.weighty=1;
		Pa_opc_derecha.add(Bt_can_cobro,gbc);
		
		
		gbc.gridx=7;
		gbc.gridy=3;
		gbc.gridwidth=GridBagConstraints.REMAINDER;
		gbc.gridheight=GridBagConstraints.REMAINDER;
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=0.1;
		gbc.weighty=0.2;
		Pa_secundario.add(Pa_opc_derecha,gbc);
		
		
		
		
		/*
		 * Panel de opciones
		 * 
		 * 
		 */
		
		Pa_opciones=new JPanel(null);
		Pa_opciones.setBackground(azulclaro);
		Pa_opciones.setBounds(0, 0, 500, 140);
		
		Bt_add_camarero=new JImageButton("<html><p align='center'>Ingresar</p><p align='center'>Camarero</p></html>");
		Bt_add_camarero.setFont(ft_bot_general);
		Bt_add_camarero.setBackground(marron);
		Bt_add_camarero.setForeground(Color.white);
		Bt_add_camarero.addActionListener(this);
		Bt_add_camarero.setBounds(0,0,110,70);
		Pa_opciones.add(Bt_add_camarero);
		
		Bt_add_dependienta=new JImageButton("<html><p align='center'>Ingresar</p><p align='center'>Dependienta</p></html>");
		Bt_add_dependienta.setFont(ft_bot_pequeno);
		Bt_add_dependienta.setBackground(marron);
		Bt_add_dependienta.setForeground(Color.white);
		Bt_add_dependienta.addActionListener(this);
		Bt_add_dependienta.setBounds(0,70,110,70);
		Pa_opciones.add(Bt_add_dependienta);
		
		Bt_out_camarero=new JImageButton("<html><p align='center'>Sacar</p><p align='center'>Camarero</p></html>");
		Bt_out_camarero.setFont(ft_bot_general);
		Bt_out_camarero.setBackground(marron);
		Bt_out_camarero.setForeground(Color.white);
		Bt_out_camarero.addActionListener(this);
		Bt_out_camarero.setBounds(110,0,110,70);
		Pa_opciones.add(Bt_out_camarero);
		
		Bt_out_dependienta=new JImageButton("<html><p align='center'>Sacar</p><p align='center'>Dependienta</p></html>");
		Bt_out_dependienta.setFont(ft_bot_pequeno);
		Bt_out_dependienta.setBackground(marron);
		Bt_out_dependienta.setForeground(Color.white);
		Bt_out_dependienta.addActionListener(this);
		Bt_out_dependienta.setBounds(110,70,110,70);
		Pa_opciones.add(Bt_out_dependienta);
		
		Bt_config=new JImageButton("Configuraci�n");
		Bt_config.setFont(ft_bot_pequeno);
		Bt_config.setBackground(Color.white);
		Bt_config.setForeground(marron);
		Bt_config.addActionListener(this);
		Bt_config.setBounds(220,0,140,90);
		Pa_opciones.add(Bt_config);
		
		Bt_jefe=new JImageButton("");
		Bt_jefe.setFont(ft_bot_general);
		Bt_jefe.addActionListener(this);
		Bt_jefe.setBounds(220,90,140,50);
		Pa_opciones.add(Bt_jefe);
		
		Bt_salir=new JImageButton("Salir");
		Bt_salir.setFont(ft_bot_general);
		Bt_salir.setBackground(Color.white);
		Bt_salir.setForeground(marron);
		Bt_salir.addActionListener(this);
		Bt_salir.setBounds(360,0,140,140);
		Pa_opciones.add(Bt_salir);
		
		
		
		actualizar_grid();
		
		
	/*
	 * 
	 * 
	 * 
	 * 
	 * 	
	 */
		
		
		Calendar c=Calendar.getInstance();
		fecha = Integer.toString(c.get(Calendar.DATE))+"/";
		fecha += Integer.toString(c.get(Calendar.MONTH)+1)+"/";
		fecha += Integer.toString(c.get(Calendar.YEAR));
		
	}
	
	private void crearinterfaz(){
		
		Icono icon;
		BufferedImage bi;
		File arch;
		
		Pa_principal.setVisible(false);
		Pa_secundario.setVisible(true);
		
		
		/*
		 * Botones menu opciones
		 * 
		 */
		
		//Salir
		if((arch=new File("resources/images/interfaz/salir.png")).exists()){
			icon=(new Icono("resources/images/interfaz/salir.png"));
			try {
				bi=ImageIO.read(arch);
				Bt_salir.setBackgroundImage(icon.getScaledInstance(bi,Bt_salir.getWidth()-7, Bt_salir.getHeight(),RenderingHints.VALUE_INTERPOLATION_BILINEAR,true));	
			
				
				
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		//Jefe
		if((arch=new File("resources/images/interfaz/matricula.png")).exists()){
			icon=(new Icono("resources/images/interfaz/matricula.png"));
			try {
				bi=ImageIO.read(arch);
				Bt_jefe.setBackgroundImage(icon.getScaledInstance(bi,Bt_jefe.getWidth()-7, Bt_jefe.getHeight(),RenderingHints.VALUE_INTERPOLATION_BILINEAR,true));	
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		/*
		 * 
		 * Botones Interfaz panel secundario
		 * 
		 */
		
		//imprimir
		if((arch=new File("resources/images/interfaz/impresora.png")).exists()){
			icon=(new Icono("resources/images/interfaz/impresora.png"));
			try {
				bi=ImageIO.read(arch);
				Bt_imprimir.setBackgroundImage(icon.getScaledInstance(bi,Bt_imprimir.getWidth()-7, Bt_imprimir.getHeight(),RenderingHints.VALUE_INTERPOLATION_BILINEAR,true));	
			} catch (IOException e) {
				e.printStackTrace();
			}
		}	
		
		//cobrar
		if((arch=new File("resources/images/interfaz/iconocajaregistradora.jpg")).exists()){
			icon=(new Icono("resources/images/interfaz/iconocajaregistradora.jpg"));
			try {
				bi=ImageIO.read(arch);
				Bt_cobrar.setBackgroundImage(icon.getScaledInstance(bi,Bt_cobrar.getWidth()-7, Bt_cobrar.getHeight(),RenderingHints.VALUE_INTERPOLATION_BILINEAR,true));	
			} catch (IOException e) {
				e.printStackTrace();
			}
		}	
		
		//camarero
		if((arch=new File("resources/images/interfaz/camarero.png")).exists()){
			icon=(new Icono("resources/images/interfaz/camarero.png"));
			try {
				bi=ImageIO.read(arch);
				Bt_camarero.setBackgroundImage(icon.getScaledInstance(bi,Bt_camarero.getWidth()-7, Bt_camarero.getHeight()-30,RenderingHints.VALUE_INTERPOLATION_BILINEAR,true));	
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		//dependienta
		if((arch=new File("resources/images/interfaz/dependienta.png")).exists()){
			icon=(new Icono("resources/images/interfaz/dependienta.png"));
			try {
				bi=ImageIO.read(arch);
				Bt_camarera.setBackgroundImage(icon.getScaledInstance(bi,Bt_camarera.getWidth()-7, Bt_camarera.getHeight()-30,RenderingHints.VALUE_INTERPOLATION_BILINEAR,true));	
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		//cerrar panel secundario
		if((arch=new File("resources/images/interfaz/volver.png")).exists()){
			icon=(new Icono("resources/images/interfaz/volver.png"));
			try {
				bi=ImageIO.read(arch);
				Bt_cerrar2.setBackgroundImage(icon.getScaledInstance(bi,Bt_cerrar2.getWidth()-7, Bt_cerrar2.getHeight()-25,RenderingHints.VALUE_INTERPOLATION_BILINEAR,true));	
			} catch (IOException e) {
				e.printStackTrace();
			}
		}	
		
		//separa cuenta
		if((arch=new File("resources/images/interfaz/separacuenta.png")).exists()){
			icon=(new Icono("resources/images/interfaz/separacuenta.png"));
			try {
				bi=ImageIO.read(arch);
				Bt_separa.setBackgroundImage(icon.getScaledInstance(bi,Bt_separa.getWidth()-7, Bt_separa.getHeight(),RenderingHints.VALUE_INTERPOLATION_BILINEAR,true));	
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		
		//juntar cuenta
		if((arch=new File("resources/images/interfaz/juntarcuenta.png")).exists()){
			icon=(new Icono("resources/images/interfaz/juntarcuenta.png"));
			try {
				bi=ImageIO.read(arch);
				Bt_juntar.setBackgroundImage(icon.getScaledInstance(bi,Bt_juntar.getWidth()-7, Bt_juntar.getHeight(),RenderingHints.VALUE_INTERPOLATION_BILINEAR,true));	
				Bt_juntar.setForeground(marron);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		
		
		/*
		 * 
		 * Productos
		 * 
		 */
		
		
		int ancho=Bt_prod_cat[0][0].getWidth(),alto=Bt_prod_cat[0][0].getHeight();
		
		
		for(int j=0;j<Bt_prod_cat.length;j++){
				for(int i=0;i<Bt_prod_cat[j].length;i++){
					if(Bt_prod_cat[j][i].getText().equals("----->")){
						icon=(new Icono("resources/images/interfaz/flecha_derecha.jpg"));
						try {
							bi=ImageIO.read(new File("resources/images/interfaz/flecha_derecha.jpg"));
							Bt_prod_cat[j][i].setBackgroundImage(icon.getScaledInstance(bi,ancho-8, alto-3,RenderingHints.VALUE_INTERPOLATION_BILINEAR,true));	
						} catch (IOException e) {
							e.printStackTrace();
						}
					}else if(Bt_prod_cat[j][i].getText().equals("<-----")){
						icon=(new Icono("resources/images/interfaz/flecha_izquierda.jpg"));
						try {
							bi=ImageIO.read(new File("resources/images/interfaz/flecha_izquierda.jpg"));
							Bt_prod_cat[j][i].setBackgroundImage(icon.getScaledInstance(bi,ancho-8, alto-3,RenderingHints.VALUE_INTERPOLATION_BILINEAR,true));	
						} catch (IOException e) {
							e.printStackTrace();
						}
					}else{
						if((arch=new File("resources/images/productos/"+Bt_prod_cat[j][i].getText()+".jpg")).exists()){
							icon=(new Icono("resources/images/productos/"+Bt_prod_cat[j][i].getText()+".jpg"));
							try {
								bi=ImageIO.read(arch);
								Bt_prod_cat[j][i].setBackgroundImage(icon.getScaledInstance(bi,ancho-7, alto,RenderingHints.VALUE_INTERPOLATION_BILINEAR,false));	
								Bt_prod_cat[j][i].setForeground(Color.white);
								
								
							
								/*try {
									ImageIO.write(icon.getScaledInstance(bi,Bt_salir.getWidth()-7, alto,RenderingHints.VALUE_INTERPOLATION_BILINEAR,false), "jpg", new File("resources/images/productos/"+Bt_prod_cat[j][i].getText()+".jpg"));
								} catch (IOException e) {
									System.out.println("Error de escritura");
								}*/
								
								
							} catch (IOException e) {
								e.printStackTrace();
							}
						}else{
							Bt_prod_cat[j][i].setText("<html><p align='center'>"+Bt_prod_cat[j][i].getText().replace(" ","</p><p align='center'>")+"</p></html>");
							Bt_prod_cat[j][i].setFont(ft_bot_general);
						}
					}
					
				}
			
		}
		ancho=Bt_categoria[0].getWidth();alto=Bt_categoria[0].getHeight();
		for(int i=0;i<Bt_categoria.length;i++){
			if((arch=new File("resources/images/familias/"+Bt_categoria[i].getText()+".jpg")).exists()){
				icon=(new Icono("resources/images/familias/"+Bt_categoria[i].getText()+".jpg"));
				try{
					bi=ImageIO.read(arch);
					Bt_categoria[i].setBackgroundImage(icon.getScaledInstance(bi,ancho-12, alto-20,RenderingHints.VALUE_INTERPOLATION_BILINEAR,true));	
					Bt_categoria[i].setForeground(Color.white);
				} catch(Exception e){
					e.printStackTrace();
				}
				
			}
		}
		
		
		Pa_secundario.setVisible(false);
		Pa_principal.setVisible(true);
	}

	/*
	 * 
	 *  Conexi�n con la base de datos localhost postgresql
	 * 
	 * 
	 */
	private void db_connect() {
		Date dia_sesion_vieja;
		int dia=0;
		DecimalFormat formato=new DecimalFormat("##");
		String hoy;
		Calendar cal=Calendar.getInstance();
		String fech;
		fech = (cal.get(Calendar.YEAR))+"-";
		if(((cal.get(Calendar.MONTH)+1)+"").length()==1){
			fech +="0"+ formato.format(cal.get(Calendar.MONTH)+1)+"-";
		}else{
			fech += formato.format(cal.get(Calendar.MONTH)+1)+"-";
		}if(((cal.get(Calendar.DAY_OF_MONTH)+"")).length()==1){
			fech +="0"+ formato.format(cal.get(Calendar.DAY_OF_MONTH));
		}else{
			fech += formato.format(cal.get(Calendar.DAY_OF_MONTH));
		}
		try {
			conexion_db =DriverManager.getConnection("jdbc:postgresql://localhost:5432/tpv","tpv","tpv2015");
			//conexion_db =DriverManager.getConnection("jdbc:postgresql://192.168.1.102:5432/venecia","postgres","venecia2013");
		
			statement=conexion_db.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			statement2=conexion_db.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			statement3=conexion_db.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			
			query="select id,nombre from sesiones where activa=true";
			rs=statement.executeQuery(query);
			if(rs.first()){
				sesion_actual=rs.getInt(1);
				dia_sesion_vieja=rs.getDate("nombre");
				//dia_sesion_vieja.getTime().
				//System.out.println(dia_sesion_vieja+":"+fech+": dia_Sesion_vieja.dia= "+cal.get(Calendar.DATE));
				if(dia_sesion_vieja.toString().equals(fech)){
					//System.out.println("Dia actual = dia sesion");
				}else if(Calendar.getInstance().get(Calendar.HOUR_OF_DAY)<7){
					System.out.println("dia actual>dia sesion pero son menos de las 7 de la ma�ana");
				}else if(dia_sesion_vieja.after(Calendar.getInstance().getTime())){
					System.out.println("Sesion actual mayor que el d�a actual");
				}else{
				
					JOptionPane.showMessageDialog(null, "Para poder iniciar la aplicaci�n debe haberse cerrado la sesi�n del d�a actual.\nLa aplicaci�n se va a cerrar.");
					
					System.exit(0);
				}
			}else{
				query="insert into sesiones (activa) values ( true) returning id";
				rs=statement.executeQuery(query);
				rs.next();
				sesion_actual=rs.getInt(1);
			}
		}
		catch(Exception e){
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Error detectado, imposible establecer conexi�n con la base de datos.\nLa aplicaci�n se va a cerrar.");
			
			System.exit(0);
		}
	}
	
	/*
	 * 
	 * 
	 * 
	 * Funcion que lee la base de datos y prepara los paneles de menus y productos a mostrar
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	private void leer_tablas(){
		Pa_menu_categorias=new JPanel();
		String query;
		int n_categorias=0,n_prod=0,n_panels,ancho,alto;
		
		Dim_panel=new Dimension(15*29,15*23);
	// Obtenemos las categorias de seleccion de productos
	// y formamos el menu.
		try{
			query="SELECT Familias.Nombre FROM Categorias,Familias where Familias.Categoria=Categorias.ID and Categorias.Nombre='Productos' and Familias.id<>0";
			
			rs=statement.executeQuery(query);
			rs.getMetaData();
			while(rs.next()){
				n_categorias++;
			}
			System.out.println("");
			Bt_categoria=new JImageButton[n_categorias];
			if(n_categorias!=0){
				rs.first();
				Pa_menu_categorias.setLayout(new GridLayout(2,n_categorias/2+n_categorias%2));
				Pa_menu_categorias.setBackground(marron);
				for(int i=1;i<=n_categorias;i++){
					Bt_categoria[i-1]=new JImageButton(""+rs.getString(1));
					Bt_categoria[i-1].setFont(new Font("TimesRoman",Font.BOLD,20));
					Bt_categoria[i-1].addActionListener(this);
					Bt_categoria[i-1].setBackground(azuloscuro);
					Bt_categoria[i-1].setForeground(marron);
					Bt_categoria[i-1].setBorder(BorderFactory.createLineBorder(marron));
					Bt_categoria[i-1].setBorderPainted(pintar_bordes);
					Pa_menu_categorias.add(Bt_categoria[i-1]);
					rs.next();
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	//--------
		
	// Preparamos los paneles con los productos a mostrar
		
		try{
			/*
			 * Variables para manejar el grid de productos
			 * 
			 */
			ancho=7;
			alto=4;
			
			Pa_categorias=new JPanel[n_categorias][];
			Bt_prod_cat=new JCustomButton[n_categorias][];
			for(int j=0;j<n_categorias;j++){
				n_prod=0;
				query="SELECT productos.ID,productos.Nombre,productos.PrecioT,productos.PrecioB FROM productos,familias where productos.familia=familias.id and familias.nombre='"+Bt_categoria[j].getText()+"' order by productos.orden";
				rs=statement.executeQuery(query);
				while(rs.next()){
					n_prod++;
				}
				if(n_prod==0){
					n_panels=1;
				}else{
					n_panels=n_prod/(ancho*alto) +1 ; //36~20 ###################################### +1
				}
				Pa_categorias[j]=new JPanel[n_panels];
				for(int i=0;i<n_panels;i++){
					Pa_categorias[j][i]=new JPanel(new GridLayout(alto,ancho)); //6,6~4,5
					Pa_categorias[j][i].setBackground(azulclaro);
					Pa_categorias[j][i].setMaximumSize(Dim_panel);
					Pa_categorias[j][i].setMinimumSize(Dim_panel);
				}
					rs.first();
					if(n_panels==1){
						Bt_prod_cat[j]=new JCustomButton[(ancho*alto)*n_panels];
							for(int i=0;i<(ancho*alto);i++){
								if(i<n_prod){
									Bt_prod_cat[j][i]=new JCustomButton(rs.getString(2),rs.getDouble(3),rs.getDouble(4),rs.getInt(1),Bt_categoria[j].getText());
									Bt_prod_cat[j][i].setBackground(Color.white);
									Bt_prod_cat[j][i].addActionListener(this);
									rs.next();
								}else{
									Bt_prod_cat[j][i]=new JCustomButton("",0,0,0,"");
									Bt_prod_cat[j][i].setBackground(azulclaro);
									Bt_prod_cat[j][i].addActionListener(this);
								}
								Pa_categorias[j][0].add(Bt_prod_cat[j][i]);	
							}
					}else if(n_panels==2){
						Bt_prod_cat[j]=new JCustomButton[(ancho*alto)*n_panels];
						for(int i=0;i<(ancho*alto)*n_panels;i++){
							if(i==(ancho*alto)-1){
								Bt_prod_cat[j][i]=new JCustomButton("----->",0,0,0,"");
								Bt_prod_cat[j][i].setBackground(azulclaro);
								Bt_prod_cat[j][i].addActionListener(this);
							}else if(i==n_prod+1){
								Bt_prod_cat[j][i]=new JCustomButton("<-----",0,0,0,"");
								Bt_prod_cat[j][i].setBackground(azulclaro);
								Bt_prod_cat[j][i].addActionListener(this);
							}else if(i>n_prod+1){
								Bt_prod_cat[j][i]=new JCustomButton("",0,0,0,"");
								Bt_prod_cat[j][i].setBackground(azulclaro);
								Bt_prod_cat[j][i].addActionListener(this);
							}else{
								Bt_prod_cat[j][i]=new JCustomButton(rs.getString(2),rs.getDouble(3),rs.getDouble(4),rs.getInt(1),Bt_categoria[j].getText());
								Bt_prod_cat[j][i].setBackground(Color.white);
								Bt_prod_cat[j][i].addActionListener(this);
								rs.next();
							}
							if(i<(ancho*alto)){
								Pa_categorias[j][0].add(Bt_prod_cat[j][i]);
							}else{
								Pa_categorias[j][1].add(Bt_prod_cat[j][i]);
							}	
						}
					}else {
						Bt_prod_cat[j]=new JCustomButton[(ancho*alto)*n_panels];
						for(int i=0;i<(ancho*alto)*n_panels;i++){
							if((i+1)%(ancho*alto)==0 && i<n_panels*(ancho*alto)-(ancho*alto)){ //&& i!=n_prod+2+(n_panels-2)*2
								Bt_prod_cat[j][i]=new JCustomButton("----->",0,0,0,"");
								Bt_prod_cat[j][i].setBackground(azulclaro);
								Bt_prod_cat[j][i].addActionListener(this);
							}else if((i+ancho)%((ancho*alto))==0 && Math.ceil((double)i/(double)(ancho*alto))!=n_panels && i>(ancho*alto)-1){ //30~15
								Bt_prod_cat[j][i]=new JCustomButton("<-----",0,0,0,"");
								Bt_prod_cat[j][i].setBackground(azulclaro);
								Bt_prod_cat[j][i].addActionListener(this);
							}else if(i==(n_prod+2+(n_panels-2)*2)-1){
								Bt_prod_cat[j][i]=new JCustomButton("<-----",0,0,0,"");
								Bt_prod_cat[j][i].setBackground(azulclaro);
								Bt_prod_cat[j][i].addActionListener(this);
							}else if(i>=n_prod+2+(n_panels-2)*2-1){
								Bt_prod_cat[j][i]=new JCustomButton("",0,0,0,"");
								Bt_prod_cat[j][i].setBackground(azulclaro);
								Bt_prod_cat[j][i].addActionListener(this);
							}else{
								Bt_prod_cat[j][i]=new JCustomButton(rs.getString(2),rs.getDouble(3),rs.getDouble(4),rs.getInt(1),Bt_categoria[j].getText());
								Bt_prod_cat[j][i].setBackground(Color.white);
								Bt_prod_cat[j][i].addActionListener(this);
								rs.next();
							}
							Pa_categorias[j][i/(ancho*alto)].add(Bt_prod_cat[j][i]);
						}
					}
					
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}	
	//------ Paneles de productos listos
	}
	
//------ Lista enlazada de botones supletorios, para uso en caso de mesas especiales

	/*
	 * 
	 * Devuelve true si encuentra el boton con indice idx en la lista
	 * 
	 */
	private boolean buscar_en_lista(int idx,boolean sit){
		boolean continuar=true,resultado=false;
		JButtonEnlazado current=lista;
		if(current!=null){
			while (continuar){
				if(current.get_index()==idx && sit==current.get_sit()){
					resultado=true;
					continuar=false;
				}else if(current.get_siguiente()==null){
					continuar=false;
				}else{
				
					current=current.get_siguiente();
				}
			}
		}
		return resultado;
	}
	
	
	/*
	 * 
	 * Permite a�adir en la lista un nuevo boton con indice idx
	 * 
	 */
	private JButtonEnlazado anadir_en_lista(int idx, boolean sit){
		current=lista;
		boolean continuar=true;
		if(lista==null){
			if(Pa_terraza.isVisible()){
				lista=new JButtonEnlazado("Mesa "+idx,null,idx,sit);
			}else{
				lista=new JButtonEnlazado("Barra "+idx,null,idx,sit);
			}
			
			lista.addActionListener(this);
			Pa_inferior.add(lista);
			current=lista;
		}else{
			while(continuar){
				if(current.get_siguiente()==null){
					if(Pa_terraza.isVisible()){
						current.set_siguiente(new JButtonEnlazado("Mesa "+idx,current,idx,sit));
					}else{
						current.set_siguiente(new JButtonEnlazado("Barra "+idx,current,idx,sit));
					}
					current.get_siguiente().addActionListener(this);
					Pa_inferior.add(current.get_siguiente());
					continuar=false;
				}
				current=current.get_siguiente();	
			}
		}
		Sc_inferior.repaint();
		return current;
	}

	/*
	 * 
	 * Elimina de la lista lst el boton con indice indx
	 * 
	 */
	private void eliminar_de_lista(int indx, JButtonEnlazado lst,boolean sit){
		JButtonEnlazado aux;
		boolean salir=false;
		aux=lst;
		do{
			if(aux.get_index()==indx && aux.get_sit()==sit){
				if(aux.get_siguiente()==null && aux.get_anterior()==null){
						lst=null;
						Pa_inferior.remove(aux);
						salir=true;
							
				}else if(aux.get_anterior()==null){
						lst=aux.get_siguiente();
						salir=true;
						Pa_inferior.remove(aux);
				
				}else if(aux.get_siguiente()==null){
						aux.get_anterior().set_siguiente(null);
						salir=true;
						Pa_inferior.remove(aux);
					
				}else {
					aux.get_anterior().set_siguiente(aux.get_siguiente());
					aux.get_siguiente().set_anterior(aux.get_anterior());
					Pa_inferior.remove(aux);
					salir=true;
					
				}
			}
		
			aux=aux.get_siguiente();
		}while(aux!=null && salir==false);
		
		
	}
	
	

//------- fin lista enlazada 
	
	/*
	 * 
	 * 
	 *  M�todo para actualizar el grid de las mesas.
	 * 
	 * 
	 * 
	 * 
	 */	
	private void actualizar_grid(){
		JButtonEnlazado aux=lista;
		Boolean encontrado=false;
		try{
			
			/*
			 * Actualizando grid Barra
			 * 
			 * 
			 */
			
			query="select mesas.nombre,mesas.id,mesas.estado from mesas,situacion where mesas.tipo=situacion.id and situacion.nombre='Barra' order by id";
			rs=statement.executeQuery(query);
			while(rs.next()){
				if(rs.getInt(1)<50 && rs.getInt(1)>0){
					if(rs.getString(3).equals("a")){
						query="select importe from pedidos where mesa="+rs.getInt(2)+" and horac is null";
						rs2=statement2.executeQuery(query);
						if(rs2.first()){
							Bt_barras[rs.getInt(1)-1].setText("<html><p align='center'>Barra "+rs.getInt(1)+"</p><p align='center'>Venta "+df.format(rs2.getDouble(1))+"</p></html>");
							Bt_barras[rs.getInt(1)-1].setBackground(mesaabierta);
							Bt_barras[rs.getInt(1)-1].setForeground(Color.white);
							Bt_barras[rs.getInt(1)-1].setBorder(BorderFactory.createLineBorder(Color.white));
							Bt_barras[rs.getInt(1)-1].setBorderPainted(pintar_bordes);
						}else{
							Bt_barras[rs.getInt(1)-1].setText("<html><p align='center'>Barra "+rs.getInt(1)+"</p></html>");
							Bt_barras[rs.getInt(1)-1].setBackground(azulclaro);
							Bt_barras[rs.getInt(1)-1].setForeground(marron);
							Bt_barras[rs.getInt(1)-1].setBorder(BorderFactory.createLineBorder(azuloscuro));
							Bt_barras[rs.getInt(1)-1].setBorderPainted(pintar_bordes);
						}
					}else if(rs.getString(3).equals("u")){
						Bt_barras[rs.getInt(1)-1].setText("<html><p align='center'>Barra "+rs.getInt(1)+"</p></html>");
						Bt_barras[rs.getInt(1)-1].setBackground(Color.red);
						Bt_barras[rs.getInt(1)-1].setForeground(Color.white);
						Bt_barras[rs.getInt(1)-1].setBorder(BorderFactory.createLineBorder(azuloscuro));
						Bt_barras[rs.getInt(1)-1].setBorderPainted(pintar_bordes);
					}else if(rs.getString(3).equals("c")){
						Bt_barras[rs.getInt(1)-1].setText("<html><p align='center'>Barra "+rs.getInt(1)+"</p></html>");
						Bt_barras[rs.getInt(1)-1].setBackground(azulclaro);
						Bt_barras[rs.getInt(1)-1].setForeground(marron);
						Bt_barras[rs.getInt(1)-1].setBorder(BorderFactory.createLineBorder(azuloscuro));
						Bt_barras[rs.getInt(1)-1].setBorderPainted(pintar_bordes);
					}
				}else{
					
					
					encontrado=false;
					
					if(rs.getString(3).equals("c")){
						if(buscar_en_lista(rs.getInt(1),false)){
							eliminar_de_lista(rs.getInt(1),lista,false);
						}
					}else{
					
						if(lista==null){
							anadir_en_lista(rs.getInt(1), false);
						}
						
						aux=lista;
						while(aux!=null && encontrado==false){
							if(aux.get_sit()==false){
								if(aux.get_index()==rs.getInt(1)){
									if(rs.getString(3).equals("a")){
										query="select importe from pedidos where mesa="+rs.getInt(2)+" and horac is null";
										rs2=statement2.executeQuery(query);
										if(rs2.first()){
											aux.setText("<html><p align='center'>Barra "+rs.getInt(1)+"</p><br> <p align='center'>Venta "+df.format(rs2.getDouble(1))+"</p></html>");
											aux.setBackground(Color.WHITE);
										}else{
											eliminar_de_lista(rs.getInt(1),lista,false);
										}
									}else if (rs.getString(3).equals("u")){
										aux.setText("<html><p align='center'>Barra "+rs.getInt(1)+"</p></html>");
										aux.setBackground(Color.RED);
									}
									encontrado=true;
								}
							}
							if (aux.get_siguiente()==null && encontrado==false){
								anadir_en_lista(rs.getInt(1), false);
							}
							aux=aux.get_siguiente();
							
						}
					}
					
				}
			}
			
			
			/*
			 * Actualizando grid Terraza.
			 * 
			 * 
			 */
			
			
			query="select mesas.nombre,mesas.id,mesas.estado from mesas,situacion where mesas.tipo=situacion.id and situacion.nombre='Terraza' and mesas.id<>0 order by nombre";
			rs=statement.executeQuery(query);
			while(rs.next()){
				if(rs.getInt(1)<50 && rs.getInt(1)>0){
					if(rs.getString(3).equals("a")){
						query="select importe from pedidos where mesa="+rs.getInt(2)+" and horac is null";
						rs2=statement2.executeQuery(query);
						if(rs2.first()){
							Bt_mesas[rs.getInt(1)-1].setText("<html><p align='center'>Mesa "+rs.getInt(1)+"</p> <p align='center'>Venta "+df.format(rs2.getDouble(1))+"</p></html>");
							Bt_mesas[rs.getInt(1)-1].setBackground(mesaabierta);
							Bt_mesas[rs.getInt(1)-1].setForeground(Color.white);
							Bt_mesas[rs.getInt(1)-1].setBorder(BorderFactory.createLineBorder(Color.white));
							Bt_mesas[rs.getInt(1)-1].setBorderPainted(pintar_bordes);
						}else{
							Bt_mesas[rs.getInt(1)-1].setText("<html><p align='center'>Mesa "+rs.getInt(1)+"</p></html>");
							Bt_mesas[rs.getInt(1)-1].setBackground(azulclaro);
							Bt_mesas[rs.getInt(1)-1].setForeground(marron);
							Bt_mesas[rs.getInt(1)-1].setBorder(BorderFactory.createLineBorder(marron));
							Bt_mesas[rs.getInt(1)-1].setBorderPainted(pintar_bordes);
						}
					}else if(rs.getString(3).equals("u")){
						Bt_mesas[rs.getInt(1)-1].setText("<html><p align='center'>Mesa "+rs.getInt(1)+"</p></html>");
						Bt_mesas[rs.getInt(1)-1].setBackground(Color.red);
						Bt_mesas[rs.getInt(1)-1].setForeground(Color.white);
						Bt_mesas[rs.getInt(1)-1].setBorder(BorderFactory.createLineBorder(azuloscuro));
						Bt_mesas[rs.getInt(1)-1].setBorderPainted(pintar_bordes);
					}else if(rs.getString(3).equals("c")){
						Bt_mesas[rs.getInt(1)-1].setText("<html><p align='center'>Mesa "+rs.getInt(1)+"</p></html>");
						Bt_mesas[rs.getInt(1)-1].setBackground(azulclaro);
						Bt_mesas[rs.getInt(1)-1].setForeground(marron);
						Bt_mesas[rs.getInt(1)-1].setBorder(BorderFactory.createLineBorder(marron));
						Bt_mesas[rs.getInt(1)-1].setBorderPainted(pintar_bordes);
					}
				}else{
					
					encontrado=false;
					
					if(rs.getString(3).equals("c")){
						if(buscar_en_lista(rs.getInt(1),true)){
							eliminar_de_lista(rs.getInt(1),lista,true);
						}
					}else{
					
						if(lista==null){
							anadir_en_lista(rs.getInt(1), true);
						}
						
						aux=lista;
						while(aux!=null && encontrado==false){
							if(aux.get_sit()==true){
								if(aux.get_index()==rs.getInt(1)){
									if(rs.getString(3).equals("a")){
										query="select importe from pedidos where mesa="+rs.getInt(2)+" and horac is null";
										rs2=statement2.executeQuery(query);
										if(rs2.first()){
											aux.setText("<html><p align='center'>Mesa "+rs.getInt(1)+"</p><br> <p align='center'>Venta "+df.format(rs2.getDouble(1))+"</p></html>");
											aux.setBackground(Color.WHITE);
										}else{
											eliminar_de_lista(rs.getInt(1),lista,true);
										}
									}else if (rs.getString(3).equals("u")){
										aux.setText("<html><p align='center'>Mesa "+rs.getInt(1)+"</p></html>");
										aux.setBackground(Color.RED);
									}
									encontrado=true;
								}
							}
							if (aux.get_siguiente()==null && encontrado==false){
								anadir_en_lista(rs.getInt(1), true);
							}
							aux=aux.get_siguiente();
							
						}
					}
				}
			}
			
		
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	//------- Actualizar datos referentes a las mesas

	/*
	 * 
	 * 
	 * 	M�todo para actualizar la lista de productos de la mesa actual. 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	private void actualizar_lista_prod() throws Exception{
		int anchotabla=Lst_prod.getWidth();
		int altotabla=Lst_prod.getHeight();
		int cantidad_borrados=0;
		
		Lst_prod.setMaximumSize(new Dimension(anchotabla,altotabla));
		Lst_prod.setMinimumSize(new Dimension(anchotabla,altotabla));
		
		ids_lista_prod.clear();
		row="";
		lista_prod.clear();
		query="select servicios_temp.id,servicios_temp.descripcion,servicios_temp.cantidad,servicios_temp.precio from servicios_temp where servicios_temp.pedido="+current_pedido+" order by id";
		rs=statement.executeQuery(query);
		ids_lista_prod.addElement("");
		lista_prod.addElement("<html> <table width=320><tr><td width=14% align='center'>CANT.</td><td width=50%>DESCRIPCION</td><td width=16% align='center'>PRECIO</td><td width=20% align='center'>IMPORTE</td></tr></table></html>");
		if(rs.first()){
			do{
				query="select servicios_temp_borrados.cantidad from servicios_temp,servicios_temp_borrados where servicio="+rs.getInt(1);
				rs2=statement2.executeQuery(query);
				cantidad_borrados=0;
				if(rs2.first()){
					cantidad_borrados=rs2.getInt(1);
				}
				
				if(rs2.first() && rs2.getInt(1)-rs.getInt(3)==0){
					
				}else{
				
					ids_lista_prod.addElement(""+rs.getInt(1));
					row="<html><table width=320><tr><td width=14% align='center'>"+(rs.getInt(3)-cantidad_borrados)+"</td>";
					row=row+"<td width=50%>"+rs.getString(2)+"</td>";
					row=row+"<td width=16% align='center'>"+df.format(rs.getDouble(4))+"</td>";
					row=row+"<td width=20% align='center'>"+df.format((rs.getInt(3)-cantidad_borrados)*rs.getDouble(4))+"</td></tr></table></html>";
					lista_prod.addElement(row);
					
					if(situacion.regionMatches(0, "Terraza", 0, 7)){
						query="select productos.nombre,productos.preciot from productos, modificaciones_temp where modificaciones_temp.modificacion=productos.id and modificaciones_temp.servicio="+rs.getInt(1);
						
					}else{
						query="select productos.nombre,productos.preciob from productos, modificaciones_temp where modificaciones_temp.modificacion=productos.id and modificaciones_temp.servicio="+rs.getInt(1);
					}
					rs2=statement2.executeQuery(query);
					if(rs2.first()){
						ids_lista_prod.addElement("");
						row="<html><table width=320><tr><td width=14% align='center'>+"+(rs.getInt(3)-cantidad_borrados)+"</td>";
						row=row+"<td width=50%>"+rs2.getString(1)+"</td>";
						row=row+"<td width=16% align='center'>"+df.format(rs2.getDouble(2))+"</td></tr></table></html>";	
						row=row+"<td width=20% align='center'>"+df.format((rs.getInt(3)-cantidad_borrados)*rs2.getDouble(2))+"</td></tr></table></html>";
						lista_prod.addElement(row);
						while(rs2.next()){
							ids_lista_prod.addElement("");
							row="<html><table width=320><tr><td width=14% align='center'>+"+(rs.getInt(3)-cantidad_borrados)+"</td>";
							row=row+"<td width=50%>"+rs2.getString(1)+"</td>";
							row=row+"<td width=16% align='center'>"+df.format(rs2.getDouble(2))+"</td></tr></table></html>";	
							row=row+"<td width=20% align='center'>"+df.format((rs.getInt(3)-cantidad_borrados)*rs2.getDouble(2))+"</td></tr></table></html>";
							lista_prod.addElement(row);
						}
					}
				}
			}while(rs.next());	
		}else{
			lista_prod.addElement("");
		}		
		Lst_prod.setListData(lista_prod);
	}
	
	
	
	/*
	 * 
	 * Metodo para actualizar la lista con el pedido indicado por pedido.
	 * 
	 */
	
	private void actualizar_lista_prod(int pedido, JList lista) throws Exception{
		int anchotabla=lista.getWidth();
		int altotabla=lista.getHeight();
		int cantidad_borrados=0;
		Vector<String> lista_p;
		
		lista.setMaximumSize(new Dimension(anchotabla,altotabla));
		lista.setMinimumSize(new Dimension(anchotabla,altotabla));
		lista_p=new Vector<String>(1);
	
		row="";
		lista_p.clear();
		query="select servicios_temp.id,servicios_temp.descripcion,servicios_temp.cantidad,servicios_temp.precio from servicios_temp where servicios_temp.pedido="+pedido+" order by id";
		rs=statement.executeQuery(query);
		lista_p.addElement("<html> <table width=320><tr><td width=14% align='center'>Cant.</td><td width=50%>Descripcion</td><td width=16% align='center'>Precio</td><td width=20% align='center'>Importe</td></tr></table></html>");
		if(rs.first()){
			do{
				query="select servicios_temp_borrados.cantidad from servicios_temp,servicios_temp_borrados where servicio="+rs.getInt(1);
				rs2=statement2.executeQuery(query);
				cantidad_borrados=0;
				if(rs2.first()){
					cantidad_borrados=rs2.getInt(1);
				}
				
				if(rs2.first() && rs2.getInt(1)-rs.getInt(3)==0){
					
				}else{
				
				
					row="<html><table width=320><tr><td width=14% align='center'>"+(rs.getInt(3)-cantidad_borrados)+"</td>";
					row=row+"<td width=50%>"+rs.getString(2)+"</td>";
					row=row+"<td width=16% align='center'>"+df.format(rs.getDouble(4))+"</td>";
					row=row+"<td width=20% align='center'>"+df.format((rs.getInt(3)-cantidad_borrados)*rs.getDouble(4))+"</td></tr></table></html>";
					lista_p.addElement(row);
					
					if(situacion.regionMatches(0, "Terraza", 0, 7)){
						query="select productos.nombre,productos.preciot from productos, modificaciones where modificaciones.modificacion=productos.id and modificaciones.servicio="+rs.getInt(1);
						
					}else{
						query="select productos.nombre,productos.preciob from productos, modificaciones where modificaciones.modificacion=productos.id and modificaciones.servicio="+rs.getInt(1);
					}
					rs2=statement2.executeQuery(query);
					if(rs2.first()){
						row="<html><table width=320><tr><td width=14% align='center'>+"+(rs.getInt(3)-cantidad_borrados)+"</td>";
						row=row+"<td width=50%>"+rs2.getString(1)+"</td>";
						row=row+"<td width=16% align='center'>"+df.format(rs2.getDouble(2))+"</td></tr></table></html>";	
						row=row+"<td width=20% align='center'>"+df.format((rs.getInt(3)-cantidad_borrados)*rs2.getDouble(2))+"</td></tr></table></html>";
						lista_p.addElement(row);
						while(rs2.next()){
							row="<html><table width=320><tr><td width=14% align='center'>+"+(rs.getInt(3)-cantidad_borrados)+"</td>";
							row=row+"<td width=50%>"+rs2.getString(1)+"</td>";
							row=row+"<td width=16% align='center'>"+df.format(rs2.getDouble(2))+"</td></tr></table></html>";	
							row=row+"<td width=20% align='center'>"+df.format((rs.getInt(3)-cantidad_borrados)*rs2.getDouble(2))+"</td></tr></table></html>";
							lista_p.addElement(row);
						}
					}
				}
			}while(rs.next());	
		}else{
			lista_p.addElement("");
		}		
		lista.setListData(lista_p);
	}

	

	 /*
	  * 
	  * M�todo para actualizar el importe total de la mesa actual
	  * 
	  * 
	  * 
	  */
	
	private void actualizar_importe() throws Exception{
			importe=0;
			double precio_modificaciones=0;
			int cantidad_borrados=0;
			
			query="select servicios_temp.id,servicios_temp.descripcion,servicios_temp.cantidad,servicios_temp.precio from servicios_temp where servicios_temp.pedido="+current_pedido+" order by id";

			rs=statement.executeQuery(query);
			while(rs.next()){	
				query="select servicios_temp_borrados.cantidad from servicios_temp,servicios_temp_borrados where servicio="+rs.getInt(1);
				rs2=statement2.executeQuery(query);
				cantidad_borrados=0;
				if(rs2.first()){
					cantidad_borrados=rs2.getInt(1);
				}
				if(rs2.first() && rs2.getInt(1)-rs.getInt(3)==0){
					
				}else{
					if(situacion.regionMatches(0, "Terraza", 0, 7)){
						query="select productos.preciot from productos,modificaciones_temp where modificaciones_temp.servicio="+rs.getInt(1)+" and modificaciones_temp.modificacion=productos.id";
					}else{
						query="select productos.preciob from productos,modificaciones_temp where modificaciones_temp.servicio="+rs.getInt(1)+" and modificaciones_temp.modificacion=productos.id";
					}
					rs2=statement2.executeQuery(query);
					precio_modificaciones=0;
					while(rs2.next()){
						precio_modificaciones=precio_modificaciones+((rs.getInt(3)-cantidad_borrados)*rs2.getDouble(1));
					}
					importe=importe+precio_modificaciones+((rs.getInt(3)-cantidad_borrados)*rs.getDouble(4));
				}
			}
			query="update pedidos set importe="+importe+" where id="+current_pedido;
			statement.executeUpdate(query);
			La_importe.setText(""+df.format(importe));
		}
	 
	 
	 
	 /*
	  * 
	  * Actualiza el importe del pedido indicado por pedido.
	  * 
	  */
	private double actualizar_importe(int pedido) throws Exception{
			double importe=0;
			double precio_modificaciones=0;
			int cantidad_borrados=0;
			
			query="select servicios_temp.id,servicios_temp.descripcion,servicios_temp.cantidad,servicios_temp.precio from servicios_temp where servicios_temp.pedido="+pedido+" order by id";

			rs=statement.executeQuery(query);
			while(rs.next()){	
				query="select servicios_temp_borrados.cantidad from servicios_temp,servicios_temp_borrados where servicio="+rs.getInt(1);
				rs2=statement2.executeQuery(query);
				cantidad_borrados=0;
				if(rs2.first()){
					cantidad_borrados=rs2.getInt(1);
				}
				if(rs2.first() && rs2.getInt(1)-rs.getInt(3)==0){
					
				}else{
					if(situacion.regionMatches(0, "Terraza", 0, 7)){
						query="select productos.preciot from productos,modificaciones_temp where modificaciones_temp.servicio="+rs.getInt(1)+" and modificaciones_temp.modificacion=productos.id";
					}else{
						query="select productos.preciob from productos,modificaciones_temp where modificaciones_temp.servicio="+rs.getInt(1)+" and modificaciones_temp.modificacion=productos.id";
					}
					rs2=statement2.executeQuery(query);
					precio_modificaciones=0;
					while(rs2.next()){
						precio_modificaciones=precio_modificaciones+((rs.getInt(3)-cantidad_borrados)*rs2.getDouble(1));
					}
					importe=importe+precio_modificaciones+((rs.getInt(3)-cantidad_borrados)*rs.getDouble(4));
				}
			}
			query="update pedidos set importe="+importe+" where id="+pedido;
			statement.executeUpdate(query);
			return importe;
		}

	 
	 /*
	 * 
	 * 
	 * 
	 * Obtiene camarero y dependienta correspondiente a la mesa
	 * 
	 * 
	 * 
	 */
	 
	private void actualizar_duenos(){
	 		query="SELECT camarero,dependienta FROM pedidos WHERE mesa="+current_mesa+" AND HORAC IS NULL";
	 		try{
	 			rs=statement.executeQuery(query);
	 			if(rs.first()){
	 				if(rs.getInt("camarero")!=0){
	 					query="select empleados.nombre,empleados.identificador from empleados,jornadas where jornadas.id="+rs.getInt(1)+" and jornadas.empleado=empleados.id ";
	 					rs2=statement2.executeQuery(query);
	 					rs2.first();
	 					Bt_camarero.setText(""+rs2.getInt(2)+"");
	 					Bt_camarero.setEnabled(false);
	 				}else{
	 					sacar=false;
	 					if(Bt_camarero.isEnabled()){
	 						seleccionar_camarero(0);
	 					}
	 					
	 				}
	 				if(rs.getInt("dependienta")!=0){
	 					query="select empleados.nombre,empleados.identificador from empleados,jornadas where jornadas.id="+rs.getInt(2)+" and jornadas.empleado=empleados.id ";
	 					rs2=statement2.executeQuery(query);
	 					rs2.first();
	 					Bt_camarera.setText(""+rs2.getInt(2)+"");
	 					Bt_camarera.setEnabled(false);
	 				}else{
	 					sacar=false;
	 					if(Bt_camarera.isEnabled()){
	 						seleccionar_camarero(1);
	 					}
	 					
	 				}
	 			}
	 		}catch (Exception e){
	 			e.printStackTrace();
	 		}
	 	
	 }
	 
	 
	 
	 /*
	 * 
	 * 
	 * M�todo para actualizar los datos de una mesa al abrirla. 
	 * Actualiza lista_prod, importe, due�os
	 * 
	 * 
	 */

	private void actualizar_datos(){
			
			try{

				actualizar_lista_prod();
				actualizar_importe();
				actualizar_duenos();
				actualizar_duenos();
				
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
	
	//-------------
		
	 
	 
	 
		
	/*
	 * 
	 * M�todo para borrar un producto de la mesa.
	 * 
	 * 
	 * 
	 */
		
	private void borrar_productos(){
		
		int index=Lst_prod.getSelectedIndex();
		query="select servicios.id,servicios.cantidad from servicios where servicios.pedido="+current_pedido+" except ( select  servicio,servicios_borrados.cantidad from servicios_borrados,servicios where servicios_borrados.servicio=servicios.id and servicios.cantidad=servicios_borrados.cantidad ) order by id";
		try{
			rs=statement.executeQuery(query);
			rs.absolute(index);
			
			query="select cantidad from servicios_borrados where servicio="+rs.getInt(1)+" and tiempo="+tiempo_mesa;
			rs2=statement2.executeQuery(query);
			if(!rs2.first()){
				query="insert into servicios_borrados (servicio,cantidad,tiempo) values ("+rs.getInt(1)+",1,"+tiempo_mesa+")";
				statement.execute(query);
			}else{
				if(rs.getInt(2)!=rs2.getInt(1)){
					query="update servicios_borrados set cantidad=cantidad+1 where servicio="+rs.getInt(1)+" and tiempo="+tiempo_mesa;
					statement.executeUpdate(query);
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		actualizar_datos();
		Lst_prod.setSelectedIndex(index);
	}
	
	private void borrar_producto(){
		
		int index=Lst_prod.getSelectedIndex();
		if(index==-1){
			index=Lst_prod.getModel().getSize()-1;
		}
		try{
			if(!ids_lista_prod.elementAt(index).equals("")){
				query="select id from servicios_temp_borrados where servicio="+ids_lista_prod.elementAt(index)+" and tiempo="+tiempo_mesa;
				rs=statement.executeQuery(query);
			
				if(!rs.first()){
					query="insert into servicios_temp_borrados (servicio,cantidad,tiempo) values ("+ids_lista_prod.elementAt(index)+",1,"+tiempo_mesa+")";
					statement.execute(query);
				}else{
					query="update servicios_temp_borrados set cantidad=cantidad+1 where id="+rs.getInt(1);
					statement.executeUpdate(query);
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		actualizar_datos();
		Lst_prod.setSelectedIndex(index);
	}
	
	
	
	
	/*
	 * 
	 *  M�todo para cobrar una mesa
	 * 
	 * 
	 * 
	 */
	
	private void cobrar_mesa(){
		
		double entrega;
		String hora;
		
		if(Bt_cifra.getText()!=""){
			entrega=Double.parseDouble(Bt_cifra.getText());
		}else{
			entrega=importe;
		}
		if(entrega>=importe){	
		
			La_entrega.setText(df.format(entrega));
			La_cambio.setText(df.format(entrega-importe));
			Pa_cobrar.setVisible(true);
			Sc_prod.setVisible(false);
			if(tiquet==true){
				imprimir(entrega,(entrega-importe));
			}
			try{
				hora=""+new java.sql.Timestamp((new java.util.Date()).getTime());
				query="update pedidos set horac='"+hora+"' where id="+current_pedido;
				statement.executeUpdate(query);
				
				query="insert into cobros (pedido,entrega,sesion,caja) values ("+current_pedido+","+entrega+","+sesion_actual+","+obj_conf.numerocaja+")";	
				statement.executeUpdate(query);
				
				query="select * from servicios_temp where pedido="+current_pedido;
				rs=statement.executeQuery(query);
				while(rs.next()){
					query="insert into servicios (id,producto,descripcion,cantidad,precio,pedido,tiempo) values ("+rs.getInt(1)+","+rs.getInt(2)+",'"+rs.getString(3)+"',"+rs.getInt(4)+","+rs.getInt(5)+","+rs.getInt(6)+","+rs.getInt(7)+")";
					statement2.execute(query);
					query="select * from modificaciones_temp where servicio="+rs.getInt(1);
					rs2=statement2.executeQuery(query);
					while(rs2.next()){
						query="insert into modificaciones (id,servicio,modificacion) values ("+rs2.getInt(1)+","+rs2.getInt(2)+","+rs2.getInt(3)+")";
						statement3.execute(query);
						query="delete from modificaciones_temp where id="+rs2.getInt(1);
						statement3.execute(query);
					}
					query="delete from servicios_temp where id="+rs.getInt(1);
					statement2.execute(query);
					query="select * from servicios_temp_borrados where servicio="+rs.getInt(1);
					rs2=statement2.executeQuery(query);
					while(rs2.next()){
						query="insert into servicios_borrados (id,servicio,cantidad,tiempo) values ("+rs2.getInt(1)+","+rs2.getInt(2)+","+rs2.getInt(3)+","+rs2.getInt(4)+")";
						statement3.execute(query);
						query="delete from servicios_temp_borrados where id="+rs2.getInt(1);
						statement3.execute(query);
					}
				}
				
				
				actualizar_lista_prod();
				actualizar_duenos();
			
				generar_pedido();
				//actualizar_datos();
			}catch (Exception e){
				e.printStackTrace();
			}
		}else{
			JOptionPane.showMessageDialog(null, "La cantidad introducida es menor\nque el importe total de la cuenta");
		}
		Bt_cifra.setText("");
		tiquet=true;
		Bt_tiquet.setText("Tiquet: Si");
	}
	
	private void cobrar_mesa(double ent){
		
		double entrega;
		String hora;
		
		entrega=ent;
		if(entrega>=importe){	
		
			La_entrega.setText(df.format(entrega));
			La_cambio.setText(df.format(entrega-importe));
			Pa_cobrar.setVisible(true);
			Sc_prod.setVisible(false);
			if(tiquet==true){
				imprimir(entrega,(entrega-importe));
			}
			try{
				hora=""+new java.sql.Timestamp((new java.util.Date()).getTime());
				query="update pedidos set horac='"+hora+"' where id="+current_pedido;
				statement.executeUpdate(query);
				
				query="insert into cobros (pedido,entrega,sesion,caja) values ("+current_pedido+","+entrega+","+sesion_actual+","+obj_conf.numerocaja+")";	
				statement.executeUpdate(query);
				
				
				query="select * from servicios_temp where pedido="+current_pedido;
				rs=statement.executeQuery(query);
				while(rs.next()){
					query="insert into servicios (id,producto,descripcion,cantidad,precio,pedido,tiempo) values ("+rs.getInt(1)+","+rs.getInt(2)+","+rs.getString(3)+","+rs.getInt(4)+","+rs.getInt(5)+","+rs.getInt(6)+","+rs.getInt(7)+")";
					statement2.execute(query);
					query="select * from modificaciones_temp where servicio="+rs.getInt(1);
					rs2=statement2.executeQuery(query);
					while(rs2.next()){
						query="insert into modificaciones (id,servicio,modificacion) values ("+rs2.getInt(1)+","+rs2.getInt(2)+","+rs2.getInt(3)+")";
						statement3.execute(query);
						query="delete from modificaciones_temp where id="+rs2.getInt(1);
						statement3.execute(query);
					}
					query="delete from servicios_temp where id="+rs.getInt(1);
					statement2.execute(query);
					query="select * from servicios_temp_borrados where servicio="+rs.getInt(1);
					rs2=statement2.executeQuery(query);
					while(rs2.next()){
						query="insert into servicios_borrados (id,servicio,cantidad,tiempo) values ("+rs2.getInt(1)+","+rs2.getInt(2)+","+rs2.getInt(3)+","+rs2.getInt(4)+")";
						statement3.execute(query);
						query="delete from servicios_temp_borrados where id="+rs2.getInt(1);
						statement3.execute(query);
					}
				}
				
				
				
				actualizar_lista_prod();
				actualizar_duenos();
			
				generar_pedido();
				//actualizar_datos();
			}catch (Exception e){
				e.printStackTrace();
			}
		}else{
			JOptionPane.showMessageDialog(null, "La cantidad introducida es menor\nque el importe total de la cuenta");
		}
		Bt_cifra.setText("");
		tiquet=true;
		Bt_tiquet.setText("Tiquet: Si");
	}
	
	
	private void cancelar_cobro(){
		final JDialog canc_cobro=new JDialog(this,"Cancelar cobro",true);
		canc_cobro.setSize(485,170);
		canc_cobro.setResizable(false);
		canc_cobro.setLocationRelativeTo(null);
		canc_cobro.setLayout(null);
		canc_cobro.getContentPane().setBackground(azuloscuro);
		
		ActionListener listener;
		final JImageButton bt_cifra[]=new JImageButton[10];
		
		JLabel la_cancel_cob=new JLabel("Numero pedido:");
		la_cancel_cob.setFont(ft_bot_general);
		la_cancel_cob.setForeground(Color.blue);
		
		final JImageButton bt_num_canc=new JImageButton("");
		bt_num_canc.setFont(ft_numeros);
		bt_num_canc.setBackground(azulclaro);
		bt_num_canc.setForeground(marron);
		
		final JButton bt_cancel_cobro=new JButton("<html><p align='center'>Cancelar</p><p align='center'>Cobro</p></html>");
		bt_cancel_cobro.setFont(ft_bot_general);
		bt_cancel_cobro.setBackground(azulclaro);
		bt_cancel_cobro.setForeground(Color.blue);
		
		final JButton bt_ult_cobro=new JButton("<html><p align='center'>Ultimo</p><p align='center'>Cobro</p></html>");
		bt_ult_cobro.setFont(ft_bot_pequeno);
		bt_ult_cobro.setBackground(azulclaro);
		bt_ult_cobro.setForeground(Color.blue);
		
		listener=new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				boolean encontrado=false;
				int i=0;
				
					
				if(ev.getSource()==bt_num_canc){
					bt_num_canc.setText("");
				}else if(ev.getSource()==bt_cancel_cobro){
					
					
					/*
					 * 
					 * 
					 * Cancelar cobro, creando pedido nuevo, 
					 * 
					 * 
					 * 
					 * 
					 */
					int cobro_id,mesa_id=0,pedido_id=0,servicio_id;
					
					query="select id from cobros where pedido="+bt_num_canc.getText();
					try{
						rs=statement.executeQuery(query);
						if(rs.first()){
							cobro_id=rs.getInt(1);
							query="insert into cobroscancelados (cobro) values ("+cobro_id+")";
							statement.execute(query);
							
							/*query="select id,nombre from mesas where nombre > 49 and estado='c' and tipo='1' order by nombre limit 1";
							try{
								rs=statement.executeQuery(query);
								rs.first();
								mesa_id=rs.getInt(1);
							}catch (Exception e){
								e.printStackTrace();
							}
							
							query="update mesas set estado='a' where id="+mesa_id;
							try{
								statement.executeUpdate(query);
							}catch (Exception e){
								e.printStackTrace();
							}
							// copiar productos al pedido nuevo, y contruir el pedido nuevo
							
							
							query="select camarero,dependienta,importe,horaa,tiempo from pedidos where id="+bt_num_canc.getText();
							try{
								rs=statement.executeQuery(query);
								rs.first();
								query="insert into pedidos (mesa,camarero,dependienta,importe,horaa,tiempo) values ("+mesa_id+","+rs.getObject(1)+","+rs.getInt(2)+","+rs.getDouble(3)+",'"+rs.getString(4)+"',"+rs.getInt(5)+") returning id";
								rs=statement.executeQuery(query);
								rs.next();
								pedido_id=rs.getInt("Id");
							}catch (Exception e){
								
								e.printStackTrace();
							}
							
							query="select * from servicios where pedido="+bt_num_canc.getText();
							try{
								rs=statement.executeQuery(query);
								if(rs.first()){
									query="insert into servicios_temp (producto,descripcion,cantidad,precio,pedido,tiempo) values ("+rs.getInt(2)+",'"+rs.getString(3)+"',"+rs.getInt(4)+","+rs.getDouble(5)+","+pedido_id+","+rs.getInt(6)+") returning id";
									rs2=statement2.executeQuery(query);
									rs2.next();
									servicio_id=rs2.getInt(1);
									
									query="select * from modificaciones where servicio="+rs.getInt(1);
									rs2=statement2.executeQuery(query);
								
									while(rs2.next()){
										query="insert into modificaciones_temp (servicio,modificacion) values ("+servicio_id+","+rs2.getInt(3)+")";
										statement3.execute(query);
									}
									
									
									query="select * from modificaprecios where servicio="+rs.getInt(1);
									rs2=statement2.executeQuery(query);
									if(rs2.first()){
										query="insert into modificaprecios (servicio,precio) values ("+servicio_id+","+rs2.getDouble(3)+")";
										statement3.execute(query);
									}
									
									
									while(rs.next()){
										query="insert into servicios_temp (producto,descripcion,cantidad,precio,pedido,tiempo) values ("+rs.getInt(2)+",'"+rs.getString(3)+"',"+rs.getInt(4)+","+rs.getDouble(5)+","+pedido_id+","+rs.getInt(6)+") returning ID";
										rs2=statement2.executeQuery(query);
										rs2.next();
										servicio_id=rs2.getInt(1);
										
										query="select * from modificaciones where servicio="+rs.getInt(1);
										rs2=statement2.executeQuery(query);
									
										while(rs2.next()){
											query="insert into modificaciones_temp (servicio,modificacion) values ("+servicio_id+","+rs2.getInt(3)+")";
											statement3.execute(query);
										}
										
										query="select * from modificaprecios where servicio="+rs.getInt(1);
										rs2=statement2.executeQuery(query);
										if(rs2.first()){
											query="insert into modificaprecios (servicio,precio) values ("+servicio_id+","+rs2.getDouble(3)+")";
											statement3.execute(query);
										}
									}
								}
								
								JOptionPane.showMessageDialog(null, "Cobro cancelado.\nEsta operaci�n queda registrada en la base de datos.");
							}catch (Exception e){
								e.printStackTrace();
							}*/
							JOptionPane.showMessageDialog(null, "Cobro cancelado.\nEsta operaci�n queda registrada en la base de datos.");
							
							
						}else{
							JOptionPane.showMessageDialog(null, "El pedido introducido no existe.");
						}
					}catch (Exception e){
						e.printStackTrace();
					}
					
					canc_cobro.dispose();
					
				}else if(ev.getSource()==bt_ult_cobro){
					query="select pedido from cobros order by id desc limit 1";
					try{
						rs=statement.executeQuery(query);
						rs.first();
						bt_num_canc.setText(""+rs.getInt(1));
					}catch(Exception e){
						e.printStackTrace();
					}
				}else{
					while(!encontrado && i<10){
						if(ev.getSource()==bt_cifra[i]){
							bt_num_canc.setText(bt_num_canc.getText()+bt_cifra[i].getText());
						}
						i++;
					}
				}
			}};
		
		for(int i=0;i<10;i++){
			bt_cifra[i]=new JImageButton(""+i);
			bt_cifra[i].addActionListener(listener);
			bt_cifra[i].setFont(ft_numeros);
			bt_cifra[i].setBackground(Color.white);
			bt_cifra[i].setForeground(marron);
			bt_cifra[i].setBounds((0+i*48),0, 48, 50);
			canc_cobro.add(bt_cifra[i]);
		}
		la_cancel_cob.setBounds(20, 60, 150, 20);
		canc_cobro.add(la_cancel_cob);
		
		bt_num_canc.addActionListener(listener);
		bt_num_canc.setBounds(20,80,150,50);
		canc_cobro.add(bt_num_canc);
		
		bt_ult_cobro.addActionListener(listener);
		bt_ult_cobro.setBounds(180,80,100,50);
		canc_cobro.add(bt_ult_cobro);
		
		bt_cancel_cobro.addActionListener(listener);
		bt_cancel_cobro.setBounds(330,60,130,70);
		canc_cobro.add(bt_cancel_cobro);
		
		canc_cobro.setVisible(true);
	}
	
	
	/*
	 * 
	 * 	M�todo para abrir una mesa. Hace visible el panel secundario y carga en el 
	 * los datos de la mesa.
	 * 
	 * 
	 * 
	 * 
	 */
	
	private void abrir_mesa(int a){
		String sit;
	
		if(a==1){
			sit="Terraza";
		}else if(a==2){
			sit="Barra";
			Bt_camarero.setEnabled(false);
		}else{
			if(Pa_terraza.isVisible()){
				sit="Terraza";
			}else{
				sit="Barra";
				Bt_camarero.setEnabled(false);
			}
		}
		
		
		try{
			
			query="begin transaction;";
			statement.execute(query);
			query="lock table mesas in access exclusive mode ;";
			statement.execute(query);
			query="select mesas.id,mesas.estado from mesas,situacion where mesas.nombre="+n_mesa+" and mesas.tipo=situacion.id and situacion.nombre='"+sit+"' for update";
			
			rs=statement.executeQuery(query);
			if(rs.first() && !rs.getString(2).equals("u")){
				current_mesa=rs.getInt(1);
				query="update mesas set estado='u' where id="+current_mesa+";";
				statement.executeUpdate(query);
				query="commit;";
				statement.execute(query);
				query="end;";
				statement.execute(query);
				situacion=sit;
				
				query="SELECT pedidos.id,pedidos.tiempo FROM pedidos WHERE mesa="+current_mesa+" AND HORAC IS NULL";
				rs=statement.executeQuery(query);
				if(!rs.first()){
					query="INSERT INTO pedidos (mesa,tiempo) VALUES ("+current_mesa+",0) ";
					statement.execute(query);
					query="SELECT pedidos.id FROM pedidos WHERE mesa="+current_mesa+" AND HORAC IS NULL";
					rs=statement.executeQuery(query);
					rs.first();
					current_pedido=rs.getInt(1);
					tiempo_mesa=0;
				}else{
					current_pedido=rs.getInt(1);
					tiempo_mesa=rs.getInt(2)+1;
					query="Update pedidos set tiempo="+tiempo_mesa+" where id="+current_pedido;
					statement.executeUpdate(query);
				}
				if(juntando==true){
					query="update servicios_temp set pedido="+current_pedido+" ,tiempo="+tiempo_mesa+" where pedido="+pedido_juntar;
					statement.executeUpdate(query);
					query="select camarero,dependienta from pedidos where id="+pedido_juntar;
					rs=statement.executeQuery(query);
					rs.next();
					query="update pedidos set camarero="+rs.getInt(1)+" ,dependienta="+rs.getInt(2)+" where id="+current_pedido;
					statement.executeUpdate(query);
					query="delete from pedidos where id="+pedido_juntar;
					statement.execute(query);
					juntando=false;
				}
				
				
				if(situacion.equals("Terraza")){
					Bt_barra2.setText("<html><p align='center'>Barra</p></html>");
					Bt_terraza2.setText("<html><p align='center'>Terraza</p><p align='center'>"+n_mesa+"</p></html>");
				}else{			
					Bt_terraza2.setText("<html><p align='center'>Terraza<p></html>");
					Bt_barra2.setText("<html><p align='center'>Barra</p><p align='center'>"+n_mesa+"</p></html>");
				}
				
				Pa_principal.setVisible(false);
				Pa_secundario.setVisible(true);
				Sc_prod.setVisible(true);
				Pa_cobrar.setVisible(false);
				actualizar_datos();
				
				
			}else if(!rs.first()){
				situacion=sit;
				query="insert into mesas (tipo,nombre,estado) values ((select id from situacion where nombre='"+situacion+"'),"+n_mesa+",'c')";
				statement.execute(query);
				if(situacion.equals("Terraza")){
					abrir_mesa(1);
				}else{
					abrir_mesa(2);
				}
			}else if(rs.getString(2).equals("u")){
				JOptionPane.showMessageDialog(this, "La mesa est� abierta por otro usuario!!!!");
			}
		
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	
	
	
	/*
	 * 
	 * 
	 *  Cierra una mesa
	 * 
	 * 
	 * 
	 * 
	 */
	
	private void cerrar_mesa(){

		Bt_camarero.setText("Camarero");
		Bt_camarero.setEnabled(true);
		Bt_camarera.setText("Dependienta");
		Bt_camarera.setEnabled(true);
		tiquet=true;
		Bt_tiquet.setText("Tiquet: Si");
		
		
		query="SELECT * FROM servicios_temp where pedido="+current_pedido;
		try{
			rs=statement.executeQuery(query);
			if(!rs.next()){
				query="DELETE FROM pedidos WHERE id="+current_pedido;
				statement.execute(query);
				
				query="update mesas set estado='c' where id="+current_mesa;
				statement.executeUpdate(query);
				
			}else if(importe==0){
				query="insert into servicios (id,producto,descripcion,cantidad,precio,pedido,tiempo) values ("+rs.getInt(1)+","+rs.getInt(2)+",'"+rs.getString(3)+"',"+rs.getInt(4)+","+rs.getInt(5)+","+rs.getInt(6)+","+rs.getInt(7)+")";
				statement2.execute(query);
				query="select * from modificaciones_temp where servicio="+rs.getInt(1);
				rs2=statement2.executeQuery(query);
				while(rs2.next()){
					query="insert into modificaciones (id,servicio,modificacion) values ("+rs2.getInt(1)+","+rs2.getInt(2)+","+rs2.getInt(3)+")";
					statement3.execute(query);
					query="delete from modificaciones_temp where id="+rs2.getInt(1);
					statement3.execute(query);
				}
				query="delete from servicios_temp where id="+rs.getInt(1);
				statement2.execute(query);
				query="select * from servicios_temp_borrados where servicio="+rs.getInt(1);
				rs2=statement2.executeQuery(query);
				while(rs2.next()){
					query="insert into servicios_borrados (id,servicio,cantidad,tiempo) values ("+rs2.getInt(1)+","+rs2.getInt(2)+","+rs2.getInt(3)+","+rs2.getInt(4)+")";
					statement3.execute(query);
					query="delete from servicios_temp_borrados where id="+rs2.getInt(1);
					statement3.execute(query);
				}
				while(rs.next()){
					query="insert into servicios (id,producto,descripcion,cantidad,precio,pedido,tiempo) values ("+rs.getInt(1)+","+rs.getInt(2)+",'"+rs.getString(3)+"',"+rs.getInt(4)+","+rs.getInt(5)+","+rs.getInt(6)+","+rs.getInt(7)+")";
					statement2.execute(query);
					query="select * from modificaciones_temp where servicio="+rs.getInt(1);
					rs2=statement2.executeQuery(query);
					while(rs2.next()){
						query="insert into modificaciones (id,servicio,modificacion) values ("+rs2.getInt(1)+","+rs2.getInt(2)+","+rs2.getInt(3)+")";
						statement3.execute(query);
						query="delete from modificaciones_temp where id="+rs2.getInt(1);
						statement3.execute(query);
					}
					query="delete from servicios_temp where id="+rs.getInt(1);
					statement2.execute(query);
					query="select * from servicios_temp_borrados where servicio="+rs.getInt(1);
					rs2=statement2.executeQuery(query);
					while(rs2.next()){
						query="insert into servicios_borrados (id,servicio,cantidad,tiempo) values ("+rs2.getInt(1)+","+rs2.getInt(2)+","+rs2.getInt(3)+","+rs2.getInt(4)+")";
						statement3.execute(query);
						query="delete from servicios_temp_borrados where id="+rs2.getInt(1);
						statement3.execute(query);
					}
				}
				query="update pedidos set horac='"+new java.sql.Timestamp((new java.util.Date()).getTime())+"' where id="+current_pedido;
				statement.executeUpdate(query);
				query="insert into pedidos_borrados (pedido) values ("+current_pedido+")";
				statement.execute(query);
				query="update mesas set estado='c' where id="+current_mesa;
				statement.executeUpdate(query);
			}else{
				if(juntando==false){
					query="update mesas set estado='a' where id="+current_mesa;
				}else{
					query="update mesas set estado='c' where id="+current_mesa;
				}
				statement.executeUpdate(query);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		
	
		Pa_secundario.setVisible(false);
		Pa_principal.setVisible(true);
		actualizar_grid();
	
	}
	
	
	
	
	/*
	 * 
	 * 
	 * 
	 * 	Muestra el men� de opciones.
	 * 
	 * 
	 * 
	 * 
	 */
	
	private void menu_opciones(){
		menu_op=new JDialog(this,"Opciones",true);
		menu_op.setSize(500,170);
		menu_op.setResizable(false);
		menu_op.setLocationRelativeTo(null);
		menu_op.setLayout(null);
		menu_op.getContentPane().setBackground(azulclaro);
		
		Pa_opciones.setVisible(true);
		menu_op.add(Pa_opciones);
		menu_op.setVisible(true);
	}
	
	
	
	/*
	 * 
	 * Muestra la lista de camareros. Permite a�adir camareros a la tabla de 
	 * currentcamareros.
	 * 
	 * 
	 * 
	 * 
	 */
	
	private void anadir_camarero(int a){
		if(a==0){
			menu_camareros=new JDialog(this,"A�adir camarero",true);
		}else{
			menu_camareros=new JDialog(this,"A�adir dependienta",true);
		}
		menu_camareros.setSize(715,530);//715,500
		menu_camareros.setResizable(false);
		menu_camareros.setLocationRelativeTo(null);
		menu_camareros.setLayout(null);
		menu_camareros.getContentPane().setBackground(azuloscuro);
		
		File arch;
		BufferedImage bi;
		Icono icon;
		
		Bt_sig_pan_cam=new JImageButton("----->");
		Bt_sig_pan_cam.setBackground(Color.white);
		if((arch=new File("resources/images/interfaz/flecha_derecha.jpg")).exists()){
			icon=(new Icono("resources/images/interfaz/flecha_derecha.jpg"));
			try {
				bi=ImageIO.read(arch);
				Bt_sig_pan_cam.setBackgroundImage(icon.getScaledInstance(bi,90, 90,RenderingHints.VALUE_INTERPOLATION_BILINEAR,true));	
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		Bt_sig_pan_cam.addActionListener(this);
		Bt_sig_pan_cam.setBounds(608, 400, 100, 100);
		menu_camareros.add(Bt_sig_pan_cam);
		
		
		Bt_ant_pan_cam=new JImageButton("<-----");
		Bt_ant_pan_cam.setBackground(Color.white);
		if((arch=new File("resources/images/interfaz/flecha_izquierda.jpg")).exists()){
			icon=(new Icono("resources/images/interfaz/flecha_izquierda.jpg"));
			try {
				bi=ImageIO.read(arch);
				Bt_ant_pan_cam.setBackgroundImage(icon.getScaledInstance(bi,90, 90,RenderingHints.VALUE_INTERPOLATION_BILINEAR,true));	
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		Bt_ant_pan_cam.addActionListener(this);
		Bt_ant_pan_cam.setBounds(0,400,100,100);
		menu_camareros.add(Bt_ant_pan_cam);
		
		
		int n_pans=0;
		
		
		try{
			if(a==0){
				query="select empleados.id,empleados.nombre,empleados.identificador from empleados,departamentos where empleados.departamento=departamentos.id and departamentos.nombre='Camareros' and empleados.id<>0 except (select empleados.id,empleados.nombre,empleados.identificador from empleados,jornadas where empleados.id=jornadas.empleado and jornadas.salida is null)";
			}else{
				query="select empleados.id,empleados.nombre,empleados.identificador from empleados,departamentos where empleados.departamento=departamentos.id and departamentos.nombre='Dependientas' and empleados.id<>0 except (select empleados.id,empleados.nombre,empleados.identificador from empleados,jornadas where empleados.id=jornadas.empleado and jornadas.salida is null)";	
			}
			rs=statement.executeQuery(query);
		
			if(a==0){
				n_cam=0;
				while(rs.next()){
					n_cam++;
				}
				n_pans=(n_cam/56)+1;
				n_pans_camareros=n_pans;
				Pa_camareros=new JPanel[n_pans];
				Sc_camareros=new JScrollPane[n_pans];
				for(int i=0;i<n_pans;i++){
					Pa_camareros[i]=new JPanel(null);
					Pa_camareros[i].setPreferredSize(new Dimension(700,1000));

					Pa_camareros[i].setBackground(azulclaro);
					
					Sc_camareros[i]=new JScrollPane(Pa_camareros[i]);
					Sc_camareros[i].setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
					Sc_camareros[i].setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
					Sc_camareros[i].setBounds(0,0,710,400);
					Sc_camareros[i].setVisible(false);
					menu_camareros.add(Sc_camareros[i]);
				}
				
				n_pans=0;
				n_cam=0;
				if(rs.first()){
					lista_camareros=new JButtonEnlazado("<html><p align='center'>"+rs.getString(2)+"</p><p align='center'>"+rs.getInt(3)+"</p></html>",null,rs.getInt(1));
					current=lista_camareros;
					current.setFont(ft_bot_general);
					current.setBackground(azuloscuro);
					current.setForeground(marron);
					
					lista_camareros.setBounds(0, 0, 100, 125);
					current.addActionListener(this);
					Pa_camareros[0].add(current);
					n_cam++;
					while(rs.next()){
						current.set_siguiente(new JButtonEnlazado("<html><p align='center'>"+rs.getString(2)+"</p><p align='center'>"+rs.getInt(3)+"</p></html>",current,rs.getInt(1)));
						current=current.get_siguiente();
						current.setFont(ft_bot_general);
						current.setBackground(azuloscuro);
						current.setForeground(marron);
						if(n_cam==0){
							x=0;
							y=0;
						}else{
							x=((n_cam%7))*100;
							y=((n_cam/7))*125;
						}
						current.setBounds(x, y, 100, 125);
						current.addActionListener(this);
						Pa_camareros[n_pans].add(current);
						if(n_cam==55){
							n_pans++;
							n_cam=-1;
						}
						n_cam++;
					}
				}
				Sc_camareros[0].setVisible(true);
				menu_camareros.setVisible(true);
				
				
			
			}else{
				n_dep=0;
				while(rs.next()){
					n_dep++;
				}
				n_pans=(n_dep/56)+1;
				n_pans_dependientas=n_pans;
				
				Pa_dependientas=new JPanel[n_pans];
				Sc_dependientas=new JScrollPane[n_pans];
				for(int i=0;i<n_pans;i++){
					Pa_dependientas[i]=new JPanel(null);
					Pa_dependientas[i].setPreferredSize(new Dimension(700,1000));
					Pa_dependientas[i].setBackground(azulclaro);
					
					Sc_dependientas[i]=new JScrollPane(Pa_dependientas[i]);
					Sc_dependientas[i].setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
					Sc_dependientas[i].setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
					Sc_dependientas[i].setBounds(0,0,710,400);
					Sc_dependientas[i].setVisible(false);
					menu_camareros.add(Sc_dependientas[i]);
				}
				
				n_pans=0;
				n_dep=0;
				if(rs.first()){
					lista_dependientas=new JButtonEnlazado("<html><p align='center'>"+rs.getString(2)+"</p><p align='center'>"+rs.getInt(3)+"</p></html>",null,rs.getInt(1));
					current=lista_dependientas;
					current.setFont(ft_bot_general);
					current.setBackground(azuloscuro);
					current.setForeground(marron);
					
					lista_dependientas.setBounds(0, 0, 100, 125);
					current.addActionListener(this);
					Pa_dependientas[0].add(current);
					n_dep++;
					while(rs.next()){
						current.set_siguiente(new JButtonEnlazado("<html><p align='center'>"+rs.getString(2)+"</p><p align='center'>"+rs.getInt(3)+"</p></html>",current,rs.getInt(1)));
						current=current.get_siguiente();
						current.setFont(ft_bot_general);
						current.setBackground(azuloscuro);
						current.setForeground(marron);
						
						if(n_dep==0){
							x=0;
							y=0;
						}else{
							x=((n_dep%7))*100;
							y=((n_dep/7))*125;
						}
						current.setBounds(x, y, 100, 125);
						current.addActionListener(this);
						Pa_dependientas[n_pans].add(current);
						if(n_dep==55){
							n_pans++;
							n_dep=-1;
						}
						n_dep++;
					}
				}
				Sc_dependientas[0].setVisible(true);
				menu_camareros.setVisible(true);
				
				
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	
	
	/*
	 * 
	 * Inserta un pedido en la lista de invitaciones. 
	 * 
	 * 
	 */
	
	private void invitar (){
		String hora;
		query="insert into invitaciones (pedido) values ("+current_pedido+")";
		try{
			statement.execute(query);
			hora=""+new java.sql.Timestamp((new java.util.Date()).getTime());
			query="update pedidos set horac='"+hora+"' where id="+current_pedido;
			statement.executeUpdate(query);
			
			if(situacion.equals("Terraza")){
				Bt_mesas[n_mesa-1].setText("Mesa "+n_mesa);
				Bt_mesas[n_mesa-1].setBackground(null);
			}else {
				Bt_barras[n_mesa-1].setText("Barra "+n_mesa);
				Bt_barras[n_mesa-1].setBackground(null);
			}
			generar_pedido();
			actualizar_datos();
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	
	/*
	 * 
	 * 
	 * Genera un nuevo pedido en caso de separar cuentas.
	 * 
	 * 
	 */
	
	private void generar_pedido(){
		try{
			query="select mesa,camarero,dependienta,importe,tiempo from pedidos where id="+current_pedido;
			rs=statement.executeQuery(query);
			rs.first();
			//System.out.println("Camarero: "+rs.getObject("camarero"));
			if(separando){
				query="insert into pedidos (mesa,camarero,dependienta,importe,tiempo) values ("+rs.getInt(1)+","+rs.getObject(2)+","+rs.getInt(3)+","+rs.getDouble(4)+","+rs.getInt(5)+")";
			}else{
			//query="insert into pedidos (mesa,camarero,dependienta,importe,tiempo) values ("+rs.getInt(1)+","+rs.getObject(2)+","+rs.getInt(3)+","+rs.getDouble(4)+","+rs.getInt(5)+")";
				query="insert into pedidos (mesa,importe,tiempo) values ("+rs.getInt(1)+","+rs.getDouble(4)+","+rs.getInt(5)+")";
			}	
			statement.execute(query);
			
			query="select pedidos.id,situacion.nombre from pedidos,situacion,mesas where mesa="+current_mesa+" and mesas.id="+current_mesa+" and mesas.tipo=situacion.id and horac is null order by horaa desc";
			rs=statement.executeQuery(query);
			rs.first();
			current_pedido=rs.getInt(1);
			System.out.println(rs.getString("nombre"));
			if(rs.getString("nombre").equals("Terraza")){
				Bt_camarero.setEnabled(true);
			}
			Bt_camarera.setEnabled(true);
			
		
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	/*
	 * 
	 * Permite asignar un numero entero como cantidad de un producto.
	 * 
	 */
	
	private void establecer_cantidad(int unidades){
		
		
		int index=Lst_prod.getSelectedIndex();
		int ya_borrados=0,a_sumar=0,servicio,borrados=0,cantidad_total,actual;
		int cant_objetivo=0;
		cant_objetivo=Integer.parseInt(Bt_cifra.getText());
		
		if(index>0){
			
			try{
				query="select cantidad from servicios_temp where id="+ids_lista_prod.elementAt(index);
				rs=statement.executeQuery(query);
				rs.first();
				cantidad_total=rs.getInt(1);
				
				query="select sum(cantidad) as borrados from servicios_temp_borrados where servicio="+ids_lista_prod.elementAt(index);
				rs2=statement2.executeQuery(query);
				rs2.first();
				borrados=rs2.getInt(1);
				actual=cantidad_total-borrados;
				
				if(cant_objetivo-actual>0){
					if(cant_objetivo-actual<borrados){
						query="select id,cantidad from servicios_temp_borrados where servicio="+ids_lista_prod.elementAt(index);
						rs=statement.executeQuery(query);
						while(rs.next()){
							if(cant_objetivo-actual-ya_borrados>=rs.getInt(2)){
								ya_borrados=cant_objetivo-actual-rs.getInt(2);
								query="delete from servicios_temp_borrados where id="+rs.getInt(1);
								statement.execute(query);
							}else{
								query="update servicios_temp_borrados set cantidad=cantidad-"+(cant_objetivo-actual-ya_borrados)+" where id="+rs.getInt(1);
								statement.executeUpdate(query);
								break;
							}
						}
						
					}else if(cant_objetivo-actual>=borrados){
						a_sumar=cant_objetivo-actual-borrados;
						query="delete from servicios_temp_borrados where servicio="+ids_lista_prod.elementAt(index);
						statement.execute(query);
						query="update servicios_temp set cantidad=cantidad+"+a_sumar+" where id="+ids_lista_prod.elementAt(index);
						statement.executeUpdate(query);
					}
				}else if(cant_objetivo-actual<0){
					query="select id from servicios_temp_borrados where servicio="+ids_lista_prod.elementAt(index)+" and tiempo="+tiempo_mesa;
					rs=statement.executeQuery(query);
					if(rs.first()){
						query="update servicios_temp_borrados set cantidad=cantidad+"+(actual-cant_objetivo)+" where id="+rs.getInt(1);
						statement.executeUpdate(query);
					}else{
						query="insert into servicios_temp_borrados (servicio,cantidad,tiempo) values ("+ids_lista_prod.elementAt(index)+","+(actual-cant_objetivo)+","+tiempo_mesa+")";
						statement.execute(query);
					}
				}
				
				/*if(!rs2.first() && a_borrar!=0){
					query="insert into servicios_borrados (servicio,cantidad) values ("+servicio+","+a_borrar+")";
					statement.execute(query);
				}else{
					if(unidades<(cantidad-borrado)){
						query="update servicios_borrados set cantidad=cantidad+"+(-unidades-borrado+cantidad)+" where servicio="+servicio;
						statement.executeUpdate(query);
					}else if(unidades>(cantidad-borrado)){
						if(borrado>=a_sumar){
							if(borrado==a_sumar){
								query="delete from servicios_borrados where servicio="+servicio;
								statement.execute(query);
							}else{
								query="update servicios_borrados set cantidad=cantidad-"+a_sumar+" where servicio="+servicio;
								statement.executeUpdate(query);
							}
						}else if(borrado<a_sumar){
							a_sumar=a_sumar-borrado;
							query="delete from servicios_borrados where servicio="+servicio;
							statement.execute(query);
							query="update servicios set cantidad=cantidad+"+a_sumar+" where id="+servicio;
							statement.executeUpdate(query);
						}
					}
					
				}*/
			}catch (Exception e){
				e.printStackTrace();
			}
			actualizar_datos();
			Lst_prod.setSelectedIndex(index);
		}
	}
	
	
	private void modif_precio(Double nuevo_precio){
		int index=Lst_prod.getSelectedIndex();
		double precio;
		int servicio;

		if(index>0){
			query="select servicios_temp.id,servicios_temp.precio from servicios_temp,servicios_temp_borrados where servicios_temp.pedido="+current_pedido+" except (select servicio,servicios_temp_borrados.cantidad from servicios_temp_borrados,servicios_temp where servicios_temp_borrados.servicio=servicios_temp.id and servicios_temp.cantidad=servicios_temp_borrados.cantidad ) order by id";
	
			try{
				rs=statement.executeQuery(query);
				
				rs.absolute(index);
				
				servicio=rs.getInt(1);
				precio=rs.getInt(2);
				
				
				
				if(precio!=nuevo_precio){
					/*query="select modificaprecios.id from modificaprecios where servicio="+servicio;
					rs=statement.executeQuery(query);
					if(rs.first()){
						query="Update modificaprecios set precio="+nuevo_precio+" where id="+rs.getInt(1);
						statement2.executeUpdate(query);
					}else{
						query="insert into modificaprecios (servicio,precio) values ("+servicio+","+nuevo_precio+")";
						statement2.execute(query);
					}*/
					query="update servicios_temp set precio="+nuevo_precio+" where id="+servicio;
					statement.executeUpdate(query);
				}
			}catch (Exception e){
				e.printStackTrace();
			}
			actualizar_datos();
			Lst_prod.setSelectedIndex(index);
		}
	}
	
	
	/*
	 * 
	 * 
	 * 
	 * Muestra una lista seleccionable de camareros y dependientas .
	 * 
	 * 
	 * 
	 * 
	 */
	
	private void seleccionar_camarero(int a){
		if(a==0){
			seleccion=new JDialog(this,"Selecci�n de Camarero",true);
		}else{
			seleccion=new JDialog(this,"Selecci�n de Dependienta",true);
		}
		
		seleccion.setSize(715,500);
		seleccion.setResizable(false);
		seleccion.setLocationRelativeTo(null);
		seleccion.setLayout(null);
		seleccion.getContentPane().setBackground(azulclaro);
		
	
		int n_cam=0;
		if(a==0){
			query="Select jornadas.id,empleados.nombre,empleados.identificador from jornadas , empleados , departamentos where jornadas.empleado=empleados.id and empleados.departamento=departamentos.id and departamentos.nombre='Camareros' and empleados.id<>0 and  salida is null order by identificador";
		}else{
			query="Select jornadas.id,empleados.nombre,empleados.identificador from jornadas , empleados , departamentos where jornadas.empleado=empleados.id and empleados.departamento=departamentos.id and departamentos.nombre='Dependientas' and empleados.id<>0 and salida is null order by identificador";
		}
		try{
			rs2=statement2.executeQuery(query);
			if(rs2.first()){
				if(a==0){
					current_camareros=new JButtonEnlazado("<html><p align='center'>"+rs2.getString(2)+"</p><p align='center'>"+rs2.getInt(3)+"</p></html>",null,rs2.getInt(1));
					current_camareros.addActionListener(this);
					current_camareros.setFont(ft_bot_general);
					current_camareros.setBackground(azuloscuro);
					current_camareros.setForeground(marron);
					current_camareros.setBounds(0, 0, 100, 125);
					seleccion.add(current_camareros);
					current=current_camareros;
				}else{
					current_dependientas=new JButtonEnlazado("<html><p align='center'>"+rs2.getString(2)+"</p><p align='center'>"+rs2.getInt(3)+"</p></html>",null,rs2.getInt(1));
					current_dependientas.addActionListener(this);
					current_dependientas.setFont(ft_bot_general);
					current_dependientas.setBackground(azuloscuro);
					current_dependientas.setForeground(marron);
					current_dependientas.setBounds(0, 0, 100, 125);
					seleccion.add(current_dependientas);
					current=current_dependientas;
				}
				n_cam++;
				while(rs2.next()){
					current.set_siguiente(new JButtonEnlazado("<html><p align='center'>"+rs2.getString(2)+"</p><p align='center'>"+rs2.getInt(3)+"</p></html>",current,rs2.getInt(1)));
					current=current.get_siguiente();
					current.setFont(ft_bot_general);
					current.setBackground(azuloscuro);
					current.setForeground(marron);
					
						x=((n_cam%7))*100;
						y=((n_cam/7))*125;
					current.setBounds(x, y, 100, 125);
					current.addActionListener(this);
					seleccion.add(current);
					n_cam++;
				}
			}
			
		}catch (Exception e){
			e.printStackTrace();
		}
		
		seleccion.setVisible(true);
		
	}
	
	
	

	
	/*
	 * 
	 * 
	 * Asigna un camarero o una dependienta a una mesa.
	 * 
	 */

	private void asignar_a_mesa(JButtonEnlazado aux,int a,boolean sacar){
		if(sacar==false){
			if(a==0){
				query="Update pedidos set camarero="+aux.get_index()+" where id="+current_pedido+" and horac is null";
			}else{
				query="update pedidos set dependienta="+aux.get_index()+" where id="+current_pedido+" and horac is null";
			}
			try{
				statement2.executeUpdate(query);
			}catch (Exception e){
				e.printStackTrace();
			}
			seleccion.dispose();
		}
		if(sacar==true){
			if(a==0){
				query="Update jornadas set salida='"+new java.sql.Timestamp((new java.util.Date()).getTime())+"' where id="+aux.get_index();
			}else{
				query="Update jornadas set salida='"+new java.sql.Timestamp((new java.util.Date()).getTime())+"' where id="+aux.get_index();
			}
			try{
				statement2.executeUpdate(query);
			}catch (Exception e){
				e.printStackTrace();
			}
			sacar=false;
			seleccion.dispose();
		}
	}

	
	
	
	/*
	 * 
	 * Metodo que permite imprimir un tiquet con los datos de la mesa actual.
	 * 
	 * 
	 */
	
	private void imprimir(){
		int cantidad_borrados,servicios=0;
		Impresora p=new Impresora();
		String lista_imprimir="",linea_tiq_fech;
		
		//int formato_normal=16,formato_resaltado=49,formato_resaltado_ancho=57;
		int formato_normal=16,formato_resaltado=24,formato_resaltado_ancho=56,formato_bajo=0;
		//ArchivoConfig.cargar_config(obj_conf);
		p.centrar();
		p.setDispositivo(obj_conf.st_imp_tiq.replace("\\", "\\\\"));
	
		p.setFormato(0);
		p.correr(1);
		for(int i=0;i<obj_conf.st_cabecera.size();i++){
			if(i<2){
				p.setFormato(formato_resaltado_ancho);
				p.centrar();
			}else{
				p.setFormato(formato_bajo);
			}
			p.escribirln(obj_conf.st_cabecera.elementAt(i));
		}
		p.correr(1);
		
		p.izquierda();
		linea_tiq_fech="TICKET: "+current_pedido;
		while(linea_tiq_fech.length()<23){
			linea_tiq_fech+=" ";
		}
		linea_tiq_fech+="FECHA: "+fecha;
		p.escribirln(linea_tiq_fech);

		p.setFormato(formato_resaltado);
		p.izquierda();
		if(situacion.regionMatches(0, "Terraza", 0, 7)){
			p.escribir("MESA ");
			p.setFormato(formato_resaltado_ancho);
			p.escribir(""+n_mesa);
		}else{
			p.escribir("BARRA ");
			p.setFormato(formato_resaltado_ancho);
			p.escribir(""+n_mesa);
		}
		
		p.setFormato(formato_normal);
		p.derecha();
		p.escribirln(" \t\t\t"+Calendar.getInstance().get(Calendar.HOUR_OF_DAY)+":"+Calendar.getInstance().get(Calendar.MINUTE)+":"+Calendar.getInstance().get(Calendar.SECOND));
		p.setFormato(formato_normal);
		p.correr(1);
		p.centrar();
		p.escribirln("UDS DESCRIPCION           PRECIO   IMPORTE");
		p.dividir();
		query="select servicios_temp.id,servicios_temp.descripcion,servicios_temp.cantidad,servicios_temp.precio from servicios_temp where servicios_temp.pedido="+current_pedido+" order by id";
		try{
			rs=statement.executeQuery(query);
			if(rs.first()){
				do{
					query="select servicios_temp_borrados.cantidad from servicios_temp,servicios_temp_borrados where servicio="+rs.getInt(1);
					rs2=statement2.executeQuery(query);
					cantidad_borrados=0;
					if(rs2.first()){
						cantidad_borrados=rs2.getInt(1);
					}
					
					if(rs2.first() && rs2.getInt(1)-rs.getInt(3)==0){
						
					}else{
						servicios+=(rs.getInt(3)-cantidad_borrados);
						row=""+(rs.getInt(3)-cantidad_borrados);
						while (row.length()<4){
							row+=" ";
						}
						if(rs.getString(2).length()>23){
							row=row+""+rs.getString(2).substring(0, 23)+"";
						}else{
							row=row+""+rs.getString(2)+"";
							while(row.length()<25){
								row+=" ";
							}
						}
						for(int k=0;k<7-(""+df.format(rs.getDouble(4))).length();k++){
							row+=" ";
						}
						row=row+""+df.format(rs.getDouble(4))+"";
						
						for(int k=0;k<8-((""+df.format((rs.getInt(3)-cantidad_borrados)*rs.getDouble(4))).length());k++){
							row+=" ";
						}
						row=row+""+df.format((rs.getInt(3)-cantidad_borrados)*rs.getDouble(4))+"";
						lista_imprimir=lista_imprimir+row+salto_linea;
						p.escribirln(row.toUpperCase());
						
						if(situacion.regionMatches(0, "Terraza", 0, 7)){
							query="select productos.nombre,productos.preciot from productos, modificaciones_temp where modificaciones_temp.modificacion=productos.id and modificaciones_temp.servicio="+rs.getInt(1)+" and productos.preciot<>0";
							
						}else{
							query="select productos.nombre,productos.preciob from productos, modificaciones_temp where modificaciones_temp.modificacion=productos.id and modificaciones_temp.servicio="+rs.getInt(1)+" and productos.preciob<>0";
						}
						rs2=statement2.executeQuery(query);
						if(rs2.first()){
							row="+"+(rs.getInt(3)-cantidad_borrados)+"";
							while (row.length()<4){
								row+=" ";
							}
							if(rs2.getString(1).length()>22){
								row=row+""+rs2.getString(1).substring(0, 21)+"";
							}else{
								row=row+""+rs2.getString(1)+"";
								while(row.length()<25){
									row+=" ";
								}
							}
							for(int k=0;k<7-(""+df.format(rs2.getDouble(2))).length();k++){
								row+=" ";
							}
							row=row+""+df.format(rs2.getDouble(2))+"";	
							for(int k=0;k<8-((""+df.format((rs.getInt(3)-cantidad_borrados)*rs2.getDouble(2))).length());k++){
								row+=" ";
							}
							row=row+""+df.format((rs.getInt(3)-cantidad_borrados)*rs2.getDouble(2))+"\t";
							lista_imprimir=lista_imprimir+row+salto_linea;
							while(rs2.next()){
								row="+"+(rs.getInt(3)-cantidad_borrados)+"";
								while (row.length()<4){
									row+=" ";
								}
								if(rs2.getString(1).length()>22){
									row=row+""+rs2.getString(1).substring(0, 21)+"";
								}else{
									row=row+""+rs2.getString(1)+"";
									while(row.length()<25){
										row+=" ";
									}
								}
								for(int k=0;k<7-(""+df.format(rs2.getDouble(2))).length();k++){
									row+=" ";
								}
								row=row+""+df.format(rs2.getDouble(2))+"";	
								for(int k=0;k<8-((""+df.format((rs.getInt(3)-cantidad_borrados)*rs2.getDouble(2))).length());k++){
									row+=" ";
								}
								row=row+""+df.format((rs.getInt(3)-cantidad_borrados)*rs2.getDouble(2))+"\t";
								//lista_imprimir=lista_imprimir+row+salto_linea;
								
							}
							p.escribirln(row.toUpperCase());
						}
						
					}
				}while(rs.next());	
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//p.escribirln(lista_imprimir);
		p.dividir();
		p.izquierda();
		p.setFormato(formato_resaltado);
		if((""+servicios).length()==1){
			p.escribir("Servicios:  "+servicios+"    TOTAL:      ");
		}else{
			p.escribir("Servicios: "+servicios+"    TOTAL:      ");
		}
		p.setFormato(formato_resaltado_ancho);
		p.escribirln(" "+df.format(importe));
		p.setFormato(formato_normal);
		p.escribirln("                 ENTREGADO          0,00");
		p.escribirln("                 CAMBIO             0,00");
		p.escribirln("                   ** IVA INCLUIDO **");
		p.setFormato(formato_bajo);
		query="select empleados.nombre from empleados,departamentos where empleados.identificador="+Bt_camarero.getText()+" and empleados.departamento=departamentos.id and departamentos.nombre='Camareros'";
		try{
			rs=statement.executeQuery(query);
			rs.first();
			p.escribirln("Camarero:    "+Bt_camarero.getText()+" "+rs.getObject("nombre"));
		}catch(Exception e){
			p.escribirln("Camarero:    "+Bt_camarero.getText());
		}
		query="select empleados.nombre from empleados,departamentos where empleados.identificador="+Bt_camarera.getText()+" and empleados.departamento=departamentos.id and departamentos.nombre='Dependientas'";
		try{
			rs=statement.executeQuery(query);
			rs.first();
			p.escribirln("Dependiente: "+Bt_camarera.getText()+" "+rs.getObject("nombre"));
		}catch(Exception e){
			p.escribirln("Dependiente: "+Bt_camarera.getText());
		}
		p.setFormato(formato_normal);
		p.centrar();
		for(int i=0;i<obj_conf.st_pie.size();i++){
			p.escribirln(obj_conf.st_pie.elementAt(i));
		}
		p.correr(5);
		p.cortar();
		p.cerrarDispositivo();
	}
	
	
	/*
	 * 
	 * Imprime un tiquet tras realizar un cobro.
	 * 
	 */
	
	private void imprimir(double entrega,double cambio){
		int cantidad_borrados,servicios=0;
		Impresora p=new Impresora();
		
		String lista_imprimir="",linea_tiq_fech;
		//int formato_normal=0,formato_resaltado=24,formato_resaltado_ancho=56;
		int formato_normal=16,formato_resaltado=24,formato_resaltado_ancho=56,formato_bajo=0;
		p.setDispositivo(obj_conf.st_imp_tiq.replace("\\", "\\\\"));
		p.abrir_cajon();
		if(tiquet==true){
			p.correr(1);
			p.setFormato(0);
			for(int i=0;i<obj_conf.st_cabecera.size();i++){
				if(i<2){
					p.setFormato(formato_resaltado_ancho);
					p.centrar();
				}else{
					p.setFormato(formato_bajo);
				}
				p.escribirln(obj_conf.st_cabecera.elementAt(i));
			}
			p.correr(1);
			p.setFormato(formato_bajo);
			p.izquierda();
			linea_tiq_fech="TICKET: "+current_pedido;
			while(linea_tiq_fech.length()<23){
				linea_tiq_fech+=" ";
			}
			linea_tiq_fech+="FECHA: "+fecha;
			p.escribirln(linea_tiq_fech);
			p.setFormato(formato_resaltado);
			if(situacion.regionMatches(0, "Terraza", 0, 7)){
				p.escribir("Mesa ");
				p.setFormato(formato_resaltado_ancho);
				p.escribir(""+n_mesa);
			}else{
				p.escribir("Barra ");
				p.setFormato(formato_resaltado_ancho);
				p.escribir(""+n_mesa);
			}
			p.setFormato(formato_normal);
			p.escribirln(" \t\t\t"+Calendar.getInstance().get(Calendar.HOUR_OF_DAY)+":"+Calendar.getInstance().get(Calendar.MINUTE)+":"+Calendar.getInstance().get(Calendar.SECOND));
			
			p.centrar();
			p.correr(1);
			p.escribirln("UDS DESCRIPCION           PRECIO   IMPORTE");
			p.dividir();
			query="select servicios_temp.id,servicios_temp.descripcion,servicios_temp.cantidad,servicios_temp.precio from servicios_temp where servicios_temp.pedido="+current_pedido+" order by id";
			try{
				rs=statement.executeQuery(query);
				if(rs.first()){
					do{
						query="select servicios_temp_borrados.cantidad from servicios_temp,servicios_temp_borrados where servicio="+rs.getInt(1);
						rs2=statement2.executeQuery(query);
						cantidad_borrados=0;
						if(rs2.first()){
							cantidad_borrados=rs2.getInt(1);
						}
						
						if(rs2.first() && rs2.getInt(1)-rs.getInt(3)==0){
							
						}else{
							servicios+=(rs.getInt(3)-cantidad_borrados);
							row=""+(rs.getInt(3)-cantidad_borrados);
							while (row.length()<4){
								row+=" ";
							}
							if(rs.getString(2).length()>23){
								row=row+""+rs.getString(2).substring(0, 23)+"";
							}else{
								row=row+""+rs.getString(2)+"";
								while(row.length()<25){
									row+=" ";
								}
							}
							for(int k=0;k<7-(""+df.format(rs.getDouble(4))).length();k++){
								row+=" ";
							}
							row=row+""+df.format(rs.getDouble(4))+"";
							
							for(int k=0;k<8-((""+df.format((rs.getInt(3)-cantidad_borrados)*rs.getDouble(4))).length());k++){
								row+=" ";
							}
							row=row+""+df.format((rs.getInt(3)-cantidad_borrados)*rs.getDouble(4))+"";
							lista_imprimir=lista_imprimir+row+salto_linea;
							p.escribirln(row.toUpperCase());
							
							if(situacion.regionMatches(0, "Terraza", 0, 7)){
								query="select productos.nombre,productos.preciot from productos, modificaciones_temp where modificaciones_temp.modificacion=productos.id and modificaciones_temp.servicio="+rs.getInt(1)+" and productos.preciot<>0";
								
							}else{
								query="select productos.nombre,productos.preciob from productos, modificaciones_temp where modificaciones_temp.modificacion=productos.id and modificaciones_temp.servicio="+rs.getInt(1)+" and productos.preciob<>0";
							}
							rs2=statement2.executeQuery(query);
							if(rs2.first()){
								row="+"+(rs.getInt(3)-cantidad_borrados)+"";
								while (row.length()<4){
									row+=" ";
								}
								if(rs2.getString(1).length()>22){
									row=row+""+rs2.getString(1).substring(0, 21)+"";
								}else{
									row=row+""+rs2.getString(1)+"";
									while(row.length()<25){
										row+=" ";
									}
								}
								for(int k=0;k<7-(""+df.format(rs2.getDouble(2))).length();k++){
									row+=" ";
								}
								row=row+""+df.format(rs2.getDouble(2))+"";	
								for(int k=0;k<8-((""+df.format((rs.getInt(3)-cantidad_borrados)*rs2.getDouble(2))).length());k++){
									row+=" ";
								}
								row=row+""+df.format((rs.getInt(3)-cantidad_borrados)*rs2.getDouble(2))+"\t";
								lista_imprimir=lista_imprimir+row+salto_linea;
								while(rs2.next()){
									row="+"+(rs.getInt(3)-cantidad_borrados)+"";
									while (row.length()<4){
										row+=" ";
									}
									if(rs2.getString(1).length()>22){
										row=row+""+rs2.getString(1).substring(0, 21)+"";
									}else{
										row=row+""+rs2.getString(1)+"";
										while(row.length()<25){
											row+=" ";
										}
									}
									for(int k=0;k<7-(""+df.format(rs2.getDouble(2))).length();k++){
										row+=" ";
									}
									row=row+""+df.format(rs2.getDouble(2))+"";	
									for(int k=0;k<8-((""+df.format((rs.getInt(3)-cantidad_borrados)*rs2.getDouble(2))).length());k++){
										row+=" ";
									}
									row=row+""+df.format((rs.getInt(3)-cantidad_borrados)*rs2.getDouble(2))+"\t";
									
									lista_imprimir=lista_imprimir+row+salto_linea;
								}
								p.escribirln(row.toUpperCase());
							}
						}
					}while(rs.next());	
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			//p.escribirln(lista_imprimir);
			p.dividir();
			p.izquierda();
			p.setFormato(formato_resaltado);
			if((""+servicios).length()==1){
				p.escribir("Servicios:  "+servicios+"    TOTAL:      ");
			}else{
				p.escribir("Servicios: "+servicios+"    TOTAL:      ");
			}
			p.setFormato(formato_resaltado_ancho);
			p.escribirln(""+df.format(importe));
			
			p.setFormato(formato_normal);
			if((""+df.format(entrega)).length()==4){
				p.escribirln("                 ENTREGADO          "+df.format(entrega));
			}else if((""+df.format(entrega)).length()==5){
				p.escribirln("                 ENTREGADO         "+df.format(entrega));
			}else if((""+df.format(entrega)).length()==6){
				p.escribirln("                 ENTREGADO        "+df.format(entrega));
			}
			if((""+df.format(cambio)).length()==4){
				p.escribirln("                 CAMBIO             "+df.format(cambio));
			}else if((""+df.format(cambio)).length()==5){
				p.escribirln("                 CAMBIO            "+df.format(cambio));
			}else if((""+df.format(cambio)).length()==6){
				p.escribirln("                 CAMBIO           "+df.format(cambio));
			}
			p.escribirln("                   ** IVA INCLUIDO **");
			p.setFormato(formato_bajo);
			query="select empleados.nombre from empleados,departamentos where empleados.identificador="+Bt_camarero.getText()+" and empleados.departamento=departamentos.id and departamentos.nombre='Camareros'";
			try{
				rs=statement.executeQuery(query);
				rs.first();
				p.escribirln("Camarero:    "+Bt_camarero.getText()+" "+rs.getObject("nombre"));
			}catch(Exception e){
				p.escribirln("Camarero:    "+Bt_camarero.getText());
			}
			query="select empleados.nombre from empleados,departamentos where empleados.identificador="+Bt_camarera.getText()+" and empleados.departamento=departamentos.id and departamentos.nombre='Dependientas'";
			try{
				rs=statement.executeQuery(query);
				rs.first();
				p.escribirln("Dependiente: "+Bt_camarera.getText()+" "+rs.getObject("nombre"));
			}catch(Exception e){
				p.escribirln("Dependiente: "+Bt_camarera.getText());
			}
			p.setFormato(formato_normal);
			p.centrar();
			for(int i=0;i<obj_conf.st_pie.size();i++){
				p.escribirln(obj_conf.st_pie.elementAt(i));
			}
			p.correr(5);
			p.cortar();
		}
		p.cerrarDispositivo();
	}
	
	
	
	
	/*
	 * 
	 * M�todo que ofrece un men� para establecer los par�metros de configuraci�n.
	 * 
	 * IP,ID_caja
	 * 
	 */
	
	private void menu_configuracion(){
		if(comprobar_pass(introd_pass())){
			new ArchivoConfig(this);
		}
	}
	
	
	private void menu_jefe(){
		String password=introd_pass();
		if(password.equals("")){
			
		}else if(comprobar_pass(password)){
			final JDialog men_jef=new JDialog(this,"EL JEFE",true);
			
			men_jef.setSize(305,440);//230
			men_jef.setResizable(false);
			men_jef.setLocationRelativeTo(null);
			men_jef.setLayout(null);
			men_jef.getContentPane().setBackground(azulclaro);
			
			ActionListener listener;
			final JImageButton bt_cifra[]=new JImageButton[10];
			final JImageButton bt_coma=new JImageButton(",");
			bt_coma.setFont(ft_numeros);
			bt_coma.setBackground(Color.white);
			bt_coma.setForeground(marron);
			
			JLabel la_cant=new JLabel("Cantidad:");
			la_cant.setFont(ft_bot_general);
			la_cant.setForeground(Color.blue);
			
			final JImageButton bt_cantidad=new JImageButton("");
			bt_cantidad.setFont(ft_numeros);
			bt_cantidad.setBackground(azulclaro);
			bt_cantidad.setForeground(Color.blue);
			
			final JImageButton bt_salida=new JImageButton("Retirar");
			bt_salida.setFont(ft_bot_general);
			bt_salida.setBackground(azuloscuro);
			bt_salida.setForeground(marron);
			
			final JImageButton bt_ingresar=new JImageButton("Ingresar");
			bt_ingresar.setFont(ft_bot_general);
			bt_ingresar.setBackground(azuloscuro);
			bt_ingresar.setForeground(marron);
			
			final JImageButton bt_imprimir_informe=new JImageButton("Imprimir Informe");
			if(!permiso.equals("a")){
				System.out.println("Boton enabled=false");
				bt_imprimir_informe.setEnabled(false);
			}
			bt_imprimir_informe.setFont(ft_bot_general);
			bt_imprimir_informe.setBackground(azuloscuro);
			bt_imprimir_informe.setForeground(marron);
			
			final JImageButton bt_informe_empleados=new JImageButton("Informe Empleados");
			bt_informe_empleados.setFont(ft_bot_general);
			bt_informe_empleados.setBackground(azuloscuro);
			bt_informe_empleados.setForeground(marron);
			
			
			final JImageButton bt_desbloquear_mesas=new JImageButton("Desbloquear mesas");
			bt_desbloquear_mesas.setFont(ft_bot_general);
			bt_desbloquear_mesas.setBackground(azuloscuro);
			bt_desbloquear_mesas.setForeground(marron);
			
			final JImageButton e_coma=new JImageButton("n");
			final JImageButton n_decimales=new JImageButton("0");
			
			listener=new ActionListener(){
				public void actionPerformed(ActionEvent ev){
					boolean encontrado=false;
					int i=0;
					
						
					if(ev.getSource()==bt_salida){
						/*
						 * Retirar dinero, insertar en movimientos de caja,tipo retirada, con la sesion, la hora y la cantidad
						 * 
						 */
						query="insert into movimientos_caja (tipo,cantidad,sesion,caja) values (1,"+bt_cantidad.getText().replace(',', '.')+",'"+sesion_actual+"',"+obj_conf.numerocaja+") ";
						try{
							statement.execute(query);
						}catch (Exception e){
							e.printStackTrace();
						}
						JOptionPane.showMessageDialog(null, "Se ha extraido "+bt_cantidad.getText().replace(',', '.')+" con �xito.");
						
						men_jef.dispose();
						
					}else if(ev.getSource()==bt_cantidad){
						bt_cantidad.setText("");
						e_coma.setText("n");
						n_decimales.setText("0");
					}else if(ev.getSource()==bt_ingresar){
						/*
						 * Ingresar dinero,insertar en movimientos de caja,tipo ingreso, sesion, hora y cantidad.
						 * 
						 */
						query="insert into movimientos_caja (tipo,cantidad,sesion,caja) values (2,"+bt_cantidad.getText().replace(',', '.')+",'"+sesion_actual+"',"+obj_conf.numerocaja+") ";
						try{
							statement.execute(query);
						}catch (Exception e){
							e.printStackTrace();
						}
						JOptionPane.showMessageDialog(null, "Se ha ingresado "+bt_cantidad.getText().replace(',', '.')+" con �xito.");
						men_jef.dispose();
						
					}else if(ev.getSource()==bt_coma){
						if(e_coma.getText().equals("n")){
							bt_cantidad.setText(bt_cantidad.getText()+",");
							e_coma.setText("s");
						}
					}else if(ev.getSource()==bt_imprimir_informe){
						imprimir_informe();
					}else if(ev.getSource()==bt_informe_empleados){
						informe_empleados();
					}else if(ev.getSource()==bt_desbloquear_mesas){
						query="update mesas set estado='a' where estado='u'";
						try{
							statement.executeUpdate(query);
							actualizar_grid();
							men_jef.dispose();
						}catch (Exception e){
							e.printStackTrace();
						}
					}else{
						while(!encontrado && i<10){
							if(ev.getSource()==bt_cifra[i]){
								
								if(bt_cantidad.getText().length()==7){
									bt_cantidad.setText("");
									n_decimales.setText("0");
									e_coma.setText("n");
								}else if(bt_cantidad.getText().length()==4 && e_coma.getText().equals("n")){
									bt_cantidad.setText("");
									n_decimales.setText("0");
									e_coma.setText("n");
								}
								if(Integer.parseInt(n_decimales.getText())<3){
									if(e_coma.getText().equals("s")){
										n_decimales.setText(""+Integer.parseInt(n_decimales.getText())+1);
									}
									bt_cantidad.setText(bt_cantidad.getText()+i);
								}else{
									bt_cantidad.setText(bt_cifra[i].getText());
									n_decimales.setText("0");
									e_coma.setText("n");
								}
							}
							i++;
						}
					}
				}};
				
				for(int i=0;i<10;i++){
					bt_cifra[i]=new JImageButton(""+i);
					bt_cifra[i].setFont(ft_numeros);
					bt_cifra[i].setBackground(Color.white);
					bt_cifra[i].setForeground(marron);
					bt_cifra[i].addActionListener(listener);
					bt_cifra[i].setBounds((i%3)*48,(i/3)*50, 48, 50);
					men_jef.add(bt_cifra[i]);
				}
				bt_coma.addActionListener(listener);
				bt_coma.setBounds(48, 3*50, 48*2,50);
				men_jef.add(bt_coma);
				
				
				la_cant.setBounds(150, 20, 150, 20);
				men_jef.add(la_cant);
				
				bt_cantidad.addActionListener(listener);
				bt_cantidad.setBounds(150,40,150,50);
				men_jef.add(bt_cantidad);
				
				
				bt_salida.addActionListener(listener);
				bt_salida.setBounds(170,100,130,50);
				men_jef.add(bt_salida);
				
				bt_ingresar.addActionListener(listener);
				bt_ingresar.setBounds(170,150,130,50);
				men_jef.add(bt_ingresar);
				
				bt_imprimir_informe.addActionListener(listener);
				bt_imprimir_informe.setBounds(0,200,300,70);
				men_jef.add(bt_imprimir_informe);
				
				bt_informe_empleados.addActionListener(listener);
				bt_informe_empleados.setBounds(0,270,300,70);
				men_jef.add(bt_informe_empleados);
				
				bt_desbloquear_mesas.addActionListener(listener);
				bt_desbloquear_mesas.setBounds(0,340,300,70);
				men_jef.add(bt_desbloquear_mesas);
				
				
				men_jef.setVisible(true);
				
		}else{
			JOptionPane.showMessageDialog(this, "Contrase�a incorrecta.\nAVISO:Todo intento de acceder a la parte restringida queda registrado en la base de datos.");
		}
		
		
	}
	
	private void imprimir_informe(){
		String query2,query3;
		ResultSet rs3;
		Calendar fecha=Calendar.getInstance();
		int n_ventas=0;
			try{
				double cantidad=0,cant_cancel=0,cantidad_mov=0,do_ingresos=0,do_retiradas=0;
				Impresora p=new Impresora();
				p.setDispositivo(obj_conf.st_imp_tiq.replace("\\", "\\\\"));
				
				query="select id,nombre from sesiones where activa=true";
				rs=statement.executeQuery(query);
				rs.first();
				p.setFormato(16);
				p.dividir_asterisco();
				p.escribirln("Sesi�n: "+ rs.getObject("nombre"));
				p.escribirln("Hora: "+fecha.get(Calendar.HOUR_OF_DAY)+":"+fecha.get(Calendar.MINUTE)+":"+fecha.get(Calendar.SECOND)+"   "+fecha.get(Calendar.DAY_OF_MONTH)+"/"+(fecha.get(Calendar.MONTH)+1)+"/"+fecha.get(Calendar.YEAR));
				p.escribirln("Usuario: "+usuario.toUpperCase());
				p.dividir_asterisco();
				p.dividir_asterisco();
				
				//#############################################################   ARQUEOS
				p.centrar();
				p.escribirln("ARQUEOS");
				p.izquierda();
				p.dividir();
				query3="select arqueos.id,cajas.nombre,permisos.nombre as nombreper,arqueos.resultado,arqueos.dinero from arqueos,cajas,permisos where permisos.id=arqueos.permiso and arqueos.caja=cajas.id and arqueos.sesion="+rs.getInt("id")+" order by arqueos.caja";
				rs3=statement3.executeQuery(query3);
				while(rs3.next()){
					p.escribirln("ARQUEO: "+rs3.getInt("id")+"\t USUARIO: "+rs3.getString("nombreper").toUpperCase());
					p.escribirln(" -"+rs3.getString("nombre").toUpperCase()+":  ...... "+rs3.getDouble("dinero")+"\tRTDO: "+rs3.getObject("resultado"));
					p.correr(1);
				}
				p.dividir_asterisco();
				p.dividir_asterisco();
				//#############################################################   CAJAS
				p.centrar();
				p.escribirln("CAJAS");
				p.izquierda();
						
				
				query3="select nombre from cajas";
				rs3=statement3.executeQuery(query3);
				while(rs3.next()){
					p.dividir();
					p.izquierda();
					p.escribirln(rs3.getString("nombre").toUpperCase()+":");
					
					query="select cantidad,tipo from movimientos_caja,sesiones,cajas where movimientos_caja.sesion=sesiones.id and sesiones.activa=true and cajas.id=movimientos_caja.caja and cajas.nombre='"+rs3.getString("nombre")+"' ";
					rs=statement.executeQuery(query);
					cantidad_mov=0;
					while(rs.next()){
						if(rs.getInt("tipo")==1){
							do_retiradas+=rs.getDouble("cantidad");
						}else{
							do_ingresos+=rs.getDouble("cantidad");
						}
					}
					cantidad_mov=do_ingresos-do_retiradas;
					p.escribirln(" -MOVIMIENTOS DE EFECTIVO: ");
					p.escribirln("   -INGRESOS:\t\t........ "+df.format(do_ingresos));
					p.escribirln("   -RETIRADAS:\t\t........ "+df.format(do_retiradas));
					p.escribirln("  -TOTAL:\t\t........ "+df.format(cantidad_mov));
					
					query="select cobros.id,pedidos.importe from pedidos,cobros,sesiones,cajas where cobros.caja=cajas.id and cajas.nombre='"+rs3.getString("nombre")+"' and cobros.pedido=pedidos.id and cobros.sesion=sesiones.id and sesiones.activa=true";
					rs=statement.executeQuery(query);
					cantidad=0;
					cant_cancel=0;
					while(rs.next()){
						query="select cobroscancelados.id from cobroscancelados where cobroscancelados.cobro="+rs.getInt(1);
						rs2=statement2.executeQuery(query);
						if(!rs2.first()){
							cantidad+=rs.getDouble(2);
						}else{
							cant_cancel+=rs.getDouble(2);
						}
					}
					p.setFormato(24);
					p.escribirln(" -VENTA NETA (Z):\t........ "+df.format(cantidad));
					p.setFormato(16);
					p.escribirln(" -CANCELACIONES:\t........ "+df.format(cant_cancel));
					p.correr(1);
					p.escribirln(" TOTAL : \t\t........ "+df.format(cantidad+cantidad_mov));
				}
				p.dividir_asterisco();
				p.dividir_asterisco();
				
				//#############################################################   VENTAS
				
				p.centrar();
				p.escribirln("VENTAS");
				p.izquierda();
				p.dividir();
				
				p.escribirln("TERRAZA:");
				cantidad=0;
				query="select pedidos.importe,pedidos.id from pedidos,cobros,sesiones,mesas,situacion where cobros.pedido=pedidos.id and cobros.sesion=sesiones.id and sesiones.activa=true and pedidos.mesa=mesas.id and mesas.tipo=situacion.id and situacion.nombre='Terraza' except (select pedidos.importe,pedidos.id from pedidos,cobros,cobroscancelados,cajas where cobros.id=cobroscancelados.cobro and cobros.pedido=pedidos.id)";
				rs=statement.executeQuery(query);
				n_ventas=0;
				while(rs.next()){
					cantidad+=rs.getDouble("importe");
					n_ventas++;
				}
				p.escribirln("   -VENTAS:\t\t........ "+n_ventas);
				p.escribirln("  -TOTAL:\t\t........ "+df.format(cantidad));
				p.dividir();
				p.escribirln("BARRA:");
				cantidad=0;
				query="select pedidos.importe,pedidos.id from pedidos,cobros,sesiones,mesas,situacion where cobros.pedido=pedidos.id and cobros.sesion=sesiones.id and sesiones.activa=true and pedidos.mesa=mesas.id and mesas.tipo=situacion.id and situacion.nombre='Barra' except (select pedidos.importe,pedidos.id from pedidos,cobros,cobroscancelados where cobros.id=cobroscancelados.cobro and cobros.pedido=pedidos.id)";
				rs=statement.executeQuery(query);
				n_ventas=0;
				while(rs.next()){
					cantidad+=rs.getDouble("importe");
					n_ventas++;
				}
				p.escribirln("   -VENTAS:\t\t........ "+n_ventas);
				p.escribirln("  -TOTAL:\t\t........ "+df.format(cantidad));
				
				p.dividir_asterisco();
				p.dividir_asterisco();
				//#############################################################   EMPLEADOS
				p.centrar();
				p.escribirln("EMPLEADOS");
				p.izquierda();
				
																	//************** Dependientas

				p.dividir();
				p.escribirln("DEPENDIENTAS:");
				query="select empleados.nombre,jornadas.id from empleados,sesiones,jornadas,departamentos where jornadas.empleado=empleados.id and jornadas.sesion=sesiones.id and sesiones.activa=true and empleados.departamento=departamentos.id and departamentos.nombre='Dependientas'";
				rs=statement.executeQuery(query);
				
				while(rs.next()){
					cantidad=0;
					cant_cancel=0;
					query2="select pedidos.id,pedidos.importe from pedidos,cobros where pedidos.dependienta="+rs.getInt(2)+" and cobros.pedido=pedidos.id ";
					rs2=statement2.executeQuery(query2);
					while(rs2.next()){
						query3="select cobroscancelados.id from cobroscancelados,cobros where cobros.pedido="+rs2.getInt("id")+" and cobroscancelados.cobro=cobros.id";
						rs3=statement3.executeQuery(query3);
						if(!rs3.first()){
							cantidad+=rs2.getDouble("importe");
							cant_cancel++;
						}
					}
					p.escribirln(" -"+rs.getString(1).toUpperCase()+": \t.... "+df.format(cantidad)+"\tPedidos: "+(int)cant_cancel+"");
				}
				
																			//************** Camareros
				
				p.dividir();
				p.escribirln("CAMAREROS:");
				query="select empleados.nombre,jornadas.id from empleados,sesiones,jornadas,departamentos where jornadas.empleado=empleados.id and jornadas.sesion=sesiones.id and sesiones.activa=true and empleados.departamento=departamentos.id and departamentos.nombre='Camareros'";
				rs=statement.executeQuery(query);
				
				while(rs.next()){
					cantidad=0;
					cant_cancel=0;
					//query2="select sum(pedidos.importe) from pedidos,cobros ";
					query2="select pedidos.id,pedidos.importe from pedidos,cobros where pedidos.camarero="+rs.getInt(2)+" and cobros.pedido=pedidos.id ";
					rs2=statement2.executeQuery(query2);
					while(rs2.next()){
						query3="select cobroscancelados.id from cobroscancelados,cobros where cobros.pedido="+rs2.getInt("id")+" and cobroscancelados.cobro=cobros.id";
						rs3=statement3.executeQuery(query3);
						if(!rs3.first()){
							cantidad+=rs2.getDouble("importe");
							cant_cancel++;
						}
					}
					p.escribirln(" -"+rs.getString(1).toUpperCase()+": \t.... "+df.format(cantidad)+"\tPedidos: "+(int)cant_cancel);
				}
				
				p.dividir_asterisco();
				p.correr(5);
				p.cortar();
				p.reestablecer();
				p.cerrarDispositivo();
				
			}catch (Exception E){
				E.printStackTrace();
			}
		
	}
	
	private void informe_empleados(){
		double cantidad=0,cantidadterraza=0,cantidadbarra=0;
		int n_ventas_barra=0,n_ventas=0,n_ventas_terraza=0,cant_cancel;
		String query2,query3;
		ResultSet rs3;
		Calendar fecha=Calendar.getInstance();
		try{
			Impresora p=new Impresora();
			p.setDispositivo(obj_conf.st_imp_tiq.replace("\\", "\\\\"));
			
			p.setFormato(16);
			query="select id,nombre from sesiones where activa=true";
			rs=statement.executeQuery(query);
			rs.first();
			
			p.dividir_asterisco();
			p.escribirln("Sesi�n: "+ rs.getObject("nombre"));
			p.escribirln("Hora: "+fecha.get(Calendar.HOUR_OF_DAY)+":"+fecha.get(Calendar.MINUTE)+":"+fecha.get(Calendar.SECOND)+"   "+fecha.get(Calendar.DAY_OF_MONTH)+"/"+(fecha.get(Calendar.MONTH)+1)+"/"+fecha.get(Calendar.YEAR));
			p.escribirln("Usuario: "+usuario.toUpperCase());
			p.dividir_asterisco();
			p.dividir_asterisco();
			
			cantidadterraza=0;
			query="select pedidos.importe,pedidos.id from pedidos,cobros,sesiones,mesas,situacion where cobros.pedido=pedidos.id and cobros.sesion=sesiones.id and sesiones.activa=true and pedidos.mesa=mesas.id and mesas.tipo=situacion.id and situacion.nombre='Terraza' except (select pedidos.importe,pedidos.id from pedidos,cobros,cobroscancelados,cajas where cobros.id=cobroscancelados.cobro and cobros.pedido=pedidos.id)";
			rs=statement.executeQuery(query);
			n_ventas_terraza=0;
			while(rs.next()){
				cantidadterraza+=rs.getDouble("importe");
				n_ventas_terraza++;
			}
			
			cantidadbarra=0;
			query="select pedidos.importe,pedidos.id from pedidos,cobros,sesiones,mesas,situacion where cobros.pedido=pedidos.id and cobros.sesion=sesiones.id and sesiones.activa=true and pedidos.mesa=mesas.id and mesas.tipo=situacion.id and situacion.nombre='Barra' except (select pedidos.importe,pedidos.id from pedidos,cobros,cobroscancelados where cobros.id=cobroscancelados.cobro and cobros.pedido=pedidos.id)";
			rs=statement.executeQuery(query);
			n_ventas_barra=0;
			while(rs.next()){
				cantidadbarra+=rs.getDouble("importe");
				n_ventas_barra++;
			}
			
			p.centrar();
			p.escribirln("VENTAS");
			p.izquierda();
			p.dividir();
			
			p.escribirln("TERRAZA:");
			
			p.escribirln("   -VENTAS:\t\t........ "+n_ventas_terraza);
			p.escribirln("  -TOTAL:\t\t........ "+df.format((cantidadterraza/(cantidadterraza+cantidadbarra))*100)+"%");
			p.dividir();
			p.escribirln("BARRA:");
			
			p.escribirln("   -VENTAS:\t\t........ "+n_ventas_barra);
			p.escribirln("  -TOTAL:\t\t........ "+df.format((cantidadbarra/(cantidadterraza+cantidadbarra))*100)+"%");
			
			p.dividir_asterisco();
			p.dividir_asterisco();
			
			//#############################################################   EMPLEADOS
			p.centrar();
			p.escribirln("EMPLEADOS");
			p.izquierda();
			
																//************** Dependientas

			p.dividir();
			p.escribirln("DEPENDIENTAS:");
			query="select empleados.nombre,jornadas.id from empleados,sesiones,jornadas,departamentos where jornadas.empleado=empleados.id and jornadas.sesion=sesiones.id and sesiones.activa=true and empleados.departamento=departamentos.id and departamentos.nombre='Dependientas'";
			rs=statement.executeQuery(query);
			
			while(rs.next()){
				cantidad=0;
				cant_cancel=0;
				query2="select pedidos.id,pedidos.importe from pedidos,cobros where pedidos.dependienta="+rs.getInt(2)+" and cobros.pedido=pedidos.id ";
				rs2=statement2.executeQuery(query2);
				while(rs2.next()){
					query3="select cobroscancelados.id from cobroscancelados,cobros where cobros.pedido="+rs2.getInt("id")+" and cobroscancelados.cobro=cobros.id";
					rs3=statement3.executeQuery(query3);
					if(!rs3.first()){
						cantidad+=rs2.getDouble("importe");
						cant_cancel++;
					}
				}
				p.escribirln(" -"+rs.getString(1).toUpperCase()+": \t.... "+df.format((cantidad/(cantidadterraza+cantidadbarra))*100)+"%\tPedidos: "+(int)cant_cancel+"");
			}
			
																		//************** Camareros
			
			p.dividir();
			p.escribirln("CAMAREROS:");
			query="select empleados.nombre,jornadas.id from empleados,sesiones,jornadas,departamentos where jornadas.empleado=empleados.id and jornadas.sesion=sesiones.id and sesiones.activa=true and empleados.departamento=departamentos.id and departamentos.nombre='Camareros'";
			rs=statement.executeQuery(query);
			
			while(rs.next()){
				cantidad=0;
				cant_cancel=0;
				query2="select pedidos.id,pedidos.importe from pedidos,cobros where pedidos.camarero="+rs.getInt(2)+" and cobros.pedido=pedidos.id ";
				rs2=statement2.executeQuery(query2);
				while(rs2.next()){
					query3="select cobroscancelados.id from cobroscancelados,cobros where cobros.pedido="+rs2.getInt("id")+" and cobroscancelados.cobro=cobros.id";
					rs3=statement3.executeQuery(query3);
					if(!rs3.first()){
						cantidad+=rs2.getDouble("importe");
						cant_cancel++;
					}
				}
				p.escribirln(" -"+rs.getString(1).toUpperCase()+": \t.... "+df.format((cantidad/(cantidadterraza))*100)+"%\tPedidos: "+(int)cant_cancel);
			}
			p.dividir_asterisco();
			p.dividir_asterisco();
			p.correr(5);
			p.cortar();
			p.cerrarDispositivo();
			
			
		}catch(Exception e){
			
		}
	}
	
	private boolean comprobar_pass(String pwd){
		boolean correcta=true;
		//System.out.println(encryptar_sha1(pwd));
		query="select permisos.id,permisos.nombre,acceso.nombre as permisos from permisos,acceso where permisos.permisos=acceso.id and password='"+encryptar_sha1(pwd)+"'";
		try{
			rs=statement.executeQuery(query);
			if(!rs.first()){
				correcta=false;
				
			}else{
				permiso= (String) rs.getObject("permisos");
				usuario=rs.getString("nombre");
				//permiso= rs.getInt("id");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return correcta;
	}
	
	private String encryptar_sha1(String pwd){
		byte[] digest = null;
		byte[] buffer = pwd.getBytes();
		
		try {
		
		    MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
		    messageDigest.reset();
		    messageDigest.update(buffer);
			digest = messageDigest.digest();
		
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		 String hash = "";
		 for(byte aux : digest) {
			 int b = aux & 0xff;
			 if (Integer.toHexString(b).length() == 1) hash += "0";
			 hash += Integer.toHexString(b);
		 }
		 return hash;


	}
	
	private String introd_pass() {
		final JButton pass=new JButton();
		final JDialog int_pass=new JDialog(this,"Contrase�a",true);
		
		int_pass.setSize(305,230);
		int_pass.setResizable(false);
		int_pass.setLocationRelativeTo(null);
		int_pass.setLayout(null);
		int_pass.getContentPane().setBackground(Color.white);
		
		ActionListener listener;
		final JImageButton bt_cifra[]=new JImageButton[10];
		
		JLabel la_pass=new JLabel("Password:");
		la_pass.setFont(ft_bot_general);
		la_pass.setForeground(Color.blue);
		
		final JImageButton bt_pass=new JImageButton("");
		bt_pass.setFont(ft_bot_general);
		bt_pass.setForeground(Color.blue);
		bt_pass.setBackground(azulclaro);
		
		final JImageButton bt_entrar=new JImageButton("Ok");
		bt_entrar.setFont(ft_bot_general);
		bt_entrar.setBackground(azuloscuro);
		bt_entrar.setForeground(marron);
		
		
		listener=new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				boolean encontrado=false;
				int i=0;
				
					
				if(ev.getSource()==bt_pass){
					bt_pass.setText("");
					pass.setText("");
				}else if(ev.getSource()==bt_entrar){
					int_pass.dispose();
					
				}else{
					while(!encontrado && i<10){
						if(ev.getSource()==bt_cifra[i]){
							bt_pass.setText(bt_pass.getText()+"*");
							pass.setText(pass.getText()+bt_cifra[i].getText());
						}
						i++;
					}
				}
			}};
			
			for(int i=0;i<10;i++){
				bt_cifra[i]=new JImageButton(""+i);
				bt_cifra[i].setFont(ft_numeros);
				bt_cifra[i].setBackground(azulclaro);
				bt_cifra[i].setForeground(marron);
				bt_cifra[i].addActionListener(listener);
				bt_cifra[i].setBounds((i%3)*48,(i/3)*50, 48, 50);
				int_pass.add(bt_cifra[i]);
			}
			la_pass.setBounds(150, 20, 150, 20);
			int_pass.add(la_pass);
			
			bt_pass.addActionListener(listener);
			bt_pass.setBounds(150,40,150,50);
			int_pass.add(bt_pass);
			
			
			bt_entrar.addActionListener(listener);
			bt_entrar.setBounds(170,130,130,70);
			int_pass.add(bt_entrar);
			
			int_pass.setVisible(true);
			
			
			
			
			return pass.getText();
	
	}
	
	/*
	 * 
	 * Permite juntar o mover las mesas de n�mero.
	 * 
	 * Pone al programa en estado juntar. Muestra el grid, y el pedido se a�ade a la pr�xima mesa que se seleccione.
	 * 
	 * 
	 */
	
	private void juntar_mesas(){
		pedido_juntar=current_pedido;
		juntando=true;
		cerrar_mesa();
	}
	
	
	
	
	/*
	 * 
	 * Permite separar las cuentas para cobrarlas por separado.
	 * 
	 * 
	 */
	
	private void separar_cuenta() {
		final JDialog v_separa=new JDialog(this,"Separando cuenta",true);
		JPanel pa_mesa,pa_aux,pa_total;
		final JList li_m_princ,li_m_aux;
		final Vector <String> vc_m_princ,vc_m_aux;
		final JScrollPane sc_m_princ;
		final JScrollPane sc_m_aux;
		JButton Bc_m_princ,Bc_m_aux;
		JImageButton Bc_izq,Bc_der;
		final JButton []Btnum;
		final JButton btcom,btcif;
		final JLabel la_m_princ=new JLabel(""),la_m_aux=new JLabel("");
		JLabel la_total_pr=new JLabel("<html><p align='center'>TOTAL</p></html>"), la_total_ax=new JLabel("<html><p align='center'>TOTAL</p></html>");
		ActionListener listener;
		final JPanel pa_c_aux,pa_c_princ;
		final JLabel la_e_aux,la_c_aux,la_e_princ,la_c_princ;
		id_pedido_aux = 0;
		
		v_separa.setSize(736,516);//716
		v_separa.getContentPane().setBackground(azulclaro);
		v_separa.setLocationRelativeTo(null);
		pa_total=new JPanel(null);
		pa_total.setBackground(azuloscuro);
		pa_total.setBounds(0,0, 720, 500);//700
		v_separa.add(pa_total);
		
		
		Btnum=new JButton[10];
		for(int i=0;i<10;i++){
			Btnum[i]=new JButton(""+i);
			Btnum[i].setBackground(Color.white);
			Btnum[i].setForeground(azuloscuro);
			Btnum[i].setFont(ft_bot_general);
			Btnum[i].setBounds(54*i,420,54,54);
			pa_total.add(Btnum[i]);
		}
		btcom=new JButton (",");
		btcom.setBackground(Color.white);
		btcom.setForeground(azuloscuro);
		btcom.setFont(ft_bot_general);
		btcom.setBounds(540,420,54,54);
		pa_total.add(btcom);
		btcif=new JButton("");
		btcif.setBackground(azulclaro);
		btcif.setForeground(marron);
		btcif.setFont(ft_bot_general);
		btcif.setBounds(594,420,126,54);//106
		pa_total.add(btcif);
		
		listener=new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				boolean coma=false;
				int n_dec=0;
				for(int i=0;i<10;i++){
					if(ev.getSource()==Btnum[i]){
						if(coma==false){
							if(btcif.getText().length()==3){
								btcif.setText(""+i);
							}else{
								btcif.setText(btcif.getText()+i);
							}
						}else{
							if(n_dec<2){
								btcif.setText(btcif.getText()+i);
								n_dec++;
							}
						}
					}
				}
				if(ev.getSource()==btcom && coma==false){
					btcif.setText(btcif.getText()+",");
					coma=true;
				}else if(ev.getSource()==btcif){
					btcif.setText("");
					n_dec=0;
					coma=false;
				}
			}
		};
		
		
		for(int i=0;i<10;i++){
			Btnum[i].addActionListener(listener);
		}
		btcom.addActionListener(listener);
		btcif.addActionListener(listener);
		
		query="select pedidos.mesa,pedidos.camarero,pedidos.dependienta,pedidos.importe,pedidos.horaa,pedidos.tiempo,situacion.nombre from pedidos,mesas,situacion where pedidos.id="+current_pedido+" and pedidos.mesa=mesas.id and mesas.tipo=situacion.id";
		try{
			rs=statement.executeQuery(query);
			rs.first();
			
			int mesa_aux=rs.getInt(1);
			if(rs.getString("nombre").equals("Barra")){
				query="insert into pedidos (mesa,dependienta,importe,horaa,tiempo) values ("+rs.getInt(1)+","+rs.getInt(3)+",0,'"+rs.getTimestamp(5)+"',"+rs.getInt(6)+")";
				statement.execute(query);
			}else{
				query="insert into pedidos (mesa,camarero,dependienta,importe,horaa,tiempo) values ("+rs.getInt(1)+","+rs.getInt(2)+","+rs.getInt(3)+",0,'"+rs.getTimestamp(5)+"',"+rs.getInt(6)+")";
				statement.execute(query);	
			}
			query="select id from pedidos where mesa="+mesa_aux+" and importe=0 and horac is null";
			rs=statement.executeQuery(query);
			rs.first();
			id_pedido_aux=rs.getInt(1);
		}catch (Exception e){
			e.printStackTrace();
		}
		
		
		
		/*
		 * Panel mesa princ 
		 * 
		 * 
		 */
		
		pa_mesa=new JPanel(null);
		pa_mesa.setBackground(azulclaro);
		
		pa_c_princ=new JPanel(null);
		pa_c_princ.setBackground(azulclaro);
		
		la_e_princ=new JLabel("");
		la_e_princ.setForeground(Color.blue);
		
		la_c_princ=new JLabel("");
		la_c_princ.setForeground(Color.blue);
		
		vc_m_princ=lista_prod;
		li_m_princ=new JList(vc_m_princ);
		li_m_princ.setFont(ft_bot_general);
		li_m_princ.setForeground(marron);
		
		sc_m_princ=new JScrollPane(li_m_princ,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		/*
		 * 
		 * Panel mesa aux
		 * 
		 */
		
		pa_aux=new JPanel(null);
		pa_aux.setBackground(azulclaro);
		
		pa_c_aux=new JPanel(null);
		pa_c_aux.setBackground(azulclaro);
		
		la_e_aux=new JLabel("");
		la_e_aux.setForeground(Color.blue);
		
		la_c_aux=new JLabel("");
		la_c_aux.setForeground(Color.blue);
		
		vc_m_aux=new Vector<String> (1,1);
		li_m_aux=new JList(vc_m_aux);
		li_m_aux.setFont(ft_bot_general);
		li_m_aux.setForeground(marron);
		
		
		sc_m_aux=new JScrollPane(li_m_aux,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		//Colocacion mesa principal
		
		pa_mesa.setBounds(0,0,330,400);//320
		pa_mesa.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		pa_total.add(pa_mesa);
		
	
		pa_c_princ.setBounds(0,20,330,300);//320
		pa_c_princ.setVisible(false);
		pa_mesa.add(pa_c_princ);
		
		
		la_e_princ.setBounds(0,0,330,150);//320
		la_e_princ.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.blue), "Entrega:",0,0,Font.getFont("Entrega"),Color.blue));		
		la_e_princ.setFont(fuente_cobros);
		pa_c_princ.add(la_e_princ);
		
		
		la_c_princ.setBounds(0,150,330,150);//320
		la_c_princ.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.blue), "Cambio:",0,0,Font.getFont("Cambio"),Color.blue));		
		la_c_princ.setFont(fuente_cobros);
		pa_c_princ.add(la_c_princ);
		
	
		//li_m_princ.setBounds(0,0,320,300);
		sc_m_princ.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		sc_m_princ.setBounds(0,20,330,300);
		pa_mesa.add(sc_m_princ);
		
		Bc_m_princ=new JButton("Cobrar");
		Bc_m_princ.setFont(ft_bot_general);
		Bc_m_princ.setForeground(marron);
		Bc_m_princ.setBackground(azuloscuro);
		Bc_m_princ.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				sc_m_aux.setVisible(true);
				pa_c_aux.setVisible(false);
				sc_m_princ.setVisible(true);
				pa_c_princ.setVisible(false);
				
				double entrega;
				try{
					
					actualizar_importe();
					if(btcif.getText()!=""){
						entrega=Double.parseDouble(btcif.getText());
					}else{
						entrega=importe;
					}
					if(entrega>=importe){
						//imprimir(entrega,(entrega-importe));
						la_e_princ.setText(""+df.format(entrega));
						la_c_princ.setText(""+df.format(entrega-importe));
						sc_m_princ.setVisible(false);
						pa_c_princ.setVisible(true);
						
						separando=true;
						cobrar_mesa(entrega);
						separando=false;
						//query="select mesa,camarero,dependienta,importe,horaa,tiempo from pedidos where id="+current_pedido;
						query="select pedidos.mesa,pedidos.camarero,pedidos.dependienta,pedidos.importe,pedidos.horaa,pedidos.tiempo,situacion.nombre from pedidos,mesas,situacion where pedidos.id="+current_pedido+" and pedidos.mesa=mesas.id and mesas.tipo=situacion.id";
						
						
						try{
							rs=statement.executeQuery(query);
							rs.first();
							int mesa_aux=rs.getInt(1);
							
							if(rs.getString("nombre").equals("Barra")){
								query="insert into pedidos (mesa,dependienta,importe,horaa,tiempo) values ("+rs.getInt(1)+","+rs.getInt(3)+",0,'"+rs.getTimestamp(5)+"',"+rs.getInt(6)+") returning id";
							}else{
								query="insert into pedidos (mesa,camarero,dependienta,importe,horaa,tiempo) values ("+rs.getInt(1)+","+rs.getInt(2)+","+rs.getInt(3)+",0,'"+rs.getTimestamp(5)+"',"+rs.getInt(6)+") returning id";
							}
							rs=statement.executeQuery(query);
							rs.first();
							current_pedido=rs.getInt("id");
						}catch (Exception e){
							e.printStackTrace();
						}
						
						
						actualizar_lista_prod(current_pedido,li_m_princ);
						actualizar_lista_prod(id_pedido_aux,li_m_aux);
						la_m_princ.setText(""+df.format(actualizar_importe(current_pedido)));
						la_m_aux.setText(""+df.format(actualizar_importe(id_pedido_aux)));
						btcif.setText("");
					}else{
						JOptionPane.showMessageDialog(null, "La cantidad introducida es menor\nque el importe total de la cuenta");
					}
					
					
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		});
		Bc_m_princ.setBounds(0,320,100,80);
		pa_mesa.add(Bc_m_princ);
		
		la_total_pr.setBounds(110,320,60,80);
		la_total_pr.setFont(ft_bot_general);
		la_total_pr.setForeground(marron);
		pa_mesa.add(la_total_pr);
		
		la_m_princ.setBounds(180,320,150,80);
		la_m_princ.setText(""+df.format(importe));
		la_m_princ.setFont(fuente_importe);
		la_m_princ.setForeground(Color.blue);
		pa_mesa.add(la_m_princ);
		
		
		//colocacion mesa aux
		
		pa_aux.setBounds(390,0,330,400);//380,320
		pa_aux.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		pa_total.add(pa_aux);
		
		
		pa_c_aux.setBounds(0,20,330,300);
		pa_c_aux.setVisible(false);
		pa_aux.add(pa_c_aux);
		
		
		la_e_aux.setBounds(0,0,330,150);
		la_e_aux.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.blue), "Entrega:",0,0,Font.getFont("Entrega"),Color.blue));		
		la_e_aux.setFont(fuente_cobros);
		pa_c_aux.add(la_e_aux);
		
		
		la_c_aux.setBounds(0,150,330,150);
		la_c_aux.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.blue), "Cambio:",0,0,Font.getFont("Cambio"),Color.blue));		
		la_c_aux.setFont(fuente_cobros);
		pa_c_aux.add(la_c_aux);
		

		
		vc_m_aux.addElement(vc_m_princ.firstElement());
		li_m_aux.setBounds(0,0,320,300);
		sc_m_aux.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		sc_m_aux.setBounds(0,20,330,300);
		pa_aux.add(sc_m_aux);
		
		Bc_m_aux=new JButton("Cobrar");
		Bc_m_aux.setFont(ft_bot_general);
		Bc_m_aux.setForeground(marron);
		Bc_m_aux.setBackground(azuloscuro);
		Bc_m_aux.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				sc_m_aux.setVisible(true);
				pa_c_aux.setVisible(false);
				sc_m_princ.setVisible(true);
				pa_c_princ.setVisible(false);
				double entrega,importe2=0,importe_aux=0;
				int pedido_aux;
				try{
			
					importe2=actualizar_importe(id_pedido_aux);
					if(btcif.getText()!=""){
						entrega=Double.parseDouble(btcif.getText());
					}else{
						entrega=importe2;
					}
					if(entrega>=importe2){
						pedido_aux=current_pedido;
						importe_aux=importe;
						importe=importe2;
						current_pedido=id_pedido_aux;
						imprimir(entrega,(entrega-importe2));
						importe=importe_aux;
						current_pedido=pedido_aux;
						la_e_aux.setText(""+df.format(entrega));
						la_c_aux.setText(""+df.format(entrega-importe2));
						sc_m_aux.setVisible(false);
						pa_c_aux.setVisible(true);
						
						
						//cobrar_mesa(id_pedido_aux);
						query="insert into cobros (pedido,entrega,sesion,caja) values ("+id_pedido_aux+","+entrega+","+sesion_actual+","+obj_conf.numerocaja+")";
						statement.execute(query);
						query="update pedidos set horac='"+new java.sql.Timestamp((new java.util.Date()).getTime())+"' where id="+id_pedido_aux;
						statement.execute(query);
						//imprimir();
						
						//query="select mesa,camarero,dependienta,importe,horaa,tiempo from pedidos where id="+current_pedido;
						query="select pedidos.mesa,pedidos.camarero,pedidos.dependienta,pedidos.importe,pedidos.horaa,pedidos.tiempo,situacion.nombre from pedidos,mesas,situacion where pedidos.id="+current_pedido+" and pedidos.mesa=mesas.id and mesas.tipo=situacion.id";
						
						try{
							rs=statement.executeQuery(query);
							rs.first();
							int mesa_aux=rs.getInt(1);
							
							if(rs.getString("nombre").equals("Barra")){
								query="insert into pedidos (mesa,dependienta,importe,horaa,tiempo) values ("+rs.getInt(1)+","+rs.getInt(3)+",0,'"+rs.getTimestamp(5)+"',"+rs.getInt(6)+")";
								statement.execute(query);
							}else{
								query="insert into pedidos (mesa,camarero,dependienta,importe,horaa,tiempo) values ("+rs.getInt(1)+","+rs.getInt(2)+","+rs.getInt(3)+",0,'"+rs.getTimestamp(5)+"',"+rs.getInt(6)+")";
								statement.execute(query);	
							}
							query="select id from pedidos where mesa="+mesa_aux+" and importe=0 and horac is null";
							rs=statement.executeQuery(query);
							rs.first();
							id_pedido_aux=rs.getInt(1);
						}catch (Exception e){
							e.printStackTrace();
						}
						
						actualizar_lista_prod(current_pedido,li_m_princ);
						actualizar_lista_prod(id_pedido_aux,li_m_aux);
						la_m_princ.setText(""+df.format(actualizar_importe(current_pedido)));
						la_m_aux.setText(""+df.format(actualizar_importe(id_pedido_aux)));
						
						btcif.setText("");
					}else{
						JOptionPane.showMessageDialog(null, "La cantidad introducida es menor\nque el importe total de la cuenta");
					}
					
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		});
		Bc_m_aux.setBounds(0,320,100,80);
		pa_aux.add(Bc_m_aux);
		
		la_total_ax.setBounds(110,320,60,80);
		la_total_ax.setFont(ft_bot_general);
		la_total_ax.setForeground(marron);
		pa_aux.add(la_total_ax);
		
		la_m_aux.setBounds(180,320,150,80);
		la_m_aux.setFont(fuente_importe);
		la_m_aux.setForeground(Color.blue);
		pa_aux.add(la_m_aux);
		
		
		Bc_izq=new JImageButton("<-----");
		Bc_izq.setBackground(Color.white);
		Icono icon=(new Icono("resources/images/interfaz/flecha_izquierda.jpg"));
		BufferedImage bi;
		try {
			bi=ImageIO.read(new File("resources/images/interfaz/flecha_izquierda.jpg"));
			Bc_izq.setBackgroundImage(icon.getScaledInstance(bi,50, 50,RenderingHints.VALUE_INTERPOLATION_BILINEAR,true));	
		} catch (IOException e) {
			e.printStackTrace();
		}
		Bc_izq.setBounds(330,160,60,60);
		Bc_izq.addActionListener(new ActionListener(){  public void actionPerformed(ActionEvent ev){
			String desc_aux;
			int id_aux,cant_aux,index,prod,tiemp,borr=0,cant_mov;
			double price;
			
			sc_m_aux.setVisible(true);
			pa_c_aux.setVisible(false);
			sc_m_princ.setVisible(true);
			pa_c_princ.setVisible(false);
			
			int indx=li_m_aux.getSelectedIndex();
			if(btcif.getText()!=""){
				try{
					cant_mov=Integer.parseInt(btcif.getText());
				}catch (Exception e){
					cant_mov=1;
				}
			}else{
				cant_mov=1;
			}
			if(indx>0){
				query="select servicios_temp.id,servicios_temp.cantidad,descripcion,servicios_temp.precio,servicios_temp.producto,servicios_temp.tiempo from servicios_temp,servicios_temp_borrados where pedido="+id_pedido_aux+"  except (select servicios_temp_borrados.servicio,servicios_temp.cantidad,servicios_temp.descripcion,servicios_temp.precio,servicios_temp.producto,servicios_temp.tiempo from servicios_temp_borrados,servicios_temp where servicios_temp_borrados.cantidad=servicios_temp.cantidad and servicios_temp_borrados.servicio=servicios_temp.id) order by id";
				try{
					rs=statement.executeQuery(query);
					index=li_m_aux.getSelectedIndex();
				
					rs.absolute(index);
					
					query="select cantidad from servicios_temp_borrados where servicio="+rs.getInt(1);
					rs2=statement2.executeQuery(query);
					if(rs2.first()){ 
						borr=rs2.getInt(1);
					}
					id_aux=rs.getInt(1);
					cant_aux=rs.getInt(2)-borr;
					desc_aux=rs.getString(3);
					price=rs.getDouble(4);
					prod=rs.getInt(5);
					tiemp=rs.getInt(6);
					if(cant_aux-cant_mov<=0){
						query="select id from servicios_temp where descripcion='"+desc_aux+"' and producto="+prod+" and precio="+price+" and tiempo="+tiemp+" and pedido="+current_pedido;
						rs=statement.executeQuery(query);
						if(!rs.first()){
							query="update servicios_temp set pedido="+current_pedido+" where id="+id_aux;
							statement.executeUpdate(query);
						}else{
							rs.first();
							query="update servicios_temp set cantidad=cantidad+"+cant_mov+" where id="+rs.getInt(1);
							statement.executeUpdate(query);
							query="delete from servicios_temp where id="+id_aux;
							statement.execute(query);
						}
						
					}else{
						query="update servicios_temp set cantidad=cantidad-"+cant_mov+" where id="+id_aux;
						statement.executeUpdate(query);
						query="select id from servicios_temp where descripcion='"+desc_aux+"' and producto="+prod+" and precio="+price+" and tiempo="+tiemp+" and pedido="+current_pedido;
						rs=statement.executeQuery(query);
						if(!rs.first()){
							query="insert into servicios_temp (producto,descripcion,precio,cantidad,pedido,tiempo) values ("+prod+",'"+desc_aux+"',"+price+","+cant_mov+","+current_pedido+","+tiemp+")";
							statement.execute(query);
						}else{
							rs.first();
							query="update servicios_temp set cantidad=cantidad+"+cant_mov+" where id="+rs.getInt(1);
							statement.executeUpdate(query);
						}
					}
					actualizar_lista_prod(current_pedido,li_m_princ);
					actualizar_lista_prod(id_pedido_aux,li_m_aux);
					la_m_princ.setText(""+df.format(actualizar_importe(current_pedido)));
					la_m_aux.setText(""+df.format(actualizar_importe(id_pedido_aux)));
					btcif.setText("");
					
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		}});
		
		pa_total.add(Bc_izq);
		
		Bc_der=new JImageButton("----->");
		Bc_der.setBackground(Color.white);
		icon=(new Icono("resources/images/interfaz/flecha_derecha.jpg"));
		try {
			bi=ImageIO.read(new File("resources/images/interfaz/flecha_derecha.jpg"));
			Bc_der.setBackgroundImage(icon.getScaledInstance(bi,50, 50,RenderingHints.VALUE_INTERPOLATION_BILINEAR,true));	
		} catch (IOException e) {
			e.printStackTrace();
		}
		Bc_der.setBounds(330,80,60,60);
		Bc_der.addActionListener(new ActionListener(){ public void actionPerformed(ActionEvent ev){
			String desc_aux;
			int id_aux,cant_aux,index,prod,tiemp,borr=0,cant_mov;
			double price;
			
			sc_m_aux.setVisible(true);
			pa_c_aux.setVisible(false);
			sc_m_princ.setVisible(true);
			pa_c_princ.setVisible(false);
			
			int indx=li_m_princ.getSelectedIndex();
			if(btcif.getText()!=""){
				try{
					cant_mov=Integer.parseInt(btcif.getText());
				}catch (Exception e){
					cant_mov=1;
				}
			}else{
				cant_mov=1;
			}
			if(indx>0){
				System.out.println("pedido actual="+current_pedido);
				query="select servicios_temp.id,servicios_temp.cantidad,servicios_temp.descripcion,servicios_temp.precio,servicios_temp.producto,servicios_temp.tiempo from servicios_temp,servicios_temp_borrados where pedido="+current_pedido+" except (select servicios_temp_borrados.servicio,servicios_temp.cantidad,servicios_temp.descripcion,servicios_temp.precio,servicios_temp.producto,servicios_temp.tiempo from servicios_temp_borrados,servicios_temp where servicios_temp_borrados.cantidad=servicios_temp.cantidad and servicios_temp_borrados.servicio=servicios_temp.id) order by id";
				try{
					rs=statement.executeQuery(query);
					
					while(rs.next()){
						System.out.println("leido: "+rs.getInt(1));
					}
					System.out.println("indice "+indx);
					rs.absolute(indx);
					int id=rs.getInt(1);
					query="select cantidad from servicios_temp_borrados where servicio="+id;
					rs2=statement2.executeQuery(query);
					if(rs2.first()){
						borr=rs2.getInt(1);
					}
					id_aux=rs.getInt(1);
					cant_aux=rs.getInt(2)-borr;
					desc_aux=rs.getString(3);
					price=rs.getDouble(4);
					prod=rs.getInt(5);
					tiemp=rs.getInt(6);
					if(cant_aux-cant_mov<=0){
						query="select id from servicios_temp where descripcion='"+desc_aux+"' and producto="+prod+" and precio="+price+" and tiempo="+tiemp+" and pedido="+id_pedido_aux;
						rs=statement.executeQuery(query);
						if(!rs.first()){
							query="update servicios_temp set pedido="+id_pedido_aux+" where id="+id_aux;
							statement.executeUpdate(query);
						}else{
							rs.first();
							query="update servicios_temp set cantidad=cantidad+"+cant_mov+" where id="+rs.getInt(1);
							statement.executeUpdate(query);
							query="delete from servicios_temp where id="+id_aux;
							statement.execute(query);
						}
						
					}else{
						query="update servicios_temp set cantidad=cantidad-"+cant_mov+" where id="+id_aux;
						statement.executeUpdate(query);
						query="select id from servicios_temp where descripcion='"+desc_aux+"' and producto="+prod+" and precio="+price+" and tiempo="+tiemp+" and pedido="+id_pedido_aux;
						rs=statement.executeQuery(query);
						if(!rs.first()){
							query="insert into servicios_temp (producto,descripcion,precio,cantidad,pedido,tiempo) values ("+prod+",'"+desc_aux+"',"+price+","+cant_mov+","+id_pedido_aux+","+tiemp+")";
							statement.execute(query);
						}else{
							rs.first();
							query="update servicios_temp set cantidad=cantidad+"+cant_mov+" where id="+rs.getInt(1);
							statement.executeUpdate(query);
						}
						
					}
					actualizar_lista_prod(current_pedido,li_m_princ);
					actualizar_lista_prod(id_pedido_aux,li_m_aux);
					la_m_princ.setText(""+df.format(actualizar_importe(current_pedido)));
					la_m_aux.setText(""+df.format(actualizar_importe(id_pedido_aux)));
					btcif.setText("");
					
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		}});
		pa_total.add(Bc_der);
		
		
		
		v_separa.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				
				btcif.setText("");
				sc_m_aux.setVisible(true);
				pa_c_aux.setVisible(false);
				sc_m_princ.setVisible(true);
				pa_c_princ.setVisible(false);
				Pa_cobrar.setVisible(false);
				Sc_prod.setVisible(true);
				
				if(e.getSource()==v_separa){
				query="select id,producto,descripcion,precio,tiempo,cantidad from servicios_temp where pedido="+id_pedido_aux;
				try{
					rs=statement.executeQuery(query);
					if(rs.first()){
						do{
							query="select id from servicios_temp where pedido="+current_pedido+" and producto="+rs.getInt(2)+" and descripcion='"+rs.getString(3)+"' and precio="+rs.getDouble(4)+" and tiempo="+rs.getInt(5);
							rs2=statement2.executeQuery(query);
							if(rs2.first()){
								query="update servicios_temp set cantidad=cantidad+"+rs.getInt(6)+" where id="+rs2.getInt(1);
								statement2.execute(query);
								query="delete from servicios_temp where id="+rs.getInt(1);		
							}else{
								query="update servicios_temp set pedido="+current_pedido+" where id="+rs.getInt(1);
							}
							statement2.execute(query);
						}while(rs.next());
					}
					query="delete from pedidos where id="+id_pedido_aux;
					statement.execute(query);
					actualizar_datos();
				}catch (Exception ex){
					ex.printStackTrace();
				}
			}}
		});
		v_separa.setVisible(true);
		
	}
	
	
	
	
	private void res_cifra(){
		Bt_cifra.setText("");
		b_coma=false;
		decimales=0;
	}
	
	/*
	 * 
	 * M�todo de control de eventos.
	 * 
	 * 
	 */
	
	public void actionPerformed(ActionEvent ev) {
		
		if(interfazcreada==false){
			javax.swing.SwingUtilities.invokeLater (() -> crearinterfaz());
			interfazcreada=true;
		}
		JButtonEnlazado aux;
		boolean encontrado=false;
		boolean sit;
		Sc_prod.setVisible(true);
		Pa_cobrar.setVisible(false);
		query="select id from sesiones where activa=true";
		try{
			rssesion=statementsesion.executeQuery(query);
			rssesion.first();
			sesion_actual=rssesion.getInt("id");
		}catch (Exception e){
			
		}
		
		/*
		 * 		Listener grid mesas
		 * 
		 * 
		 */
		
		if(Pa_principal.isVisible()){
			actualizar_grid();
			if (ev.getSource()==Bt_barra){
				((CardLayout) Pa_terbar.getLayout()).show(Pa_terbar,"barra");
				encontrado=true;
			}else if(ev.getSource()==Bt_terraza){
				((CardLayout) Pa_terbar.getLayout()).show(Pa_terbar,"terraza");
				encontrado=true;
			}else if(ev.getSource()==Bt_cerrar){
				encontrado=true;
				actualizar_grid();
			}else if(ev.getSource()==Bt_opciones){
				encontrado=true;
				menu_opciones();
			}else if(ev.getSource()==Bt_add_camarero){
				encontrado=true;
				sacar=false;
				anadir_camarero(0);
			}else if(ev.getSource()==Bt_add_dependienta){
				encontrado=true;
				sacar=false;
				anadir_camarero(1);
			}else if(ev.getSource()==Bt_out_camarero){
				encontrado=true;
				sacar=true;
				seleccionar_camarero(0);
				
			}else if(ev.getSource()==Bt_out_dependienta){
				encontrado=true;
				sacar=true;
				seleccionar_camarero(1);
			}else if(ev.getSource()==Bt_config){
				menu_configuracion();
			}else if(ev.getSource()==Bt_jefe){
				menu_jefe();
			}else if(ev.getSource()==Bt_salir){
				if(comprobar_pass(introd_pass())){
					if(obj_conf.bol_replicante)p.destroy();
					System.exit(0);
				}
			}else if(ev.getSource()==Bt_sig_pan_cam){
				encontrado=true;
				if(current_panel_camareros+1==n_pans_camareros){
					
				}else{
					Sc_camareros[current_panel_camareros].setVisible(false);
					current_panel_camareros++;
					Sc_camareros[current_panel_camareros].setVisible(true);
				}
			}else if(ev.getSource()==Bt_ant_pan_cam){
				encontrado=true;
				if(current_panel_camareros!=0){
					Sc_camareros[current_panel_camareros].setVisible(false);
					current_panel_camareros--;
					Sc_camareros[current_panel_camareros].setVisible(true);
				}
			}else if(ev.getSource()==Bt_sig_pan_dep){
				encontrado=true;
				if(current_panel_dependientas+1==n_pans_dependientas){
					
				}else{
					Sc_camareros[current_panel_dependientas].setVisible(false);
					current_panel_dependientas++;
					Sc_camareros[current_panel_dependientas].setVisible(true);
				}
			}else if(ev.getSource()==Bt_ant_pan_dep){
				encontrado=true;
				if(current_panel_dependientas!=0){
					Sc_dependientas[current_panel_dependientas].setVisible(false);
					current_panel_dependientas--;
					Sc_dependientas[current_panel_dependientas].setVisible(true);
				}
			}else if(ev.getSource().getClass()==JButtonEnlazado.class){
				aux=lista_camareros;
				while(aux!=null){
					if(ev.getSource()==aux){
						try{
							query="Insert into jornadas (empleado,sesion) values ("+aux.get_index()+","+sesion_actual+")";
							statement.execute(query);
							menu_camareros.dispose();
							encontrado=true;
						}catch (Exception e){
							e.printStackTrace();
						}
					}
					aux=aux.get_siguiente();
				}
				aux=lista_dependientas;
				while(aux!=null){
					if(ev.getSource()==aux){
						try{
							query="Insert into jornadas (empleado,sesion) values ("+aux.get_index()+","+sesion_actual+")";
							statement.execute(query);
							menu_camareros.dispose();
							encontrado=true;
						}catch (Exception e){
							e.printStackTrace();
						}
					}
					aux=aux.get_siguiente();
				}
				aux=current_camareros;
				while(aux!=null){
					if(ev.getSource()==aux){
						asignar_a_mesa(aux,0,sacar);
						encontrado=true;
					}
					aux=aux.get_siguiente();
				}
				aux=current_dependientas;
				while(aux!=null){
					if(ev.getSource()==aux){
						asignar_a_mesa(aux,1,sacar);
						encontrado=true;
					}
					aux=aux.get_siguiente();
				}
				aux=lista;
				while(aux!=null){
					if(ev.getSource()==aux){
						n_mesa=aux.get_index();
						
						if(aux.getText().regionMatches(0, "<html><p align='center'>Mesa", 0, 27)){
							abrir_mesa(1);
						}else{
							abrir_mesa(2);
						}
						encontrado=true;
					}
					aux=aux.get_siguiente();
				}
				
			}else if(ev.getSource()==Bt_num){
				
				encontrado=true;
				if(Bt_num.getText()!=""){
					
					String str=Bt_num.getText();
					int num=Integer.parseInt(str);
					n_mesa=num;
					if(Pa_terraza.isVisible()){
						sit=true;
					}else{
						sit=false;
					}
					
					if(num>49 || num==0){
						if(buscar_en_lista(num,sit)==false){
							anadir_en_lista(num, sit);
						}
						abrir_mesa(0);
					}else{
						abrir_mesa(0);
					}
					Bt_num.setText("");
				}
			}else {
				for(int i=0;i<10;i++){
					if(ev.getSource()==Bt_numero[i]){
						encontrado=true;
						if(Bt_num.getText().length()==3){
							Bt_num.setText(""+i);
						}else{
							Bt_num.setText(Bt_num.getText()+i);
						}
					}
				}
			}
			if(!encontrado){
				if(Pa_terraza.isVisible()){
					for (int i=0;i<49;i++){
						if(ev.getSource()==Bt_mesas[i]){
							if(Bt_mesas[i].getBackground()==Color.red){
								JOptionPane.showMessageDialog(this, "La mesa est� abierta por otro usuario!!!!");
							}else{
								n_mesa=i+1;
								abrir_mesa(1);
							}
							
						}
					}
				}else if(Pa_barra.isVisible()){
					for (int i=0;i<49;i++){
						if(ev.getSource()==Bt_barras[i]){
							if(Bt_barras[i].getBackground()==Color.red){
								JOptionPane.showMessageDialog(this, "La mesa est� abierta por otro usuario!!!!");
							}else{
								n_mesa=i+1;
								abrir_mesa(2);
							}
						}
					}
				}
					
			}
		}
		/*
		 * 
		 * 		Listener panel secundario
		 * 
		 */
		else{
			for(int i=0;i<Bt_categoria.length;i++){
				if(ev.getSource()==Bt_categoria[i]){
					Pa_categorias[currentcategoria][currentpanel].setVisible(false);
					currentcategoria=i;
					currentpanel=0;
					Pa_categorias[currentcategoria][currentpanel].setVisible(true);
				}
			}
			for(int i=0;i<10;i++){
				if(ev.getSource()==Bt_number[i]){
					if(b_coma==false){
						if(Bt_cifra.getText().length()==3){
							res_cifra();
							Bt_cifra.setText(""+i);
						}else{
							Bt_cifra.setText(Bt_cifra.getText()+i);
						}
					}else{
						if(decimales<2){
							Bt_cifra.setText(Bt_cifra.getText()+i);
							decimales++;
						}
					}
				}
			}
			if(ev.getSource()==Bt_coma && b_coma==false){
				Bt_cifra.setText(Bt_cifra.getText()+".");
				b_coma=true;
			}else if(ev.getSource()==Bt_cifra){
				res_cifra();
			}else if(((JButton)ev.getSource()).getText()=="----->"){
				Pa_categorias[currentcategoria][currentpanel].setVisible(false);
				currentpanel++;
				Pa_categorias[currentcategoria][currentpanel].setVisible(true);
			}else if(((JButton) ev.getSource()).getText()=="<-----"){
				Pa_categorias[currentcategoria][currentpanel].setVisible(false);
				currentpanel--;
				Pa_categorias[currentcategoria][currentpanel].setVisible(true);
			}else if(ev.getSource().getClass()==JCustomButton.class && ((JCustomButton)ev.getSource()).getText()!=""){
				System.out.println("Empieza: "+Calendar.getInstance().getTimeInMillis());
				if(situacion.regionMatches(0, "Terraza", 0, 6)){
					query="SELECT servicios_temp.id,servicios_temp.cantidad FROM servicios_temp,productos WHERE servicios_temp.pedido="+current_pedido+" and servicios_temp.tiempo="+tiempo_mesa+" and servicios_temp.producto="+((JCustomButton)(ev.getSource())).get_id()+" and servicios_temp.precio=productos.preciot and servicios_temp.descripcion=productos.nombre";
				}else{
					query="SELECT servicios_temp.id,servicios_temp.cantidad FROM servicios_temp,productos WHERE servicios_temp.pedido="+current_pedido+" and servicios_temp.tiempo="+tiempo_mesa+" and servicios_temp.producto="+((JCustomButton)(ev.getSource())).get_id()+" and servicios_temp.precio=productos.preciob and servicios_temp.descripcion=productos.nombre";
				}
				unidades=1;
				if(Bt_cifra.getText()!="" && b_coma==false){
					unidades=Integer.parseInt(Bt_cifra.getText());
				}
				try{
					rs=statement.executeQuery(query);
					System.out.println("Busqueda: "+Calendar.getInstance().getTimeInMillis());
					if(rs.next()){
						query="UPDATE servicios_temp SET CANTIDAD="+(rs.getInt(2)+unidades)+" WHERE id="+rs.getInt(1);
						statement.executeUpdate(query);
						System.out.println("Update +1 : "+Calendar.getInstance().getTimeInMillis());
					}else{
						if(situacion.regionMatches(0, "Terraza", 0, 7)){
							query="INSERT INTO servicios_temp (producto,descripcion,cantidad,precio,pedido,TIEMPO) VALUES ("+((JCustomButton)ev.getSource()).get_id()+",(select nombre from productos where id="+((JCustomButton)ev.getSource()).get_id()+"),"+unidades+","+((JCustomButton)ev.getSource()).get_precioT()+","+current_pedido+","+tiempo_mesa+")";
						}else{
							//query="INSERT INTO servicios (producto,descripcion,cantidad,precio,pedido,TIEMPO) VALUES ("+((JCustomButton)ev.getSource()).get_id()+",'"+((JCustomButton)ev.getSource()).getText()+"',"+unidades+","+((JCustomButton)ev.getSource()).get_precioB()+","+current_pedido+","+tiempo_mesa+")";	
							query="INSERT INTO servicios_temp (producto,descripcion,cantidad,precio,pedido,TIEMPO) VALUES ("+((JCustomButton)ev.getSource()).get_id()+",(select nombre from productos where id="+((JCustomButton)ev.getSource()).get_id()+"),"+unidades+","+((JCustomButton)ev.getSource()).get_precioB()+","+current_pedido+","+tiempo_mesa+")";
						}
						statement.execute(query);
						System.out.println("Insert: "+Calendar.getInstance().getTimeInMillis());
					}
				}catch (Exception ex){
					ex.printStackTrace();
				}
				res_cifra();
				actualizar_datos();
			}else if(ev.getSource()==Bt_cerrar2){
				res_cifra();
				actualizar_grid();
				cerrar_mesa();
			}else if(ev.getSource()==Bt_terraza2 && b_coma==false && Bt_cifra.getText()!=""){
				
				n_mesa=Integer.parseInt(Bt_cifra.getText());
				if(n_mesa<50 && n_mesa>0 && Bt_mesas[n_mesa-1].getBackground()==Color.red){
					JOptionPane.showMessageDialog(this, "La mesa est� abierta por otro usuario!!!!");
				}else{
				//current_mesa="Mesa_"+n_mesa;
					cerrar_mesa();
					abrir_mesa(1);
					res_cifra();
				}
			}else if(ev.getSource()==Bt_barra2 && b_coma==false && Bt_cifra.getText()!=""){
				
				n_mesa=Integer.parseInt(Bt_cifra.getText());
				if(n_mesa<50 && n_mesa>0 && Bt_barras[n_mesa-1].getBackground()==Color.red){
					JOptionPane.showMessageDialog(this, "La mesa est� abierta por otro usuario!!!!");
				}else{
				//current_mesa="Barra_"+n_mesa;
					cerrar_mesa();
					abrir_mesa(2);
					res_cifra();
				}
			}else if(ev.getSource()==Bt_cobrar){
				cobrar_mesa();
				res_cifra();
			}else if(ev.getSource()==Bt_borrar){
				borrar_producto();
			}else if(ev.getSource()==Bt_camarero){
				sacar=false;
				seleccionar_camarero(0);
				actualizar_duenos();
			}else if(ev.getSource()==Bt_camarera){
				sacar=false;
				seleccionar_camarero(1);
				actualizar_duenos();
			}else if(ev.getSource()==Bt_tiquet){
				if(tiquet==false){
					tiquet=true;
					Bt_tiquet.setText("Tiquet: Si");
				}else{
					tiquet=false;
					Bt_tiquet.setText("Tiquet: No");
				}
			}else if (ev.getSource()==Bt_imprimir){
				imprimir();
			}else if (ev.getSource()==Bt_juntar){
				juntar_mesas();
			}else if(ev.getSource()==Bt_separa){
				separar_cuenta();
			}else if (ev.getSource()==Bt_invita){
				invitar();
			}else if (ev.getSource()==Bt_precio){
				modif_precio(Double.parseDouble(Bt_cifra.getText()));
				res_cifra();
			}else if(ev.getSource()==Bt_unidades && b_coma==false && Bt_cifra.getText()!=""){
				establecer_cantidad(Integer.parseInt(Bt_cifra.getText()));
				res_cifra();
			}else if(ev.getSource()==Bt_can_cobro){
				cancelar_cobro();
			}else if(ev.getSource().getClass()==JButtonEnlazado.class){
				aux=current_camareros;
				while(aux!=null){
					if(ev.getSource()==aux){
						asignar_a_mesa(aux,0,sacar);
					}
					aux=aux.get_siguiente();
				}
				aux=current_dependientas;
				while(aux!=null){
					if(ev.getSource()==aux){
						asignar_a_mesa(aux,1,sacar);
					}
					aux=aux.get_siguiente();
				}
			}
		
		/*
		 * 
		 * 
		 * 
		 * 
		 */
		}
		
		

	}
}




	

