package interfaz;

import java.awt.Color;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;

import javax.swing.JButton;

public class ListenThread extends Thread{

	private DatagramSocket sck;
	private ServerSocket svrs;
	private Socket skt;
	BufferedReader in;
	private final int puerto=6543;
	private DatagramPacket entrada;
	private byte [] buffer ;
	private String[] lectura;
	private int numero;
	private JButton mesas[],barras[];
	private String impresora;
	private Impresora disp;
	
	private Connection db_conec;
	private Statement statement,statement2;
	private ResultSet rs,rs2;
	private String query;
	
	
	public ListenThread(JButton mesas[],JButton barras[],String imp){
		super("ListenThread");
		try{
			db_conec =DriverManager.getConnection("jdbc:postgresql://localhost:5432/venecia","postgres","venecia2013");
			statement=db_conec.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			statement2=db_conec.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			//lectura=new String[5];
			this.mesas=mesas;
			this.barras=barras;
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Error de conexion con la base de datos");
		}
		try{
			System.out.println("abriendo puerto");
			//svrs=new ServerSocket(puerto);
			System.out.println("socket creado");
			//in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
			System.out.println("Todo listo");
			//while(!in.ready()){}
			sck=new DatagramSocket(puerto,InetAddress.getLocalHost());
			buffer=new byte [1000];
			entrada=new DatagramPacket(buffer,1000);
			
		}catch (Exception e){
			e.printStackTrace();
			System.out.println("Error al crear el socket.!!!!");
		}
		
		try{
			impresora=imp;
			disp=new Impresora();
			disp.setDispositivo(impresora);
			disp.centrar();
			disp.correr(3);
			disp.escribirln("Hilo iniciado");
			disp.correr(1);
			disp.dividir();
			disp.correr(3);
			disp.cortar();
			disp.cerrarDispositivo();
			Toolkit.getDefaultToolkit().beep(); 
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Error al iniciar impresora");
		}
		
	}/*
	public void run(){
		while(true){
			try{
				buffer=new byte [1000];
				entrada.setData(buffer);
				sck.receive(entrada);
				lectura=null;
				disp=new Impresora();
				disp.setDispositivo(impresora);
				
				
				if(!entrada.getAddress().getHostAddress().equals(InetAddress.getLocalHost().getHostAddress())){
					lectura=new String(entrada.getData());
					if(lectura.charAt(0)=='#'){
						if(lectura.charAt(1)=='C'){
							disp.correr(2);
							disp.escribirln("######################################");
							disp.escribirln("MESA "+lectura.substring(8));
							disp.escribirln("--------------------------------------");
							disp.emitir_sonido();
						}else{
							disp.escribirln("--------------------------------------");
							disp.escribirln("######################################");
							disp.correr(5);
							disp.cortar();
							disp.emitir_sonido();
						}
					}else{
						disp.escribirln(lectura);
						disp.emitir_sonido();
					}
				}else {
					System.out.println("Recibido paquete desde propio pc.");
				}
				disp.cerrarDispositivo();
			}catch(Exception e){
				System.out.println("Error en lectura de socket.!!!!");
				e.printStackTrace();
			}
			
		}
	}*/
	
	public void run(){
		boolean anulacion=false,accion=false;
		String leido;
		while(true){
			try{
				//formato=> mesa,pedido,tiempo,camarero,accion
				accion=false;
				anulacion=false;
				System.out.println("Hilo activo");
				sck.receive(entrada);
			
				//skt=svrs.accept();
				System.out.println("socket aceptado");
				
				//BufferedReader in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
				//lectura=in.readLine().split(":");
				lectura=new String(entrada.getData()).split(":");
				//leido=in.readLine();
				
				//System.out.println(leido);
				//lectura=leido.split(":");
				//System.out.println(lectura[0]+":"+lectura[1]+":"+lectura[2]);
				//System.out.println(new String(entrada.getData()));
				disp=new Impresora();
				disp.setDispositivo(impresora);
				disp.setFormato(16);
				//if(!entrada.getAddress().getHostAddress().equals(InetAddress.getLocalHost().getHostAddress())){
					//lectura=new String(entrada.getData()).split(":");
					
					lectura[4]=lectura[4].substring(0, 6);
					System.out.println("lectura[4]->"+lectura[4]);
					
					query="select servicios_temp_borrados.id from servicios_temp,servicios_temp_borrados where servicios_temp.pedido="+lectura[1]+" and servicios_temp_borrados.servicio=servicios_temp.id and servicios_temp_borrados.tiempo="+lectura[2];
					try{
						rs=statement.executeQuery(query);
						if(rs.first()){
							anulacion=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					
					if (anulacion){
						query="select servicios_temp.id,servicios_temp_borrados.cantidad,servicios_temp.descripcion from servicios_temp,servicios_temp_borrados where servicios_temp_borrados.servicio=servicios_temp.id and servicios_temp_borrados.tiempo="+lectura[2]+" and servicios_temp.pedido="+lectura[1];
						rs=statement.executeQuery(query);
						if(rs.first()){
							disp.correr(2);
							disp.centrar();
							disp.escribirln("######################################");
							disp.escribirln("######################################");
							disp.escribirln("######################################");
							disp.setFormato(24);
							disp.escribirln("ANULACION");
							disp.setFormato(16);
							disp.escribirln("######################################");
							disp.escribirln("######################################");
							disp.escribirln("######################################");
							disp.correr(1);
							
							disp.escribirln(""+Calendar.getInstance().get(Calendar.HOUR_OF_DAY)+":"+Calendar.getInstance().get(Calendar.MINUTE)+":"+Calendar.getInstance().get(Calendar.SECOND));
							
							disp.setFormato(24);
							disp.escribir("MESA "+lectura[0]+" \t\t\t ");
							disp.setFormato(16);
							disp.escribirln(lectura[3]);
							
							
							disp.escribirln("--------------------------------------");
							disp.correr(1);
							disp.izquierda();
							query="select servicios_temp.id,servicios_temp_borrados.cantidad,servicios_temp.descripcion from servicios_temp,servicios_temp_borrados where servicios_temp_borrados.servicio=servicios_temp.id and servicios_temp_borrados.tiempo="+lectura[2]+" and servicios_temp.pedido="+lectura[1];
							rs=statement.executeQuery(query);
							while(rs.next()){
								disp.escribirln(" "+rs.getInt(2)+"\t"+rs.getString(3));
								query="select productos.nombre from modificaciones_temp,productos where modificaciones_temp.servicio="+rs.getInt(1)+" and productos.id=modificaciones_temp.modificacion ";
								rs2=statement2.executeQuery(query);
								while(rs2.next()){
									disp.escribirln("\t\t >"+rs2.getString(1));
								}
							}
							disp.centrar();
							disp.escribirln("--------------------------------------");
							disp.escribirln("######################################");
							disp.correr(7);
							disp.cortar();
							Toolkit.getDefaultToolkit().beep(); 
							Toolkit.getDefaultToolkit().beep(); 
						}
					}
					
					query="select id from servicios_temp where pedido="+lectura[1]+" and tiempo="+lectura[2];
					rs=statement.executeQuery(query);
					if(rs.first()){
						accion=true;
					}
					
					
					if(accion){
						query="select servicios_temp.id,servicios_temp.cantidad,servicios_temp.descripcion from servicios_temp where servicios_temp.pedido="+lectura[1]+" and servicios_temp.tiempo="+lectura[2];//+" except (select servicios.id,servicios.cantidad-servicios_borrados.cantidad as cantidad,servicios.descripcion from servicios,servicios_borrados where servicios.id=servicios_borrados.servicio and servicios.pedido="+lectura[1]+" and servicios.tiempo="+lectura[2]+" and servicios.cantidad-servicios_borrados.cantidad<1)";
						rs=statement.executeQuery(query);
						if(rs.first()){
							disp.correr(2);
							disp.centrar();
							disp.escribirln("######################################");
							disp.setFormato(24);
							disp.escribirln("COMANDA");
							disp.setFormato(16);
							disp.escribirln("######################################");
							disp.correr(1);
							disp.escribirln(""+Calendar.getInstance().get(Calendar.HOUR_OF_DAY)+":"+Calendar.getInstance().get(Calendar.MINUTE)+":"+Calendar.getInstance().get(Calendar.SECOND));
							disp.setFormato(24);
							disp.escribir("MESA "+lectura[0]+" \t\t\t ");
							disp.setFormato(16);
							disp.escribirln(lectura[3]);
							disp.escribirln("--------------------------------------");
							disp.correr(1);
							disp.izquierda();
							query="select servicios_temp.id,servicios_temp.cantidad,servicios_temp.descripcion from servicios_temp where servicios_temp.pedido="+lectura[1]+" and servicios_temp.tiempo="+lectura[2];//+" except (select servicios.id,servicios.cantidad-servicios_borrados.cantidad as cantidad,servicios.descripcion from servicios,servicios_borrados where servicios.id=servicios_borrados.servicio and servicios.pedido="+lectura[1]+" and servicios.tiempo="+lectura[2]+" and servicios.cantidad-servicios_borrados.cantidad<1)";
							rs=statement.executeQuery(query);
							while(rs.next()){
								disp.escribirln(" "+rs.getInt(2)+"\t"+rs.getString(3));
								query="select productos.nombre from modificaciones_temp,productos where modificaciones_temp.servicio="+rs.getInt(1)+" and productos.id=modificaciones_temp.modificacion ";
								rs2=statement2.executeQuery(query);
								while(rs2.next()){
									disp.escribirln("\t\t >"+rs2.getString(1));
								}
							}
							disp.centrar();
							disp.escribirln("--------------------------------------");
							disp.escribirln("######################################");
							disp.correr(7);
							disp.cortar();
							Toolkit.getDefaultToolkit().beep(); 
						}
					}
				/*}else{
					System.out.println("Recibido paquete desde propio pc.");
				}*/
				disp.cerrarDispositivo();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
}
