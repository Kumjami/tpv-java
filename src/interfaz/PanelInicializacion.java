package interfaz;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;



@SuppressWarnings("serial")
public class PanelInicializacion extends JFrame implements ActionListener{

	
	private JButton bt_entrar,bt_config;
	private JPanel pa_inf,pa_med;
	private ImageIcon img;
	
	
	public PanelInicializacion(){
		super("Launcher TPV");
		this.setIconImage( Toolkit.getDefaultToolkit().getImage("resources/images/interfaz/iconogondola.jpg"));
		this.setLayout(new BorderLayout());
		this.setSize(800,500);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		pa_med=new JPanel(new BorderLayout());
		pa_inf=new JPanel(new BorderLayout());
		this.add(pa_med,BorderLayout.CENTER);
		this.add(pa_inf,BorderLayout.SOUTH);
		
		bt_entrar=new JButton("ENTRAR");
		bt_entrar.addActionListener(this);
		bt_entrar.setPreferredSize(new Dimension(200,100));
		pa_inf.add(bt_entrar,BorderLayout.CENTER);
		
		bt_config=new JButton("CONFIGURACION");
		bt_config.addActionListener(this);
		bt_config.setPreferredSize(new Dimension(200,100));
		pa_inf.add(bt_config,BorderLayout.WEST);
		img=new ImageIcon("resources/images/interfaz/logo.jpg");
		pa_med.add(new JLabel(img),BorderLayout.CENTER);
		this.getContentPane().setBackground(Color.WHITE);
		pa_med.setBackground(Color.WHITE);
		
		this.setVisible(true);
	}
	

	public void actionPerformed(ActionEvent ev) {
		if(ev.getSource()==bt_entrar){
			javax.swing.SwingUtilities.invokeLater (() -> new VeneciaTPV());
			this.dispose();
			
		}else if(ev.getSource()==bt_config){
			new ArchivoConfig(this);
		}
	}
}
