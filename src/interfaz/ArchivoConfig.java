package interfaz;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.*;
import nu.xom.*;


public class ArchivoConfig implements ActionListener{
	
	
	private JDialog menu_conf;
	private JCheckBox cb_modif,cb_replica,cb_habilitar;
	private JTextField tf_imp_tiq,tf_imp_com;
	private JLabel la_rep,la_imp_tiq,la_imp_com,la_modif,la_caja,la_habilitar;
	private JButton bt_apli,bt_cancel,bt_generar;
	private JPanel pa_impresoras,pa_cont,pa_ta1,pa_ta3;
	private JComboBox combo_caja;
	private JTextArea ta_head1,ta_footer;
	
	private final Color azulclaro=new Color(175,224,254),azuloscuro=new Color(39,196,239),marron=new Color(82,53,21),blanco=Color.white,azul=Color.blue;
	private final Font fuente_gen=new Font("TimesRoman",Font.BOLD,15),fuente_ta=new Font("Monospaced",Font.PLAIN,12);
	
	public ArchivoConfig(JFrame own){
		menu_conf=new JDialog(own,"Configuración",true);
		menu_conf.setSize(400,600);
		menu_conf.setLayout(null);
		menu_conf.setLocationRelativeTo(null);
	
		menu_conf.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		menu_conf.getContentPane().setBackground(azulclaro);
		
		la_caja=new JLabel("Caja");
		la_caja.setEnabled(false);
		la_caja.setFont(fuente_gen);
		la_caja.setForeground(azul);
		la_caja.setBounds(20,10,50,30);
		menu_conf.add(la_caja);
		
		combo_caja=new JComboBox();
		combo_caja.setEnabled(false);
		combo_caja.addItem("Caja 1");
		combo_caja.addItem("Caja 2");
		combo_caja.setBounds(60,10,100,30);
		menu_conf.add(combo_caja);
		
		la_habilitar=new JLabel("Editar");
		la_habilitar.setFont(fuente_gen);
		la_habilitar.setForeground(azul);
		la_habilitar.setBounds(240,10,40,30);
		menu_conf.add(la_habilitar);
		
		cb_habilitar=new JCheckBox();
		cb_habilitar.addActionListener(this);
		cb_habilitar.setBounds(290,10,100,30);
		menu_conf.add(cb_habilitar);
		
		pa_cont=new JPanel(null);
		pa_cont.setBackground(azuloscuro);
		pa_cont.setBorder(BorderFactory.createLineBorder(blanco, 5));
		
		la_modif=new JLabel("Modificadores");
		la_modif.setEnabled(false);
		la_modif.setForeground(azul);
		la_modif.setBounds(20,10,80,30);
		pa_cont.add(la_modif);
		
		cb_modif=new JCheckBox();
		cb_modif.setEnabled(false);
		cb_modif.setBounds(110,10, 20, 30);
		pa_cont.add(cb_modif);
		
		la_rep=new JLabel("Replicador");
		la_rep.setEnabled(false);
		la_rep.setForeground(azul);
		la_rep.setBounds(250,10,80, 30);
		pa_cont.add(la_rep);
		
		cb_replica=new JCheckBox();
		cb_replica.setEnabled(false);
		cb_replica.setBounds(330,10,20,30);
		pa_cont.add(cb_replica);
		
		pa_ta1=new JPanel(new BorderLayout());
		
		ta_head1=new JTextArea(40,5);
		ta_head1.setFont(fuente_ta);
		ta_head1.setEnabled(false);
		ta_head1.setLineWrap(true);
		ta_head1.setWrapStyleWord(false);
		ta_head1.setTabSize(10);
		
		pa_ta1.add(ta_head1,BorderLayout.CENTER);
		pa_ta1.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.blue), "Cabecera tiquet",2,2,Font.getFont("Impresoras"),Color.blue));
		pa_ta1.setBackground(azuloscuro);
		pa_ta1.setBounds(30,50,310,150);//313,200
		pa_cont.add(pa_ta1);
		
		pa_ta3=new JPanel(new BorderLayout());
		
		ta_footer=new JTextArea(40,3);
		ta_footer.setFont(fuente_ta);
		ta_footer.setEnabled(false);
		ta_footer.setLineWrap(true);
		ta_footer.setWrapStyleWord(false);
		ta_footer.setTabSize(10);
		
		pa_ta3.add(ta_footer,BorderLayout.CENTER);
		pa_ta3.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.blue), "Pie tiquet",2,2,Font.getFont("Impresoras"),Color.blue));
		pa_ta3.setBackground(azuloscuro);
		pa_ta3.setBounds(30,210,310,90);//313,200
		pa_cont.add(pa_ta3);
		
		pa_impresoras=new JPanel(null);
		pa_impresoras.setBackground(azuloscuro);
		pa_impresoras.setForeground(azuloscuro);
		pa_impresoras.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.blue), "Impresoras",0,0,fuente_gen,Color.blue));
		
		la_imp_tiq=new JLabel("Tique:");
		la_imp_tiq.setEnabled(false);
		la_imp_tiq.setBackground(blanco);
		la_imp_tiq.setForeground(Color.blue);
		la_imp_tiq.setFont(fuente_gen);
		la_imp_tiq.setBounds(20, 30, 130, 30);
		pa_impresoras.add(la_imp_tiq);
		
		tf_imp_tiq=new JTextField();
		tf_imp_tiq.setEnabled(false);
		tf_imp_tiq.setBackground(blanco);
		tf_imp_tiq.setForeground(marron);
		tf_imp_tiq.setFont(fuente_gen);
		tf_imp_tiq.setBounds(150,30,180,30);
		pa_impresoras.add(tf_imp_tiq);
		
		la_imp_com=new JLabel("Comanda:");
		la_imp_com.setEnabled(false);
		la_imp_com.setBackground(blanco);
		la_imp_com.setForeground(Color.blue);
		la_imp_com.setFont(fuente_gen);
		la_imp_com.setBounds(20,60,130,30);
		pa_impresoras.add(la_imp_com);
		
		tf_imp_com=new JTextField();
		tf_imp_com.setEnabled(false);
		tf_imp_com.setBackground(blanco);
		tf_imp_com.setForeground(marron);
		tf_imp_com.setFont(fuente_gen);
		tf_imp_com.setBounds(150,60,180,30);
		pa_impresoras.add(tf_imp_com);
		
		pa_impresoras.setBounds(5, 320, 373, 100);
		pa_cont.add(pa_impresoras);
		
		pa_cont.setBounds(0,50,383,425);
		menu_conf.add(pa_cont);
		
		bt_apli=new JButton("Aplicar");
		bt_apli.setBackground(blanco);
		bt_apli.setForeground(azul);
		bt_apli.setFont(fuente_gen);
		bt_apli.setBounds(200,500,90,40);
		bt_apli.addActionListener(this);
		menu_conf.add(bt_apli);
		
		bt_cancel=new JButton("Cancelar");
		bt_cancel.setBackground(blanco);
		bt_cancel.setForeground(azul);
		bt_cancel.setFont(fuente_gen);
		bt_cancel.setBounds(290,500,90,40);
		bt_cancel.addActionListener(this);
		menu_conf.add(bt_cancel);
		
		bt_generar=new JButton("Por defecto");
		bt_generar.setBackground(blanco);
		bt_generar.setForeground(azul);
		bt_generar.setBounds(10,500,90,40);
		bt_generar.addActionListener(this);
		menu_conf.add(bt_generar);
		
		leer_config();
		
		menu_conf.setVisible(true);
		
	}
	
	public void leer_config(){
		boolean error=false;
		Builder builder=new Builder();
		Document doc=null;
		String linea;
		
		File archivo ;
		FileReader fr;
		
		if((archivo = new File ("resources/head.txt")).exists()){
			try {
				fr = new FileReader (archivo);
				BufferedReader br = new BufferedReader(fr);
				ta_head1.setText("");
				while((linea=br.readLine())!=null){
					ta_head1.append(linea+"\n");
				}
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if((archivo = new File ("resources/footer.txt")).exists()){
			try {
				fr = new FileReader (archivo);
				BufferedReader br = new BufferedReader(fr);
				ta_footer.setText("");
				while((linea=br.readLine())!=null){
					ta_footer.append(linea+"\n");
				}
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try{
			doc = builder.build(new FileInputStream("resources/config2.xml"));  
		}catch (IOException e){
			JOptionPane.showMessageDialog(null, "El archivo de configuración no se ha encontrado y se ha generado uno por defecto");
			generar_config();
		}catch (Exception e){
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Error inesperado, el archivo de configuración no se ha podido cargar.");
			error=true;
		}
		if(error==false){
			Element raiz = doc.getRootElement();  
			
			if(raiz.getFirstChildElement("caja").getValue().equals("1")){
				combo_caja.setSelectedIndex(0);
			}else{
				combo_caja.setSelectedIndex(1);
			}
			if(raiz.getFirstChildElement("replicante").getValue().equals("y")){
				cb_replica.setSelected(true);
			}else{
				cb_replica.setSelected(false);
			}
			if(raiz.getFirstChildElement("modif").getValue().equals("y")){
				cb_modif.setSelected(true);
			}else{
				cb_modif.setSelected(false);
			}
			
			tf_imp_tiq.setText(raiz.getFirstChildElement("imp_tiquet").getValue());
			tf_imp_com.setText(raiz.getFirstChildElement("imp_comanda").getValue());
		}
		
	}
	
	public void modificar_config(){  
	      
		FileWriter fichero;
		try{
			fichero = new FileWriter("resources/head.txt");
			fichero.write(ta_head1.getText());
			fichero.flush();
			fichero.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		try{
			fichero = new FileWriter("resources/footer.txt");
			fichero.write(ta_footer.getText());
			fichero.flush();
			fichero.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	    Element raiz = new Element("config");  
	   
        Element caja= new Element("caja");
        if(combo_caja.getSelectedIndex()==0){
        	caja.appendChild("1");
        }else{
        	caja.appendChild("2");
        }
        raiz.appendChild(caja);
		
        Element replicante= new Element("replicante");
        if(cb_replica.isSelected()){
        	replicante.appendChild("y");
        }else{
        	replicante.appendChild("n");
        }
        raiz.appendChild(replicante);
        
        Element modif= new Element("modif");
        if(cb_modif.isSelected()){
        	modif.appendChild("y");
        }else{
        	modif.appendChild("n");
        }
        raiz.appendChild(modif);
	     
        Element imp_tiq= new Element("imp_tiquet");
        imp_tiq.appendChild(tf_imp_tiq.getText());
        raiz.appendChild(imp_tiq);
        
        Element imp_com= new Element("imp_comanda");
        imp_com.appendChild(tf_imp_com.getText());
        raiz.appendChild(imp_com);
        
        Element cabecera=new Element("cabecera");
       
		cabecera.appendChild(ta_head1.getText());
        raiz.appendChild(cabecera);
        
	    Document doc = new Document(raiz);
	    try{
	    	Serializer serializer = new Serializer(System.out, "UTF-8");
	        serializer.setIndent(4);
	        serializer.setMaxLength(64);
	        
	        serializer.setOutputStream(new BufferedOutputStream(new FileOutputStream("resources/config2.xml")));
	        serializer.write(doc);
	        serializer.flush();
	    }catch (Exception e){
	    	e.printStackTrace();
	    }
	   
	}
	
	public static void cargar_config(ConfigObject c){
		boolean error=false;
		Builder builder=new Builder();
		Document doc=null;
		String linea;
		File archivo;
		FileReader fr;
		try {
			if((archivo = new File ("resources/head.txt")).exists()){
				fr = new FileReader (archivo);
				BufferedReader br = new BufferedReader(fr);
				while((linea=br.readLine())!=null){
					c.st_cabecera.add(linea);
				}
			}
			if((archivo = new File ("resources/footer.txt")).exists()){
				fr = new FileReader (archivo);
				BufferedReader br = new BufferedReader(fr);
				while((linea=br.readLine())!=null){
					c.st_pie.add(linea);
				}
			}
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		try{
			doc = builder.build(new FileInputStream("resources/config2.xml"));  
		}catch (IOException e){
			JOptionPane.showMessageDialog(null, "El archivo de configuración no se ha encontrado y se ha generado uno por defecto");
			generar_config();
		}catch (Exception e){
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Error inesperado, el archivo de configuración no se ha podido cargar.");
			error=true;
		}
		if(error==false){
			Element raiz = doc.getRootElement();  
			
			if(raiz.getFirstChildElement("caja").getValue().equals("1")){
				c.numerocaja=1;
			}else{
				c.numerocaja=2;
			}
			if(raiz.getFirstChildElement("replicante").getValue().equals("y")){
				c.bol_replicante=true;
			}else{
				c.bol_replicante=false;
			}
			if(raiz.getFirstChildElement("modif").getValue().equals("y")){
				c.bol_modificadores=true;
			}else{
				c.bol_modificadores=false;
			}
			
			c.st_imp_tiq=raiz.getFirstChildElement("imp_tiquet").getValue();
			c.st_imp_com=raiz.getFirstChildElement("imp_comanda").getValue();
			
		}
	}
	
	
	
	public static void generar_config(){
		
		 Element raiz = new Element("config");  
		   
		    
	        Element caja= new Element("caja");
	        caja.appendChild("1");
	        raiz.appendChild(caja);
			
	        Element replicante= new Element("replicante");
	        replicante.appendChild("n");
	        raiz.appendChild(replicante);
	        
	        Element modif= new Element("modif");
	        modif.appendChild("n");
	        raiz.appendChild(modif);
		     
	        Element imp_tiq= new Element("imp_tiquet");
	        imp_tiq.appendChild("Etiqueta de impresora");
	        raiz.appendChild(imp_tiq);
	        
	        Element imp_com= new Element("imp_comanda");
	        imp_com.appendChild("Etiqueta de impresora");
	        raiz.appendChild(imp_com);
	        
		    Document doc = new Document(raiz);
		    try{
		    	Serializer serializer = new Serializer(System.out, "UTF-8");
		        serializer.setIndent(4);
		        serializer.setMaxLength(64);
		        
		        serializer.setOutputStream(new BufferedOutputStream(new FileOutputStream("resources/config2.xml")));
		        serializer.write(doc);
		        serializer.flush();
		    }catch (Exception e){
		    	e.printStackTrace();
		    }
		
		
	}

	@Override
	public void actionPerformed(ActionEvent ev) {
		// TODO Auto-generated method stub
		
		if(ev.getSource()==bt_cancel){
			menu_conf.dispose();
		}else if(ev.getSource()==bt_apli){
			modificar_config();
			menu_conf.dispose();
		}else if(ev.getSource()==bt_generar){
			int eleccion;
			String str="Si elige esta opción se eliminará para siempre la configuración actual y se creará \nun archivo de configuración con los parámetros por defecto. ¿Continuar de todos modos?";
			eleccion=JOptionPane.showConfirmDialog(null, (Object)str,"Confirmación",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
			if(eleccion==JOptionPane.YES_OPTION){
				generar_config();
				leer_config();
			}
		}else if(ev.getSource()==cb_habilitar){
			
			boolean habilitar;
			if(cb_habilitar.isSelected()){
				habilitar=true;
			}else{
				habilitar=false;
			}
			la_caja.setEnabled(habilitar);
			combo_caja.setEnabled(habilitar);
			cb_replica.setEnabled(habilitar);
			tf_imp_tiq.setEnabled(habilitar);
			la_rep.setEnabled(habilitar);
			la_imp_tiq.setEnabled(habilitar);
			//la_modif.setEnabled(habilitar);
			la_caja.setEnabled(habilitar);
			ta_head1.setEnabled(habilitar);
			ta_footer.setEnabled(habilitar);
			//cb_modif.setEnabled(habilitar);
			la_imp_com.setEnabled(habilitar);
			tf_imp_com.setEnabled(habilitar);
		}
	}
	
	
}
